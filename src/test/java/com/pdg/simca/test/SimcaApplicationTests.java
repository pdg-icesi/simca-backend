package com.pdg.simca.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.pdg.simca.main.SimcaApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SimcaApplication.class)
public class SimcaApplicationTests {

	@Test
	public void contextLoads() {
	}

}
