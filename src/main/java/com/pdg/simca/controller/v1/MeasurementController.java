package com.pdg.simca.controller.v1;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pdg.simca.config.SwaggerConfig;
import com.pdg.simca.dto.AddMeasurementDTO;
import com.pdg.simca.dto.AddValuesToMeasurementDTO;
import com.pdg.simca.dto.MeasurementDTO;
import com.pdg.simca.dto.MeasurementsDTO;
import com.pdg.simca.exception.BadRequestException;
import com.pdg.simca.exception.NotFoundException;
import com.pdg.simca.logic.MeasurementLogic;
import com.pdg.simca.model.Measurement;
import com.pdg.simca.utils.ModelUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.AuthorizationScope;

@RestController
@Api(value = "onlinestore", description = "Measurement API v1")
@RequestMapping(value = "/api/measurement/v1")
public class MeasurementController {

	@Autowired
	private MeasurementLogic measurementLogic;

	@ApiOperation(value = "Get measurements (optional filters)", response = MeasurementsDTO.class, notes = "Fetch a list of measurements. Measurements can be filtered by a combination of date, time, place and/or pollutant filters.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@GetMapping(value = "/measurements")
	public ResponseEntity<MeasurementsDTO> getMeasurement(
			@ApiParam(value = "Date filter") @RequestParam(name = "date", required = false) String dateRange,
			@ApiParam(value = "Time filter") @RequestParam(name = "time", required = false) String timeRange,
			@ApiParam(value = "Place filter") @RequestParam(name = "place", required = false) String placesIds,
			@ApiParam(value = "Pollutant filter") @RequestParam(name = "pollutant", required = false) String pollutantsIds)
			throws BadRequestException, ParseException {

		List<Measurement> measurements = measurementLogic.findByFilter(dateRange, timeRange, placesIds, pollutantsIds);

		List<MeasurementDTO> listMeasurementsDTO = new ArrayList<MeasurementDTO>();

		for (Measurement actMeasurement : measurements) {
			MeasurementDTO measurementDTO = ModelUtils.mapObjectToClass(actMeasurement, MeasurementDTO.class);
			listMeasurementsDTO.add(measurementDTO);
		}

		MeasurementsDTO measurementsDTO = new MeasurementsDTO(listMeasurementsDTO);
		return new ResponseEntity<MeasurementsDTO>(measurementsDTO, HttpStatus.OK);
	}

	@ApiOperation(value = "Get measurement by id", response = MeasurementDTO.class, notes = "Fetch a single measurement by a given id.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@GetMapping(value = "/measurement/{measurementId}")
	public ResponseEntity<MeasurementDTO> getMeasurementById(
			@ApiParam(value = "Id of the measurement to be fetched") @PathVariable Integer measurementId)
			throws NotFoundException, BadRequestException {

		Measurement measurement = measurementLogic.get(measurementId);
		MeasurementDTO measurementDTO = ModelUtils.mapObjectToClass(measurement, MeasurementDTO.class);
		return new ResponseEntity<MeasurementDTO>(measurementDTO, HttpStatus.OK);
	}

	@ApiOperation(value = "Create a measurement", response = Void.class, notes = "Create a measurement. Place and pollutants ids must already exist.", authorizations = {
			@Authorization(value = SwaggerConfig.securitySchemaOAuth2, scopes = {
					@AuthorizationScope(scope = SwaggerConfig.authorizationScopeGlobal, description = SwaggerConfig.authorizationScopeGlobalDesc) }) })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@PostMapping(value = "/measurement")
	public ResponseEntity<Void> createMeasurement(@RequestBody AddMeasurementDTO addMeasurementDTO,
			OAuth2Authentication auth) throws BadRequestException, ParseException, NotFoundException {
		measurementLogic.add(addMeasurementDTO, auth);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	@ApiOperation(value = "Add values to a measurement", response = Void.class, notes = "Add measured values of a given pollutant to a specified measurement.", authorizations = {
			@Authorization(value = SwaggerConfig.securitySchemaOAuth2, scopes = {
					@AuthorizationScope(scope = SwaggerConfig.authorizationScopeGlobal, description = SwaggerConfig.authorizationScopeGlobalDesc) }) })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@PostMapping(value = "/measurement/{measurementId}/add-values")
	public ResponseEntity<Void> addValuesToMeasurement(
			@ApiParam(value = "Id of the measuremet to add values to") @PathVariable("measurementId") Integer measurementId,
			@RequestBody AddValuesToMeasurementDTO addValuesToMeasurementDTO)
			throws NotFoundException, BadRequestException {
		measurementLogic.addValuesToMeasurement(measurementId, addValuesToMeasurementDTO);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	@ApiOperation(value = "Delete a measurement", response = Void.class, notes = "Delete a specified measurement.", authorizations = {
			@Authorization(value = SwaggerConfig.securitySchemaOAuth2, scopes = {
					@AuthorizationScope(scope = SwaggerConfig.authorizationScopeGlobal, description = SwaggerConfig.authorizationScopeGlobalDesc) }) })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@DeleteMapping(value = "/measurement/{measurementId}")
	public ResponseEntity<Void> deleteMeasurement(
			@ApiParam(value = "Id of the measurement to be deleted") @PathVariable("measurementId") Integer measurementId)
			throws NotFoundException, BadRequestException {

		measurementLogic.delete(measurementId);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	@ApiOperation(value = "Edit a measurement", response = Void.class, notes = "Edit a specified measurement. All values must be valid.", authorizations = {
			@Authorization(value = SwaggerConfig.securitySchemaOAuth2, scopes = {
					@AuthorizationScope(scope = SwaggerConfig.authorizationScopeGlobal, description = SwaggerConfig.authorizationScopeGlobalDesc) }) })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@PutMapping(value = "/measurement")
	public ResponseEntity<Void> editMeasurement(@RequestBody MeasurementDTO measurementDTO)
			throws NotFoundException, BadRequestException, ParseException {

		measurementLogic.edit(measurementDTO);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

}
