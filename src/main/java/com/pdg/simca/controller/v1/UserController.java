package com.pdg.simca.controller.v1;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pdg.simca.config.SwaggerConfig;
import com.pdg.simca.dto.AddUserDTO;
import com.pdg.simca.dto.RequestPasswordResetDTO;
import com.pdg.simca.dto.ResetPasswordDTO;
import com.pdg.simca.dto.StateDTO;
import com.pdg.simca.dto.UserDTO;
import com.pdg.simca.dto.UsersDTO;
import com.pdg.simca.exception.BadRequestException;
import com.pdg.simca.exception.ConflictException;
import com.pdg.simca.exception.NotFoundException;
import com.pdg.simca.logic.UserLogic;
import com.pdg.simca.model.State;
import com.pdg.simca.model.User;
import com.pdg.simca.utils.ModelUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.AuthorizationScope;

@RestController
@Api(value = "onlinestore", description = "User API v1")
@RequestMapping("/api/user/v1")
public class UserController {

	@Autowired
	private UserLogic userLogic;

	@ApiOperation(value = "Get all users", response = UsersDTO.class, notes = "Fetch a list of all registered users", authorizations = {
			@Authorization(value = SwaggerConfig.securitySchemaOAuth2, scopes = {
					@AuthorizationScope(scope = SwaggerConfig.authorizationScopeGlobal, description = SwaggerConfig.authorizationScopeGlobalDesc) }) })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@GetMapping(name = "List all users", path = "/users")
	public ResponseEntity<UsersDTO> getUsers() {
		List<User> users = userLogic.getAll();
		List<UserDTO> usersDTOList = new ArrayList<UserDTO>();
		for (User user : users) {
			UserDTO userDTO = ModelUtils.mapObjectToClass(user, UserDTO.class);
			userDTO.setState(user.getStateId().getName());
			usersDTOList.add(userDTO);
		}
		UsersDTO usersDTO = new UsersDTO(usersDTOList);
		return new ResponseEntity<UsersDTO>(usersDTO, HttpStatus.OK);
	}

	@ApiOperation(value = "Get user by id", response = UserDTO.class, notes = "Fetch a user by a given user id", authorizations = {
			@Authorization(value = SwaggerConfig.securitySchemaOAuth2, scopes = {
					@AuthorizationScope(scope = SwaggerConfig.authorizationScopeGlobal, description = SwaggerConfig.authorizationScopeGlobalDesc) }) })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@GetMapping(name = "Get specific user", path = "/user/{id}")
	public ResponseEntity<UserDTO> getUser(
			@ApiParam(value = "Registered user id") @PathVariable(name = "id", required = true) String id)
			throws BadRequestException, NotFoundException {

		User user = userLogic.get(id);
		UserDTO userDTO = ModelUtils.mapObjectToClass(user, UserDTO.class);
		userDTO.setState(user.getStateId().getName());
		return new ResponseEntity<UserDTO>(userDTO, HttpStatus.OK);
	}

	@ApiOperation(value = "Get user by username", response = UserDTO.class, notes = "Fetch a user by a given username", authorizations = {
			@Authorization(value = SwaggerConfig.securitySchemaOAuth2, scopes = {
					@AuthorizationScope(scope = SwaggerConfig.authorizationScopeGlobal, description = SwaggerConfig.authorizationScopeGlobalDesc) }) })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@GetMapping(name = "Get specific user", path = "/user/by-username/{userName}")
	public ResponseEntity<UserDTO> getUserByUsername(
			@ApiParam(value = "Registered user username") @PathVariable(name = "userName", required = true) String userName)
			throws BadRequestException, NotFoundException {

		User user = userLogic.getByUsername(userName);
		UserDTO userDTO = ModelUtils.mapObjectToClass(user, UserDTO.class);
		userDTO.setState(user.getStateId().getName());
		return new ResponseEntity<UserDTO>(userDTO, HttpStatus.OK);
	}

	@ApiOperation(value = "Create a user", response = Void.class, notes = "Create a user. Username and email must be unique. Password must contain at least an uppercase letter, a number and between 8-20 characters")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@PostMapping(value = "/user")
	public ResponseEntity<Void> addUser(@RequestBody(required = true) AddUserDTO addUserDTO)
			throws BadRequestException, ConflictException {

		userLogic.add(addUserDTO);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	@ApiOperation(value = "Delete a user", response = Void.class, notes = "Delete a user by a given user id", authorizations = {
			@Authorization(value = SwaggerConfig.securitySchemaOAuth2, scopes = {
					@AuthorizationScope(scope = SwaggerConfig.authorizationScopeGlobal, description = SwaggerConfig.authorizationScopeGlobalDesc) }) })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@DeleteMapping(value = "/user")
	public ResponseEntity<Void> deleteUser(
			@ApiParam(value = "Registered user id") @RequestParam(name = "id", required = true) String id)
			throws BadRequestException {

		userLogic.delete(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	@ApiOperation(value = "Check an email confirmation status", response = StateDTO.class, notes = "Check the currnet state of an email confirmation request")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@GetMapping(value = "/confirm-email")
	public ResponseEntity<StateDTO> checkEmailConfirmationStatus(
			@ApiParam(value = "Email confirmation request token") @RequestParam(name = "token", required = true) String token)
			throws BadRequestException, NotFoundException {

		State state = userLogic.checkEmailConfirmationStatus(token);
		StateDTO stateDTO = ModelUtils.mapObjectToClass(state, StateDTO.class);
		stateDTO.setStateType(state.getStateTypeId().getName());
		return new ResponseEntity<StateDTO>(stateDTO, HttpStatus.OK);
	}

	@ApiOperation(value = "Check password reset request status", response = StateDTO.class, notes = "Check the current state of a password reset request")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@GetMapping(value = "/password-reset")
	public ResponseEntity<StateDTO> checkPasswordResetRequestStatus(
			@ApiParam(value = "Password reset request token") @RequestParam(name = "token", required = true) String token)
			throws BadRequestException, NotFoundException {

		State state = userLogic.checkPasswordResetRequestStatus(token);
		StateDTO stateDTO = ModelUtils.mapObjectToClass(state, StateDTO.class);
		stateDTO.setStateType(state.getStateTypeId().getName());
		return new ResponseEntity<StateDTO>(stateDTO, HttpStatus.OK);
	}

	@ApiOperation(value = "Confirm email account", response = Void.class, notes = "Confirm an email account with a currently active email confirmation request")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@PutMapping(value = "/confirm-email")
	public ResponseEntity<Void> confirmEmail(
			@ApiParam(value = "Email confirmation request token") @RequestParam(name = "token", required = true) String token)
			throws BadRequestException, NotFoundException, ConflictException {

		userLogic.confirmEmail(token);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	@ApiOperation(value = "Reset user password", response = Void.class, notes = "Reset a user password with a currently active password reset request")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@PutMapping(value = "/password-reset")
	public ResponseEntity<Void> resetPassword(@RequestBody ResetPasswordDTO resetPasswordDTO)
			throws BadRequestException, NotFoundException, ConflictException {

		userLogic.resetPassword(resetPasswordDTO);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	@ApiOperation(value = "Request a password reset", response = Void.class, notes = "Request a password reset for a user with specified email. The email must be confirmed")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@PostMapping(value = "/password-reset")
	public ResponseEntity<Void> requestPasswordReset(@RequestBody RequestPasswordResetDTO requestPasswordResetDTO)
			throws BadRequestException, NotFoundException, ConflictException {

		userLogic.requestPasswordReset(requestPasswordResetDTO);
		return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
	}

}
