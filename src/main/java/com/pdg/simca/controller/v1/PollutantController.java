package com.pdg.simca.controller.v1;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pdg.simca.config.SwaggerConfig;
import com.pdg.simca.dto.AddPollutantDTO;
import com.pdg.simca.dto.PollutantDTO;
import com.pdg.simca.dto.PollutantsDTO;
import com.pdg.simca.exception.BadRequestException;
import com.pdg.simca.exception.ConflictException;
import com.pdg.simca.exception.NotFoundException;
import com.pdg.simca.logic.PollutantLogic;
import com.pdg.simca.model.Pollutant;
import com.pdg.simca.utils.ModelUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.AuthorizationScope;

@RestController
@Api(value = "onlinestore", description = "Pollutant API v1")
@RequestMapping(value = "/api/pollutant/v1")
public class PollutantController {

	@Autowired
	private PollutantLogic pollutantLogic;

	@ApiOperation(value = "Create a pollutant", response = Void.class, notes = "Create a pollutant. Specify the correct measurement units", authorizations = {
			@Authorization(value = SwaggerConfig.securitySchemaOAuth2, scopes = {
					@AuthorizationScope(scope = SwaggerConfig.authorizationScopeGlobal, description = SwaggerConfig.authorizationScopeGlobalDesc) }) })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@PostMapping(value = "/pollutant")
	public ResponseEntity<Void> createPollutant(@RequestBody AddPollutantDTO addPollutantDTO)
			throws BadRequestException, ConflictException {

		pollutantLogic.addPollutant(addPollutantDTO);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	@ApiOperation(value = "Get a pollutant", response = PollutantDTO.class, notes = "Get a pollutant by a given pollutant id")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@GetMapping(value = "/pollutant/{idPollutant}")
	public ResponseEntity<PollutantDTO> getPollutant(@ApiParam("Pollutant id") @PathVariable Integer idPollutant)
			throws NotFoundException, BadRequestException {

		Pollutant pollutant = pollutantLogic.get(idPollutant);
		PollutantDTO pollutantDTO = ModelUtils.mapObjectToClass(pollutant, PollutantDTO.class);
		return new ResponseEntity<PollutantDTO>(pollutantDTO, HttpStatus.OK);
	}

	@ApiOperation(value = "Get all pollutants", response = PollutantsDTO.class, notes = "Get all registered pollutants")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@GetMapping(value = "/pollutants")
	public ResponseEntity<PollutantsDTO> getPollutants() {

		List<Pollutant> pollutants = pollutantLogic.getPollutants();
		List<PollutantDTO> pollutantsDTOList = new ArrayList<PollutantDTO>();

		for (Pollutant pollutant : pollutants) {
			PollutantDTO pollutantDTO = ModelUtils.mapObjectToClass(pollutant, PollutantDTO.class);
			pollutantsDTOList.add(pollutantDTO);
		}

		PollutantsDTO pollutantsDTO = new PollutantsDTO(pollutantsDTOList);
		return new ResponseEntity<PollutantsDTO>(pollutantsDTO, HttpStatus.OK);
	}

	@ApiOperation(value = "Edit a pollutant", response = Void.class, notes = "Edit a pollutat with a given id. All values must be vaid", authorizations = {
			@Authorization(value = SwaggerConfig.securitySchemaOAuth2, scopes = {
					@AuthorizationScope(scope = SwaggerConfig.authorizationScopeGlobal, description = SwaggerConfig.authorizationScopeGlobalDesc) }) })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@PutMapping(value = "/pollutant")
	public ResponseEntity<Void> editPollutant(@RequestBody PollutantDTO pollutantDTO)
			throws BadRequestException, NotFoundException, ConflictException {

		pollutantLogic.editPollutant(pollutantDTO);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	@ApiOperation(value = "Delete a pollutant", response = Void.class, notes = "Delete a pollutant by a given pollutant id", authorizations = {
			@Authorization(value = SwaggerConfig.securitySchemaOAuth2, scopes = {
					@AuthorizationScope(scope = SwaggerConfig.authorizationScopeGlobal, description = SwaggerConfig.authorizationScopeGlobalDesc) }) })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@DeleteMapping(value = "/pollutant/{idPollutant}")
	public ResponseEntity<Void> deletePollutant(@ApiParam("Pollutant id") @PathVariable Integer idPollutant)
			throws NotFoundException, BadRequestException {

		pollutantLogic.deletePollutant(idPollutant);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

}
