package com.pdg.simca.controller.v1;

import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mongodb.BasicDBObject;
import com.pdg.simca.dto.MeasurementDataDTO;
import com.pdg.simca.exception.BadRequestException;
import com.pdg.simca.exception.NotFoundException;
import com.pdg.simca.logic.MeasurementDataLogic;
import com.pdg.simca.model.nosql.MeasurementData;
import com.pdg.simca.utils.Tools;

import io.swagger.annotations.Api;

@RestController
@Api(value = "onlinestore", description = "Data API v1")
@RequestMapping(value = "/api/data/v1")
public class DataController {

	@Autowired
	private MeasurementDataLogic measurementDataLogic;

	@GetMapping("/data")
	public ResponseEntity<MeasurementDataDTO> getMeasurementPollutantData(
			@RequestParam("measurementId") Integer measurementId, @RequestParam("pollutantId") Integer pollutantId)
			throws BadRequestException, NotFoundException {

		MeasurementData measurementData = measurementDataLogic.find(measurementId, pollutantId);

		BasicDBObject basicDBObject = measurementData.getData();

		HashMap<Date, Float> data = Tools.convertBasicDBObjectToDataMap(basicDBObject);

		MeasurementDataDTO measurementDataDTO = new MeasurementDataDTO(measurementData.getId(), data);

		return new ResponseEntity<MeasurementDataDTO>(measurementDataDTO, HttpStatus.OK);
	}

}