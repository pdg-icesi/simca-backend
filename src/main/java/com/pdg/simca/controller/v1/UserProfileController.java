package com.pdg.simca.controller.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pdg.simca.config.SwaggerConfig;
import com.pdg.simca.dto.UserProfileDTO;
import com.pdg.simca.exception.BadRequestException;
import com.pdg.simca.exception.NotFoundException;
import com.pdg.simca.logic.UserProfileLogic;
import com.pdg.simca.model.UserProfile;
import com.pdg.simca.utils.ModelUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.AuthorizationScope;

@RestController
@Api(value = "onlinestore", description = "User Profile API v1")
@RequestMapping("/api/userprofile/v1")
public class UserProfileController {

	@Autowired
	private UserProfileLogic userProfileLogic;

	@ApiOperation(value = "Get user profile", response = UserProfileDTO.class, notes = "Get a user profile by a given user id", authorizations = {
			@Authorization(value = SwaggerConfig.securitySchemaOAuth2, scopes = {
					@AuthorizationScope(scope = SwaggerConfig.authorizationScopeGlobal, description = SwaggerConfig.authorizationScopeGlobalDesc) }) })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@GetMapping(value = "/profile/{id}")
	public ResponseEntity<UserProfileDTO> getUserProfile(@ApiParam("User id") @PathVariable("id") String id)
			throws BadRequestException {

		UserProfile userProfile = userProfileLogic.getUserProfile(id);
		UserProfileDTO userProfileDTO = ModelUtils.mapObjectToClass(userProfile, UserProfileDTO.class, true);
		userProfileDTO.setUserId(userProfile.getUserId());
		return new ResponseEntity<UserProfileDTO>(userProfileDTO, HttpStatus.OK);
	}

	@ApiOperation(value = "Edit a user profile", response = Void.class, notes = "Edit a user profile by a given user id", authorizations = {
			@Authorization(value = SwaggerConfig.securitySchemaOAuth2, scopes = {
					@AuthorizationScope(scope = SwaggerConfig.authorizationScopeGlobal, description = SwaggerConfig.authorizationScopeGlobalDesc) }) })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@PutMapping(value = "/profile")
	public ResponseEntity<Void> editUserProfile(@RequestBody UserProfileDTO userProfileDTO)
			throws BadRequestException, NotFoundException {

		userProfileLogic.editUserProfile(userProfileDTO);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

}
