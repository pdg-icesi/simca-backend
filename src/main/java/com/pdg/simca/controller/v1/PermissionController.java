package com.pdg.simca.controller.v1;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;

@RestController
@Api(value = "onlinestore", description = "Permission API v1")
@RequestMapping("/api/permission/v1")
public class PermissionController {

	/*@Autowired
	private PermissionLogic permissionLogic;

	@GetMapping(value = "/")
	public ResponseEntity<PermissionsDTO> getPermissions() {
		List<Permission> permissions = permissionLogic.getPermissions();
		List<PermissionDTO> permissionsDTOList = new ArrayList<PermissionDTO>();
		for (Permission permission : permissions) {
			permissionsDTOList.add(ModelUtils.mapObjectToClass(permission, PermissionDTO.class));
		}
		PermissionsDTO permissionsDTO = new PermissionsDTO(permissionsDTOList);
		return new ResponseEntity<PermissionsDTO>(permissionsDTO, HttpStatus.OK);
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<?> getPermission(@PathVariable("id") Integer id) {

		Permission permission = permissionLogic.getPermission(id);
		PermissionDTO permissionDTO = ModelUtils.mapObjectToClass(permission, PermissionDTO.class);
		return new ResponseEntity<PermissionDTO>(permissionDTO, HttpStatus.OK);
	}

	@PutMapping(value = "/")
	public ResponseEntity<?> editPermission(@RequestBody PermissionDTO permissionDTO) {
				
		permissionLogic.editPermission(permissionDTO);		
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}*/

}
