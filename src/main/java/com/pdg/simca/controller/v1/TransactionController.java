package com.pdg.simca.controller.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pdg.simca.dto.AddTransactionDTO;
import com.pdg.simca.exception.BadRequestException;
import com.pdg.simca.exception.ConflictException;
import com.pdg.simca.logic.TransactionLogic;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "onlinestore", description = "Transaction API v1")
@RequestMapping(value = "/api/transaction/v1")
public class TransactionController {
	
	@Autowired
	private TransactionLogic transactionLogic;
	
	@ApiOperation(value = "Handle transaction", response = Void.class, notes="")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@PostMapping(value = "/donation")
	public ResponseEntity<Void> createTransaction(
			@ApiParam(value = "Transaction state") @RequestParam(name = "state_pol", required = true) Integer transactionState,
			@ApiParam(value = "Transaction reference in SIMCA") @RequestParam(name = "reference_sale", required = true) String transactionId,
			@ApiParam(value = "Transaction reference in PayU") @RequestParam(name = "reference_pol", required = true) String transactionReference,
			@ApiParam(value = "Signature") @RequestParam(name = "sign", required = true) String signature,
			@ApiParam(value = "Transaction value") @RequestParam(name = "value", required = true) double value,
			@ApiParam(value = "Test indicator") @RequestParam(name = "test", required = true) boolean test)
			throws BadRequestException, ConflictException {
		
		AddTransactionDTO addTransactionDTO = new AddTransactionDTO();
		addTransactionDTO.setState(transactionState);
		addTransactionDTO.setReference(transactionReference);
		addTransactionDTO.setSignature(signature);
		addTransactionDTO.setValue(value);
		
		transactionLogic.addTransaction(addTransactionDTO);
		return new ResponseEntity<Void>(HttpStatus.OK);
		
	}

}
