package com.pdg.simca.controller.v1;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pdg.simca.config.SwaggerConfig;
import com.pdg.simca.dto.AddPlaceDTO;
import com.pdg.simca.dto.PlaceDTO;
import com.pdg.simca.dto.PlacesDTO;
import com.pdg.simca.exception.BadRequestException;
import com.pdg.simca.exception.NotFoundException;
import com.pdg.simca.logic.PlaceLogic;
import com.pdg.simca.model.Place;
import com.pdg.simca.utils.ModelUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.AuthorizationScope;

@RestController
@Api(value = "onlinestore", description = "Place API v1")
@RequestMapping(value = "/api/place/v1")
public class PlaceController {

	@Autowired
	private PlaceLogic placeLogic;

	@ApiOperation(value = "Create a place", response = Void.class, notes = "Create a place. Latitude and longitude will be shown in a map, make sure they match with the given address", authorizations = {
			@Authorization(value = SwaggerConfig.securitySchemaOAuth2, scopes = {
					@AuthorizationScope(scope = SwaggerConfig.authorizationScopeGlobal, description = SwaggerConfig.authorizationScopeGlobalDesc) }) })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@PostMapping(value = "/place")
	public ResponseEntity<Void> createPlace(@RequestBody AddPlaceDTO addPlaceDTO) throws BadRequestException {

		placeLogic.addPlace(addPlaceDTO);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	@ApiOperation(value = "Get a place", response = PlaceDTO.class, notes = "Fetch a place by a given place id")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@GetMapping(value = "/place/{idPlace}")
	public ResponseEntity<PlaceDTO> getPlace(@ApiParam("Place id") @PathVariable Integer idPlace)
			throws NotFoundException, BadRequestException {

		Place place = placeLogic.getPlace(idPlace);
		PlaceDTO placeDTO = ModelUtils.mapObjectToClass(place, PlaceDTO.class);
		return new ResponseEntity<PlaceDTO>(placeDTO, HttpStatus.OK);
	}

	@ApiOperation(value = "Get all places", response = PlacesDTO.class, notes = "Fetch all registered places")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@GetMapping(value = "/places")
	public ResponseEntity<PlacesDTO> getPlaces() throws NotFoundException {

		List<Place> places = placeLogic.getPlaces();
		List<PlaceDTO> placesDTOList = new ArrayList<PlaceDTO>();

		for (Place p : places) {
			PlaceDTO placeDTO = ModelUtils.mapObjectToClass(p, PlaceDTO.class);
			placesDTOList.add(placeDTO);
		}

		PlacesDTO placesDTO = new PlacesDTO(placesDTOList);
		return new ResponseEntity<PlacesDTO>(placesDTO, HttpStatus.OK);
	}

	@ApiOperation(value = "Edit a place", response = Void.class, notes = "Edit a place with a given id. All values must be valid", authorizations = {
			@Authorization(value = SwaggerConfig.securitySchemaOAuth2, scopes = {
					@AuthorizationScope(scope = SwaggerConfig.authorizationScopeGlobal, description = SwaggerConfig.authorizationScopeGlobalDesc) }) })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@PutMapping(value = "/place")
	public ResponseEntity<Void> editPlace(@RequestBody PlaceDTO placeDTO)
			throws NotFoundException, BadRequestException {

		placeLogic.editPlace(placeDTO);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	@ApiOperation(value = "Delete a place", response = Void.class, notes = "Delete a place by a given id", authorizations = {
			@Authorization(value = SwaggerConfig.securitySchemaOAuth2, scopes = {
					@AuthorizationScope(scope = SwaggerConfig.authorizationScopeGlobal, description = SwaggerConfig.authorizationScopeGlobalDesc) }) })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully received the request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 400, message = "There is a problem with your request, check the response error"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 409, message = "The resource you were trying to reach is not found") })
	@DeleteMapping(value = "/place/{idPlace}")
	public ResponseEntity<Void> deleteLugar(@ApiParam("Place id") @PathVariable Integer idPlace)
			throws NotFoundException, BadRequestException {

		placeLogic.deletePlace(idPlace);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

}
