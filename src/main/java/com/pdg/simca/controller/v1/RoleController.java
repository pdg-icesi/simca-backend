package com.pdg.simca.controller.v1;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;

@RestController
@Api(value = "onlinestore", description = "Role API v1")
@RequestMapping("/api/role/v1")
public class RoleController {

	/*@Autowired
	private RoleLogic roleLogic;

	@GetMapping(value = "/roles")
	public ResponseEntity<?> getRoles() {
		List<Role> roles = roleLogic.getRoles();
		List<RoleDTO> rolesDTOList = new ArrayList<RoleDTO>();
		for (Role role : roles) {
			rolesDTOList.add(ModelUtils.mapObjectToClass(role, RoleDTO.class));
		}
		RolesDTO rolesDTO = new RolesDTO(rolesDTOList);
		return new ResponseEntity<RolesDTO>(rolesDTO, HttpStatus.OK);
	}

	@GetMapping(value = "/roles/{id}")
	public ResponseEntity<?> getRole(@PathVariable("id") Integer id) {
	
		Role role = roleLogic.getRole(id);		
		RoleDTO roleDTO = ModelUtils.mapObjectToClass(role, RoleDTO.class);
		return new ResponseEntity<RoleDTO>(roleDTO, HttpStatus.OK);
	}

	@PostMapping(value = "/roles")
	public ResponseEntity<?> addRole(@RequestBody AddRoleDTO addRoleDTO) {
		
		roleLogic.addRole(addRoleDTO);		
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	@PutMapping(value = "/roles")
	public ResponseEntity<?> editRole(@RequestBody RoleDTO roleDTO) {
		
		roleLogic.editRole(roleDTO);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	@DeleteMapping(value = "/roles")
	public ResponseEntity<?> deleteRole(@RequestParam(name = "id", required = true) Integer id) {
		roleLogic.deleteRole(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}*/

}
