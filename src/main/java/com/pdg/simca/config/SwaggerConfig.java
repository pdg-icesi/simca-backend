package com.pdg.simca.config;

import static com.google.common.collect.Lists.newArrayList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicate;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.GrantType;
import springfox.documentation.service.ImplicitGrant;
import springfox.documentation.service.LoginEndpoint;
import springfox.documentation.service.OAuth;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	public static final String securitySchemaOAuth2 = "oauth2schema";
	public static final String authorizationScopeGlobal = "global";
	public static final String authorizationScopeGlobalDesc = "accessEverything";

	@Value("${swagger.protocol}")
	private String protocol;

	@Value("${swagger.host}")
	private String host;

	@Value("${security.web.client.id}")
	private String clientId;

	@Value("${security.web.client.secret}")
	private String clientSecret;

	@Value("${swagger.api_metadata.title}")
	private String title;

	@Value("${swagger.api_metadata.description}")
	private String description;

	@Value("${swagger.api_metadata.version}")
	private String version;

	@Value("${swagger.api_metadata.termsOfServiceUrl}")
	private String termsOfServiceUrl;

	@Value("${swagger.api_metadata.contact.name}")
	private String contactName;

	@Value("${swagger.api_metadata.contact.url}")
	private String contactUrl;

	@Value("${swagger.api_metadata.contact.email}")
	private String contactEmail;

	@Value("${swagger.api_metadata.license}")
	private String license;

	@Value("${swagger.api_metadata.licenseUrl}")
	private String licenseUrl;

	@Bean
	public Docket api() {

		Docket docket = new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.pdg.simca.controller")).paths(internalPaths()).build()
				.securitySchemes(newArrayList(securitySchema()))//.securityContexts(newArrayList(securityContext()))
//				.globalOperationParameters(newArrayList(
//						new ParameterBuilder().name("Authorization").description("Bearer authorization header")
//								.modelRef(new ModelRef("string")).parameterType("header").required(true).build()))
				.protocols(Collections.singleton(protocol)).host(host).apiInfo(metaData());

		return docket;
	}

	private OAuth securitySchema() {
		AuthorizationScope authorizationScope = new AuthorizationScope(authorizationScopeGlobal,
				"Global authorization scope");
		LoginEndpoint loginEndpoint = new LoginEndpoint(protocol + "://" + host + "/oauth/token");
		GrantType grantType = new ImplicitGrant(loginEndpoint, "access_token");
		return new OAuth(securitySchemaOAuth2, newArrayList(authorizationScope), newArrayList(grantType));
	}

	private SecurityContext securityContext() {
		return SecurityContext.builder().securityReferences(defaultAuth()).forPaths(internalPaths()).build();
	}

	private List<SecurityReference> defaultAuth() {
		AuthorizationScope authorizationScope = new AuthorizationScope(authorizationScopeGlobal,
				authorizationScopeGlobalDesc);
		AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
		authorizationScopes[0] = authorizationScope;
		return newArrayList(new SecurityReference(securitySchemaOAuth2, authorizationScopes));
	}

	@Bean
	public SecurityConfiguration security() {

		return new SecurityConfiguration(clientId, clientSecret, null, "simca-app", "oauth2", ApiKeyVehicle.HEADER,
				"Authorization", "," /* scope separator */);
	}

	private ApiInfo metaData() {

		Contact contact = new Contact(contactName, contactUrl, contactEmail);

		@SuppressWarnings("rawtypes")
		List<VendorExtension> vendorExtensions = new ArrayList<VendorExtension>();

		ApiInfo apiInfo = new ApiInfo(title, description, version, termsOfServiceUrl, contact, license, licenseUrl,
				vendorExtensions);
		return apiInfo;
	}

	private Predicate<String> internalPaths() {
		return PathSelectors.any();
	}

}