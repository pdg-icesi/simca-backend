package com.pdg.simca.main;

import java.util.TimeZone;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
@ComponentScan("com.pdg.simca")
@EntityScan("com.pdg.simca.model")
@EnableJpaRepositories("com.pdg.simca.dao")
@EnableMongoRepositories("com.pdg.simca.dao.nosql")
public class SimcaApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(SimcaApplication.class, args);
	}

	@Override
	public void run(String... arg0) throws Exception {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}

}
