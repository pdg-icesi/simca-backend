/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pdg.simca.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author root
 */
@Entity
@Table(name = "measurement_pollutant")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MeasurementPollutant.findAll", query = "SELECT m FROM MeasurementPollutant m"),
    @NamedQuery(name = "MeasurementPollutant.findById", query = "SELECT m FROM MeasurementPollutant m WHERE m.id = :id"),
    @NamedQuery(name = "MeasurementPollutant.findByAverage", query = "SELECT m FROM MeasurementPollutant m WHERE m.average = :average"),
    @NamedQuery(name = "MeasurementPollutant.findByCreated", query = "SELECT m FROM MeasurementPollutant m WHERE m.created = :created"),
    @NamedQuery(name = "MeasurementPollutant.findByUpdated", query = "SELECT m FROM MeasurementPollutant m WHERE m.updated = :updated")})
public class MeasurementPollutant implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "average")
    private float average;
    @Basic(optional = false)
    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @Column(name = "updated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;
    @JoinColumn(name = "measurement_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Measurement measurementId;
    @JoinColumn(name = "pollutant_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Pollutant pollutantId;

    public MeasurementPollutant() {
    }

    public MeasurementPollutant(Integer id) {
        this.id = id;
    }

    public MeasurementPollutant(Integer id, float average, Date created) {
        this.id = id;
        this.average = average;
        this.created = created;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public float getAverage() {
        return average;
    }

    public void setAverage(float average) {
        this.average = average;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Measurement getMeasurementId() {
        return measurementId;
    }

    public void setMeasurementId(Measurement measurementId) {
        this.measurementId = measurementId;
    }

    public Pollutant getPollutantId() {
        return pollutantId;
    }

    public void setPollutantId(Pollutant pollutantId) {
        this.pollutantId = pollutantId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MeasurementPollutant)) {
            return false;
        }
        MeasurementPollutant other = (MeasurementPollutant) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.pdg.simca.model.MeasurementPollutant[ id=" + id + " ]";
    }
    
}
