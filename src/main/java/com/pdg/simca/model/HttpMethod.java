/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pdg.simca.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author root
 */
@Entity
@Table(name = "http_method")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HttpMethod.findAll", query = "SELECT h FROM HttpMethod h"),
    @NamedQuery(name = "HttpMethod.findById", query = "SELECT h FROM HttpMethod h WHERE h.id = :id"),
    @NamedQuery(name = "HttpMethod.findByName", query = "SELECT h FROM HttpMethod h WHERE h.name = :name"),
    @NamedQuery(name = "HttpMethod.findByCreated", query = "SELECT h FROM HttpMethod h WHERE h.created = :created"),
    @NamedQuery(name = "HttpMethod.findByUpdated", query = "SELECT h FROM HttpMethod h WHERE h.updated = :updated")})
public class HttpMethod implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @Column(name = "updated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "httpMethodId")
    private List<ResourceAuthorization> resourceAuthorizationList;

    public HttpMethod() {
    }

    public HttpMethod(Integer id) {
        this.id = id;
    }

    public HttpMethod(Integer id, String name, Date created) {
        this.id = id;
        this.name = name;
        this.created = created;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @XmlTransient
    public List<ResourceAuthorization> getResourceAuthorizationList() {
        return resourceAuthorizationList;
    }

    public void setResourceAuthorizationList(List<ResourceAuthorization> resourceAuthorizationList) {
        this.resourceAuthorizationList = resourceAuthorizationList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HttpMethod)) {
            return false;
        }
        HttpMethod other = (HttpMethod) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.pdg.simca.model.HttpMethod[ id=" + id + " ]";
    }
    
}
