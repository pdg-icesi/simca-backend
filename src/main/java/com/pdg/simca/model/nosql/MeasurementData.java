package com.pdg.simca.model.nosql;

import org.springframework.data.annotation.Id;

import com.mongodb.BasicDBObject;

public class MeasurementData {

	@Id
	private Integer id;

	private BasicDBObject data;

	public MeasurementData() {
		data = new BasicDBObject();
	}

	public MeasurementData(Integer id) {
		this.id = id;
		data = new BasicDBObject();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BasicDBObject getData() {
		return data;
	}

	public void setData(BasicDBObject data) {
		this.data = data;
	}

}
