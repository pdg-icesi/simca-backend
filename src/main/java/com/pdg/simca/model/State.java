/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pdg.simca.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author root
 */
@Entity
@Table(name = "state")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "State.findAll", query = "SELECT s FROM State s"),
    @NamedQuery(name = "State.findById", query = "SELECT s FROM State s WHERE s.id = :id"),
    @NamedQuery(name = "State.findByName", query = "SELECT s FROM State s WHERE s.name = :name"),
    @NamedQuery(name = "State.findByDescription", query = "SELECT s FROM State s WHERE s.description = :description"),
    @NamedQuery(name = "State.findByCreated", query = "SELECT s FROM State s WHERE s.created = :created"),
    @NamedQuery(name = "State.findByUpdated", query = "SELECT s FROM State s WHERE s.updated = :updated")})
public class State implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @Column(name = "updated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "stateId")
    private List<EmailConfirmation> emailConfirmationList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "stateId")
    private List<PasswordResetRequest> passwordResetRequestList;
    @JoinColumn(name = "state_type_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private StateType stateTypeId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "stateId")
    private List<User> userList;

    public State() {
    }

    public State(Integer id) {
        this.id = id;
    }

    public State(Integer id, String name, String description, Date created) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.created = created;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @XmlTransient
    public List<EmailConfirmation> getEmailConfirmationList() {
        return emailConfirmationList;
    }

    public void setEmailConfirmationList(List<EmailConfirmation> emailConfirmationList) {
        this.emailConfirmationList = emailConfirmationList;
    }

    @XmlTransient
    public List<PasswordResetRequest> getPasswordResetRequestList() {
        return passwordResetRequestList;
    }

    public void setPasswordResetRequestList(List<PasswordResetRequest> passwordResetRequestList) {
        this.passwordResetRequestList = passwordResetRequestList;
    }

    public StateType getStateTypeId() {
        return stateTypeId;
    }

    public void setStateTypeId(StateType stateTypeId) {
        this.stateTypeId = stateTypeId;
    }

    @XmlTransient
    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof State)) {
            return false;
        }
        State other = (State) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.pdg.simca.model.State[ id=" + id + " ]";
    }
    
}
