/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pdg.simca.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author root
 */
@Entity
@Table(name = "resource_authorization")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ResourceAuthorization.findAll", query = "SELECT r FROM ResourceAuthorization r"),
    @NamedQuery(name = "ResourceAuthorization.findById", query = "SELECT r FROM ResourceAuthorization r WHERE r.id = :id"),
    @NamedQuery(name = "ResourceAuthorization.findByUrlPattern", query = "SELECT r FROM ResourceAuthorization r WHERE r.urlPattern = :urlPattern"),
    @NamedQuery(name = "ResourceAuthorization.findByCreated", query = "SELECT r FROM ResourceAuthorization r WHERE r.created = :created"),
    @NamedQuery(name = "ResourceAuthorization.findByUpdated", query = "SELECT r FROM ResourceAuthorization r WHERE r.updated = :updated")})
public class ResourceAuthorization implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "url_pattern")
    private String urlPattern;
    @Basic(optional = false)
    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @Column(name = "updated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;
    @JoinColumn(name = "http_method_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private HttpMethod httpMethodId;
    @JoinColumn(name = "permission_id", referencedColumnName = "id")
    @ManyToOne
    private Permission permissionId;

    public ResourceAuthorization() {
    }

    public ResourceAuthorization(Integer id) {
        this.id = id;
    }

    public ResourceAuthorization(Integer id, String urlPattern, Date created) {
        this.id = id;
        this.urlPattern = urlPattern;
        this.created = created;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrlPattern() {
        return urlPattern;
    }

    public void setUrlPattern(String urlPattern) {
        this.urlPattern = urlPattern;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public HttpMethod getHttpMethodId() {
        return httpMethodId;
    }

    public void setHttpMethodId(HttpMethod httpMethodId) {
        this.httpMethodId = httpMethodId;
    }

    public Permission getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Permission permissionId) {
        this.permissionId = permissionId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ResourceAuthorization)) {
            return false;
        }
        ResourceAuthorization other = (ResourceAuthorization) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.pdg.simca.model.ResourceAuthorization[ id=" + id + " ]";
    }
    
}
