/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pdg.simca.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author root
 */
@Entity
@Table(name = "pollutant")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pollutant.findAll", query = "SELECT p FROM Pollutant p"),
    @NamedQuery(name = "Pollutant.findById", query = "SELECT p FROM Pollutant p WHERE p.id = :id"),
    @NamedQuery(name = "Pollutant.findByName", query = "SELECT p FROM Pollutant p WHERE p.name = :name"),
    @NamedQuery(name = "Pollutant.findBySymbol", query = "SELECT p FROM Pollutant p WHERE p.symbol = :symbol"),
    @NamedQuery(name = "Pollutant.findByDescription", query = "SELECT p FROM Pollutant p WHERE p.description = :description"),
    @NamedQuery(name = "Pollutant.findByUnits", query = "SELECT p FROM Pollutant p WHERE p.units = :units"),
    @NamedQuery(name = "Pollutant.findByCreated", query = "SELECT p FROM Pollutant p WHERE p.created = :created"),
    @NamedQuery(name = "Pollutant.findByUpdated", query = "SELECT p FROM Pollutant p WHERE p.updated = :updated")})
public class Pollutant implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "symbol")
    private String symbol;
    @Basic(optional = false)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @Column(name = "units")
    private String units;
    @Basic(optional = false)
    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @Column(name = "updated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pollutantId")
    private List<MeasurementPollutant> measurementPollutantList;

    public Pollutant() {
    }

    public Pollutant(Integer id) {
        this.id = id;
    }

    public Pollutant(Integer id, String name, String symbol, String description, String units, Date created) {
        this.id = id;
        this.name = name;
        this.symbol = symbol;
        this.description = description;
        this.units = units;
        this.created = created;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @XmlTransient
    public List<MeasurementPollutant> getMeasurementPollutantList() {
        return measurementPollutantList;
    }

    public void setMeasurementPollutantList(List<MeasurementPollutant> measurementPollutantList) {
        this.measurementPollutantList = measurementPollutantList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pollutant)) {
            return false;
        }
        Pollutant other = (Pollutant) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.pdg.simca.model.Pollutant[ id=" + id + " ]";
    }
    
}
