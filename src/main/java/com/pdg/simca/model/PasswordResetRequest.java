/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pdg.simca.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author root
 */
@Entity
@Table(name = "password_reset_request")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PasswordResetRequest.findAll", query = "SELECT p FROM PasswordResetRequest p"),
    @NamedQuery(name = "PasswordResetRequest.findByToken", query = "SELECT p FROM PasswordResetRequest p WHERE p.token = :token"),
    @NamedQuery(name = "PasswordResetRequest.findByExpiryDate", query = "SELECT p FROM PasswordResetRequest p WHERE p.expiryDate = :expiryDate"),
    @NamedQuery(name = "PasswordResetRequest.findByCreated", query = "SELECT p FROM PasswordResetRequest p WHERE p.created = :created"),
    @NamedQuery(name = "PasswordResetRequest.findByUpdated", query = "SELECT p FROM PasswordResetRequest p WHERE p.updated = :updated")})
public class PasswordResetRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "token")
    private String token;
    @Basic(optional = false)
    @Column(name = "expiry_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expiryDate;
    @Basic(optional = false)
    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @Column(name = "updated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;
    @JoinColumn(name = "state_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private State stateId;
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private User userId;

    public PasswordResetRequest() {
    }

    public PasswordResetRequest(String token) {
        this.token = token;
    }

    public PasswordResetRequest(String token, Date expiryDate, Date created) {
        this.token = token;
        this.expiryDate = expiryDate;
        this.created = created;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public State getStateId() {
        return stateId;
    }

    public void setStateId(State stateId) {
        this.stateId = stateId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (token != null ? token.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PasswordResetRequest)) {
            return false;
        }
        PasswordResetRequest other = (PasswordResetRequest) object;
        if ((this.token == null && other.token != null) || (this.token != null && !this.token.equals(other.token))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.pdg.simca.model.PasswordResetRequest[ token=" + token + " ]";
    }
    
}
