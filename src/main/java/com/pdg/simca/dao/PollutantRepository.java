package com.pdg.simca.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.pdg.simca.model.Pollutant;

public interface PollutantRepository extends JpaRepository<Pollutant, Integer> {

	public Pollutant findByName(@Param("name") String name);

}
