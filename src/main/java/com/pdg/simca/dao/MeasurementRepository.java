package com.pdg.simca.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pdg.simca.model.Measurement;

@Repository
public interface MeasurementRepository extends JpaRepository<Measurement, Integer> {

	public Measurement findById(@Param("idMeasurement") Integer idMeasurement);

	public List<Measurement> findAll();

	public List<Measurement> findByDate(@Param("date") Date date);

	public List<Measurement> findByTime(@Param("time") Date time);

	public List<Measurement> findByDateFilter(@Param("minDate") Date minDate, @Param("maxDate") Date maxDate);

	public List<Measurement> findByTimeFilter(@Param("minTime") Date minTime, @Param("maxTime") Date maxTime);

	public List<Measurement> findByPlaceFilter(@Param("placeIds") List<Integer> placeIds);

	public List<Measurement> findByPollutantFilter(@Param("pollutantIds") List<Integer> pollutantIds);

	public List<Measurement> findByDateTimeFilter(@Param("minDate") Date minDate, @Param("maxDate") Date maxDate,
			@Param("minTime") Date minTime, @Param("maxTime") Date maxTime);

	public List<Measurement> findByDatePlaceFilter(@Param("minDate") Date minDate, @Param("maxDate") Date maxDate,
			@Param("placeIds") List<Integer> placeIds);

	public List<Measurement> findByDatePollutantFilter(@Param("minDate") Date minDate, @Param("maxDate") Date maxDate,
			@Param("pollutantIds") List<Integer> pollutantIds);

	public List<Measurement> findByTimePlaceFilter(@Param("minTime") Date minTime, @Param("maxTime") Date maxTime,
			@Param("placeIds") List<Integer> placeIds);

	public List<Measurement> findByTimePollutantFilter(@Param("minTime") Date minTime, @Param("maxTime") Date maxTime,
			@Param("pollutantIds") List<Integer> pollutantIds);

	public List<Measurement> findByPlacePollutantFilter(@Param("placeIds") List<Integer> placeIds,
			@Param("pollutantIds") List<Integer> pollutantIds);

	public List<Measurement> findByDateTimePlaceFilter(@Param("minDate") Date minDate, @Param("maxDate") Date maxDate,
			@Param("minTime") Date minTime, @Param("maxTime") Date maxTime, @Param("placeIds") List<Integer> placeIds);

	public List<Measurement> findByDateTimePollutantFilter(@Param("minDate") Date minDate,
			@Param("maxDate") Date maxDate, @Param("minTime") Date minTime, @Param("maxTime") Date maxTime,
			@Param("pollutantIds") List<Integer> pollutantIds);

	public List<Measurement> findByDatePlacePollutantFilter(@Param("minDate") Date minDate,
			@Param("maxDate") Date maxDate, @Param("placeIds") List<Integer> placeIds,
			@Param("pollutantIds") List<Integer> pollutantIds);

	public List<Measurement> findByTimePlacePollutantFilter(@Param("minTime") Date minTime,
			@Param("maxTime") Date maxTime, @Param("placeIds") List<Integer> placeIds,
			@Param("pollutantIds") List<Integer> pollutantIds);

	public List<Measurement> findByDateTimePlacePollutantFilter(@Param("minDate") Date minDate,
			@Param("maxDate") Date maxDate, @Param("minTime") Date minTime, @Param("maxTime") Date maxTime,
			@Param("placeIds") List<Integer> placeIds, @Param("pollutantIds") List<Integer> pollutantIds);

}
