package com.pdg.simca.dao.nosql;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.pdg.simca.model.nosql.MeasurementData;

public interface MeasurementDataRepository extends MongoRepository<MeasurementData, Integer> {

}
