package com.pdg.simca.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pdg.simca.model.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {

}
