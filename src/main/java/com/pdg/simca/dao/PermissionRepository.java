package com.pdg.simca.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pdg.simca.model.Permission;

public interface PermissionRepository extends JpaRepository<Permission, Integer> {

}
