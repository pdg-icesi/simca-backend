package com.pdg.simca.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pdg.simca.model.ResourceAuthorization;

public interface ResourceAuthorizationRepository extends JpaRepository<ResourceAuthorization, Integer> {

}
