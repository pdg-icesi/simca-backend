package com.pdg.simca.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pdg.simca.model.UserProfile;

public interface UserProfileRepository extends JpaRepository<UserProfile, String> {

}
