package com.pdg.simca.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pdg.simca.model.Place;

public interface PlaceRepository extends JpaRepository<Place, Integer> {

	List<Place> findAll();
}
