package com.pdg.simca.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pdg.simca.model.RolePermission;

public interface RolePermissionRepository extends JpaRepository<RolePermission, Integer> {

}
