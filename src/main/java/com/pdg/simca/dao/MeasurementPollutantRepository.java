package com.pdg.simca.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.pdg.simca.model.MeasurementPollutant;

public interface MeasurementPollutantRepository extends JpaRepository<MeasurementPollutant, Integer> {

	public MeasurementPollutant findByMeasurementAndPollutant(@Param("measurementId") Integer measurementId,
			@Param("pollutantId") Integer pollutantId);

}
