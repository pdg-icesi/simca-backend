package com.pdg.simca.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.pdg.simca.model.User;

public interface UserRepository extends JpaRepository<User, String> {

	public User findByUsername(@Param("username") String username);

	public User findByEmail(@Param("email") String email);
}
