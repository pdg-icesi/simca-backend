package com.pdg.simca.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;

import com.pdg.simca.model.EmailConfirmation;
import com.pdg.simca.model.User;

public interface EmailConfirmationRepository extends JpaRepository<EmailConfirmation, String> {

	@Modifying
	public void deleteByUser(@Param("user") User user);

}
