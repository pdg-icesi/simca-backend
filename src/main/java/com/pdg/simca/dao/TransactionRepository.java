package com.pdg.simca.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.pdg.simca.model.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Integer> {
	
	public Transaction findByReference(@Param("reference") String reference);

}
