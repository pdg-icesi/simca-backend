package com.pdg.simca.values;

public enum RoleValues {
	
	ROLE_ADMIN(1),
	ROLE_STANDARD(2);
	
	private final Integer id;

	RoleValues(Integer id) {
		this.id = id;
	}

	public Integer id() {
		return id;
	}

}
