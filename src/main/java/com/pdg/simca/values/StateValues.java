package com.pdg.simca.values;

public enum StateValues {

	USER_UNVERIFIED(1),
	USER_ACTIVE(2),
	USER_INACTIVE(3),
	USER_BANNED(4),
	PRR_REQUESTED(5),
	PRR_EXPIRED(6),
	PRR_CONFIRMED(7),
	EMAIL_CONFIRMATION_PENDING(8),
	EMAIL_CONFIRMATION_CONFIRMED(9),
	EMAIL_CONFIRMATION_EXPIRED(10);

	private final Integer id;

	StateValues(Integer id) {
		this.id = id;
	}

	public Integer id() {
		return id;
	}

}
