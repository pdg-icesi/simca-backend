package com.pdg.simca.values;

public class MeasurementValues {
	
	public final static int MODE_NO_FILTER = 0;
	public final static int MODE_DATE = 1;
	public final static int MODE_TIME = 2;
	public final static int MODE_PLACE = 3;
	public final static int MODE_POLLUTANT = 4;
	public final static int MODE_DATE_TIME = 5;
	public final static int MODE_DATE_PLACE = 6;
	public final static int MODE_DATE_POLLUTANT = 7;
	public final static int MODE_TIME_PLACE = 8;
	public final static int MODE_TIME_POLLUTANT = 9;
	public final static int MODE_PLACE_POLLUTANT = 10;
	public final static int MODE_DATE_TIME_PLACE = 11;
	public final static int MODE_DATE_TIME_POLLUTANT = 12;
	public final static int MODE_DATE_PLACE_POLLUTANT = 13;
	public final static int MODE_TIME_PLACE_POLLUTANT = 14;
	public final static int MODE_DATE_TIME_PLACE_POLLUTANT = 15;

}
