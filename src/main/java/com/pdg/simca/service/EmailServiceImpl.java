package com.pdg.simca.service;

import java.io.File;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.pdg.simca.model.EmailConfirmation;
import com.pdg.simca.model.PasswordResetRequest;

@Service
public class EmailServiceImpl implements EmailService {

	@Autowired
	public JavaMailSender emailSender;

	@Async
	@Override
	public void sendSimpleMessage(String[] to, String subject, String text) {
		try {

			SimpleMailMessage message = new SimpleMailMessage();
			message.setTo(to);
			message.setSubject(subject);
			message.setText(text);

			emailSender.send(message);
		} catch (MailException exception) {
			exception.printStackTrace();
		}
	}

	@Async
	@Override
	public void sendSimpleMessageUsingTemplate(String[] to, String subject, SimpleMailMessage template,
			String... templateArgs) {

		String text = String.format(template.getText(), (Object[]) templateArgs);
		sendSimpleMessage(to, subject, text);
	}

	@Async
	@Override
	public void sendMessageWithAttachment(String[] to, String subject, String text, String pathToAttachment) {

		try {

			MimeMessage message = emailSender.createMimeMessage();
			// pass 'true' to the constructor to create a multipart message
			MimeMessageHelper helper = new MimeMessageHelper(message, true);

			helper.setTo(to);
			helper.setSubject(subject);
			helper.setText(text);

			FileSystemResource file = new FileSystemResource(new File(pathToAttachment));
			helper.addAttachment("Invoice", file);

			emailSender.send(message);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	@Async
	@Override
	public void sendConfirmationEmail(EmailConfirmation emailConfirmation) {

		String token = emailConfirmation.getToken();

		String email = emailConfirmation.getUserId().getEmail();

		sendSimpleMessage(new String[] { email }, "Bienvenido a SIMCA",
				"Tu token de confirmación de correo es: " + token);

	}

	@Async
	@Override
	public void sendPasswordResetRequestEmail(PasswordResetRequest passwordResetRequest) {

		String token = passwordResetRequest.getToken();

		String email = passwordResetRequest.getUserId().getEmail();

		sendSimpleMessage(new String[] { email }, "SIMCA - Restauración de contraseña", "El token para restaurar tu contraseña es: " + token);
	}
}