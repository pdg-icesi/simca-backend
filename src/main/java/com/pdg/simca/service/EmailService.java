package com.pdg.simca.service;

import org.springframework.mail.SimpleMailMessage;

import com.pdg.simca.model.EmailConfirmation;
import com.pdg.simca.model.PasswordResetRequest;

public interface EmailService {

	public void sendSimpleMessage(String[] to, String subject, String text);

	public void sendSimpleMessageUsingTemplate(String[] to, String subject, SimpleMailMessage template,
			String... templateArgs);

	public void sendMessageWithAttachment(String[] to, String subject, String text, String pathToAttachment);
	
	public void sendConfirmationEmail(EmailConfirmation emailConfirmation);
	
	public void sendPasswordResetRequestEmail(PasswordResetRequest passwordResetRequest);
}