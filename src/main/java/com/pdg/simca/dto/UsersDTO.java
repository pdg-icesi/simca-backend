package com.pdg.simca.dto;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class UsersDTO {

	@ApiModelProperty(notes = "List of users")
	private List<UserDTO> users;

	public UsersDTO() {
	}

	public UsersDTO(List<UserDTO> users) {
		this.users = users;
	}

	public List<UserDTO> getUsers() {
		return users;
	}

	public void setUsers(List<UserDTO> users) {
		this.users = users;
	}

}
