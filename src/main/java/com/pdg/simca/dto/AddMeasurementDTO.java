package com.pdg.simca.dto;

import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author sdg
 *
 */
public class AddMeasurementDTO {

	@ApiModelProperty(notes = "Creation date of the measurement")
	private Date date;

	@ApiModelProperty(notes = "Creation time of the measurement")
	private Date time;

	@ApiModelProperty(notes = "Place's id where the measurement is made")
	private Integer placeId;

	private List<Integer> pollutantIds;

	public AddMeasurementDTO() {
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getPlaceId() {
		return placeId;
	}

	public void setPlaceId(Integer placeId) {
		this.placeId = placeId;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public List<Integer> getPollutantIds() {
		return pollutantIds;
	}

	public void setPollutantIds(List<Integer> pollutantIds) {
		this.pollutantIds = pollutantIds;
	}

}
