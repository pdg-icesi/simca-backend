package com.pdg.simca.dto;

import io.swagger.annotations.ApiModelProperty;

public class ResponseDTO {

	@ApiModelProperty(notes = "Message of a default response")
	private String message;

	public ResponseDTO() {
	}

	public ResponseDTO(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
