package com.pdg.simca.dto;

import io.swagger.annotations.ApiModelProperty;

public class AddPermissionDTO {

	@ApiModelProperty(notes = "Permission's name or description")
	private String name;
	
	public AddPermissionDTO() {	
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
