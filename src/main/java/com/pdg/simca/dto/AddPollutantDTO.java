package com.pdg.simca.dto;

import io.swagger.annotations.ApiModelProperty;

public class AddPollutantDTO {

	@ApiModelProperty(notes = "Full name of the pollutant")
	private String name;
	
	@ApiModelProperty(notes = "Pollutant's symbol")
	private String symbol;
	
	@ApiModelProperty(notes = "Pollutant's description")
	private String description;
	
	@ApiModelProperty(notes = "Pollutant's units of measurement")
	private String units;
	
	public AddPollutantDTO() {
		
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUnits() {
		return units;
	}
	public void setUnits(String units) {
		this.units = units;
	}

}
