package com.pdg.simca.dto;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

public class UserProfileDTO {

	private String userId;

	@ApiModelProperty(notes = "User's name")
	private String name;

	@ApiModelProperty(notes = "User's lastname")
	private String lastname;

	@ApiModelProperty(notes = "User's birthday: YYYY-MM-DDTHH:mm:ss.GTM or value in milliseconds")
	private Date birthday;

	@ApiModelProperty(notes = "User's residence")
	private CountryDTO country;

	@ApiModelProperty(notes = "Country time zone")
	private TimezoneDTO timezone;

	@ApiModelProperty(notes = "Brief user's description")
	private String bio;

	@ApiModelProperty(notes = "User profile URL")
	private String url;

	@ApiModelProperty(notes = "User profile pictre URL")
	private String profilePicture;

	public UserProfileDTO() {
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public CountryDTO getCountry() {
		return country;
	}

	public void setCountry(CountryDTO country) {
		this.country = country;
	}

	public TimezoneDTO getTimezone() {
		return timezone;
	}

	public void setTimezone(TimezoneDTO timezone) {
		this.timezone = timezone;
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

}
