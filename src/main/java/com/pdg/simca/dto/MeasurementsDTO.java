package com.pdg.simca.dto;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class MeasurementsDTO {
	
	@ApiModelProperty(notes = "List of measurements")
	private List<MeasurementDTO> measurements;
	
	public MeasurementsDTO() {
	}
	
	public MeasurementsDTO(List<MeasurementDTO> measurements) {
		this.measurements = measurements;
	}

	public List<MeasurementDTO> getMeasurements() {
		return measurements;
	}

	public void setMeasurements(List<MeasurementDTO> measurements) {
		this.measurements = measurements;
	}

}
