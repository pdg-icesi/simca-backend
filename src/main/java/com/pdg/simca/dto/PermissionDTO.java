package com.pdg.simca.dto;

import io.swagger.annotations.ApiModelProperty;

public class PermissionDTO {

	@ApiModelProperty(notes = "Autogenerated permission id")
	private Integer id;

	@ApiModelProperty(notes = "Permission name or description")
	private String name;
	
	public PermissionDTO() {	
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
