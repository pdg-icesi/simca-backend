package com.pdg.simca.dto;

import io.swagger.annotations.ApiModelProperty;

public class ResetPasswordDTO {

	@ApiModelProperty(notes = "Reset password request token")
	private String token;

	@ApiModelProperty(notes = "Reset password request password")
	private String password;

	public ResetPasswordDTO() {
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
