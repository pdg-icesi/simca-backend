package com.pdg.simca.dto;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class PermissionsDTO {

	@ApiModelProperty(notes = "List of permissions")
	private List<PermissionDTO> permissions;

	public PermissionsDTO() {
	}
	
	public PermissionsDTO(List<PermissionDTO> permissions) {
		this.permissions = permissions;
	}

	public List<PermissionDTO> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<PermissionDTO> permissions) {
		this.permissions = permissions;
	}

}
