package com.pdg.simca.dto;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class UserProfilesDTO {

	@ApiModelProperty(notes = "List of user profiles")
	private List<UserProfileDTO> profiles;

	public UserProfilesDTO() {
	}
	
	public UserProfilesDTO(List<UserProfileDTO> profiles) {
		this.profiles = profiles;
	}

	public List<UserProfileDTO> getProfiles() {
		return profiles;
	}

	public void setProfiles(List<UserProfileDTO> profiles) {
		this.profiles = profiles;
	}

}
