package com.pdg.simca.dto;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class RolesDTO {

	@ApiModelProperty(notes = "List of roles")
	private List<RoleDTO> roles;

	public RolesDTO() {
	}
	
	public RolesDTO(List<RoleDTO> roles) {
		this.roles = roles;
	}

	public List<RoleDTO> getRoles() {
		return roles;
	}

	public void setRoles(List<RoleDTO> roles) {
		this.roles = roles;
	}

}
