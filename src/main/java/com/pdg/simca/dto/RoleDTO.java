package com.pdg.simca.dto;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class RoleDTO {

	@ApiModelProperty(notes = "Autogenerated role id")
	private Integer id;

	@ApiModelProperty(notes = "Role's name or description")
	private String name;

	@ApiModelProperty(notes = "List of permissions of a role")
	private List<PermissionDTO> permissions;

	public RoleDTO() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<PermissionDTO> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<PermissionDTO> permissions) {
		this.permissions = permissions;
	}

}
