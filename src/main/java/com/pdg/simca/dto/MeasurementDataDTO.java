package com.pdg.simca.dto;

import java.util.Date;
import java.util.HashMap;

public class MeasurementDataDTO {

	private Integer id;

	private HashMap<Date, Float> data;

	public MeasurementDataDTO() {
	}

	public MeasurementDataDTO(Integer id, HashMap<Date, Float> data) {
		this.id = id;
		this.data = data;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public HashMap<Date, Float> getData() {
		return data;
	}

	public void setData(HashMap<Date, Float> data) {
		this.data = data;
	}

}
