package com.pdg.simca.dto;

import io.swagger.annotations.ApiModelProperty;

public class RequestPasswordResetDTO {

	@ApiModelProperty(notes = "User's email who requested password reset")
	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
