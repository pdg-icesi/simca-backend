package com.pdg.simca.dto;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class PollutantsDTO {

	@ApiModelProperty(notes = "List of pollutants")
	public List<PollutantDTO> pollutants;

	public PollutantsDTO() {
	}
	
	public PollutantsDTO(List<PollutantDTO> pollutants) {
		this.pollutants = pollutants;
	}

	public List<PollutantDTO> getPollutants() {
		return pollutants;
	}

	public void setPollutants(List<PollutantDTO> pollutants) {
		this.pollutants = pollutants;
	}

}
