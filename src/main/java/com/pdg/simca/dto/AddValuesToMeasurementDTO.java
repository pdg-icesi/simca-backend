package com.pdg.simca.dto;

import java.util.Date;
import java.util.HashMap;

public class AddValuesToMeasurementDTO {

	private Integer pollutantId;

	private Float average;

	private HashMap<Date, Float> data;

	public AddValuesToMeasurementDTO() {
	}

	public Integer getPollutantId() {
		return pollutantId;
	}

	public void setPollutantId(Integer pollutantId) {
		this.pollutantId = pollutantId;
	}

	public Float getAverage() {
		return average;
	}

	public void setAverage(Float average) {
		this.average = average;
	}

	public HashMap<Date, Float> getData() {
		return data;
	}

	public void setData(HashMap<Date, Float> data) {
		this.data = data;
	}

}
