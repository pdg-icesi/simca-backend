package com.pdg.simca.dto;

import io.swagger.annotations.ApiModelProperty;

public class ErrorDTO {

	@ApiModelProperty(notes = "Error description")
	private String error;

	public ErrorDTO() {
	}
	
	public ErrorDTO(String error) {
		this.error = error;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

}
