package com.pdg.simca.dto;

import io.swagger.annotations.ApiModelProperty;

public class AddPlaceDTO {

	@ApiModelProperty(notes = "Name of the place")
	private String name;

	@ApiModelProperty(notes = "Full address and city name of the place")
	private String address;

	@ApiModelProperty(notes = "Place's geographic coordinate")
	private float latitude;

	@ApiModelProperty(notes = "Place's geographic coordinate")
	private float longitude;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public float getLatitude() {
		return latitude;
	}

	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

	public float getLongitude() {
		return longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}
	
	
	

}
