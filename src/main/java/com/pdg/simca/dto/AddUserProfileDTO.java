package com.pdg.simca.dto;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

public class AddUserProfileDTO {

	@ApiModelProperty(notes = "User's name")
	private String name;

	@ApiModelProperty(notes = "User's lastname")
	private String lastname;

	@ApiModelProperty(notes = "User's birthday: YYYY-MM-DDTHH:mm:ss.GTM or value in milliseconds")
	private Date birthday;

	@ApiModelProperty(notes = "User's residence")
	private Integer country;

	@ApiModelProperty(notes = "Country time zone")
	private Integer timezone;

	@ApiModelProperty(notes = "Brief user's description")
	private String bio;

	@ApiModelProperty(notes = "User profile URL")
	private String url;

	@ApiModelProperty(notes = "User profile pictre URL")
	private String profilePicture;

	public AddUserProfileDTO() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Integer getCountry() {
		return country;
	}

	public void setCountry(Integer country) {
		this.country = country;
	}

	public Integer getTimezone() {
		return timezone;
	}

	public void setTimezone(Integer timezone) {
		this.timezone = timezone;
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

}
