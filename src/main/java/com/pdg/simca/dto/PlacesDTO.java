package com.pdg.simca.dto;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class PlacesDTO {

	@ApiModelProperty(notes = "List of places")
	private List<PlaceDTO> places;
	
	public PlacesDTO(List<PlaceDTO> placesDTOList) {
		this.places= placesDTOList;
	}

	public List<PlaceDTO> getPlaces() {
		return places;
	}

	public void setPlaces(List<PlaceDTO> places) {
		this.places = places;
	}

}
