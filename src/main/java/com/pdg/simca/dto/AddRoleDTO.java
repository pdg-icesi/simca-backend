package com.pdg.simca.dto;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class AddRoleDTO {

	@ApiModelProperty(notes = "Role's name or description")
	private String name;

	@ApiModelProperty(notes = "List of permissions of a role")
	private List<PermissionDTO> permissions;

	public AddRoleDTO() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<PermissionDTO> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<PermissionDTO> permissions) {
		this.permissions = permissions;
	}

}
