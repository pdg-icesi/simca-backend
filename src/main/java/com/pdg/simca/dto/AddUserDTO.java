package com.pdg.simca.dto;

import io.swagger.annotations.ApiModelProperty;

public class AddUserDTO {

	@ApiModelProperty(notes = "Login name in the web and mobile apps")
	private String username;

	@ApiModelProperty(notes = "Login password in the web and mobile apps")
	private String password;

	@ApiModelProperty(notes = "User's email")
	private String email;

	@ApiModelProperty(notes = "User's Profile information")
	private AddUserProfileDTO profile;

	public AddUserDTO() {
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public AddUserProfileDTO getProfile() {
		return profile;
	}

	public void setProfile(AddUserProfileDTO profile) {
		this.profile = profile;
	}

}
