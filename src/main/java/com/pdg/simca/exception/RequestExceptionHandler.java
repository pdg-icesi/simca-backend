package com.pdg.simca.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.pdg.simca.dto.ErrorDTO;

@ControllerAdvice
public class RequestExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = BadRequestException.class)
	protected ResponseEntity<ErrorDTO> handleBadRequestException(BadRequestException e) {
		return new ResponseEntity<ErrorDTO>(new ErrorDTO(e.getMessage()), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(value = NotFoundException.class)
	protected ResponseEntity<ErrorDTO> handleNotFoundException(NotFoundException e) {
		return new ResponseEntity<ErrorDTO>(new ErrorDTO(e.getMessage()), HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(value = ConflictException.class)
	protected ResponseEntity<ErrorDTO> handleConflictException(ConflictException e) {
		return new ResponseEntity<ErrorDTO>(new ErrorDTO(e.getMessage()), HttpStatus.CONFLICT);
	}

}
