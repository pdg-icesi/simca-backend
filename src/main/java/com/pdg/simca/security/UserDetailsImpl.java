package com.pdg.simca.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.pdg.simca.model.Permission;
import com.pdg.simca.model.Role;
import com.pdg.simca.model.RolePermission;
import com.pdg.simca.model.User;
import com.pdg.simca.model.UserRole;
import com.pdg.simca.values.StateValues;

/**
 * Provides a basic implementation of the UserDetails interface
 */
public class UserDetailsImpl implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Collection<? extends GrantedAuthority> authorities;

	private String password;

	private String username;

	private boolean banned;

	private boolean enable;

	public UserDetailsImpl(User user) {
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.authorities = translateUserRolesToGrantedAuthorities(user.getUserRoleList());
		this.enable = user.getStateId().getId() == StateValues.USER_ACTIVE.id() ? true : false;
		this.banned = user.getStateId().getId() == StateValues.USER_BANNED.id() ? true : false;
	}

	/**
	 * Translates the List<Role> to a List<GrantedAuthority>
	 * 
	 * @param roles
	 *            the input list of roles.
	 * @return a list of granted authorities
	 */
	private Collection<? extends GrantedAuthority> translateUserRolesToGrantedAuthorities(List<UserRole> roles) {

		List<GrantedAuthority> authorities = new ArrayList<>();
		for (UserRole userRole : roles) {
			Role role = userRole.getRoleId();

			List<RolePermission> rolePermissions = role.getRolePermissionList();

			for (RolePermission rolePermission : rolePermissions) {
				Permission permission = rolePermission.getPermissionId();

				String name = permission.getName();

				if (!name.startsWith("ROLE_"))
					name = "ROLE_" + name;
				authorities.add(new SimpleGrantedAuthority(name));
			}

		}
		return authorities;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return !banned;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return enable;
	}

}