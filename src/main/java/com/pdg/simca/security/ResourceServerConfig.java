package com.pdg.simca.security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.pdg.simca.dao.ResourceAuthorizationRepository;
import com.pdg.simca.model.Permission;
import com.pdg.simca.model.ResourceAuthorization;

/**
 * The @EnableResourceServer annotation adds a filter of type
 * OAuth2AuthenticationProcessingFilter automatically to the Spring Security
 * filter chain.
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

	@Autowired
    private JsonToUrlEncodedAuthenticationFilter jsonFilter;
	
	@Autowired
	private ResourceAuthorizationRepository resourceAuthorizationRepository;

	@Override
	public void configure(HttpSecurity http) throws Exception {
		
		http.addFilterAfter(jsonFilter, BasicAuthenticationFilter.class);
		
		List<ResourceAuthorization> resourceAuthorizartions = resourceAuthorizationRepository.findAll();
		
		for (ResourceAuthorization resourceAuthorizartion : resourceAuthorizartions) {
			
			HttpMethod method = getHttpMethod(resourceAuthorizartion.getHttpMethodId().getName());
			String pattern = resourceAuthorizartion.getUrlPattern();
			Permission permission = resourceAuthorizartion.getPermissionId();
			
			if (permission == null) {
				http.authorizeRequests().antMatchers(method, pattern).permitAll();
			} else {
				http.authorizeRequests().antMatchers(method, pattern).hasRole(permission.getName());
			}
			
		}

		http.authorizeRequests().antMatchers("/oauth/token").permitAll();
		http.authorizeRequests().antMatchers("/v2/api-docs").permitAll();
		http.authorizeRequests().antMatchers("/swagger-resources/configuration/ui").permitAll();
		http.authorizeRequests().antMatchers("/swagger-resources/configuration/security").permitAll();
		http.authorizeRequests().antMatchers("/swagger-resources").permitAll();
		http.authorizeRequests().antMatchers("/swagger-ui.html").permitAll();
		http.authorizeRequests().antMatchers("/_ah/admin/**").permitAll();
		http.authorizeRequests().anyRequest().denyAll();
	}

	private HttpMethod getHttpMethod(String method) {

		switch (method) {

		case "GET":
			return HttpMethod.GET;
		case "POST":
			return HttpMethod.POST;
		case "PUT":
			return HttpMethod.PUT;
		case "DELETE":
			return HttpMethod.DELETE;

		default:
			return null;
		}

	}

}
