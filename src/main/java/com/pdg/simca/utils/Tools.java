package com.pdg.simca.utils;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.mongodb.BasicDBObject;

public class Tools {

	public static Integer[] convertStringArrayToIntegerArray(String[] array) {

		Integer[] intArray = new Integer[array.length];

		for (int i = 0; i < intArray.length; i++) {
			intArray[i] = Integer.parseInt(array[i]);
		}

		return intArray;
	}
	
	public static <T, K> BasicDBObject convertMapToBasicDBObject(Map<T, K> map) {

		BasicDBObject basicDBObject = new BasicDBObject();

		for (Map.Entry<T, K> entry : map.entrySet()) {
			basicDBObject.put(entry.getKey().toString(), entry.getValue());
		}

		return basicDBObject;
	}

	public static HashMap<Date, Float> convertBasicDBObjectToDataMap(BasicDBObject basicDBObject) {

		HashMap<Date, Float> data = new HashMap<Date, Float>();

		for (Map.Entry<String, Object> entry : basicDBObject.entrySet()) {

			if (entry.getValue() instanceof Float) {
				try {
					data.put(DateUtils.DATE_FORMAT.parse(entry.getKey()), (Float) entry.getValue());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			} else {
				// Internal server error
			}
		}

		return data;
	}

}
