package com.pdg.simca.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.pdg.simca.dto.ErrorDTO;

public class ControllerUtils {

	public static ResponseEntity<ErrorDTO> badRequestResponse(String errorMessage) {
		return new ResponseEntity<ErrorDTO>(new ErrorDTO(errorMessage), HttpStatus.BAD_REQUEST);
	}

	public static ResponseEntity<ErrorDTO> notFoundResponse(String errorMessage) {
		return new ResponseEntity<ErrorDTO>(new ErrorDTO(errorMessage), HttpStatus.NOT_FOUND);
	}

	public static ResponseEntity<ErrorDTO> conflictResponse(String errorMessage) {
		return new ResponseEntity<ErrorDTO>(new ErrorDTO(errorMessage), HttpStatus.CONFLICT);
	}

}
