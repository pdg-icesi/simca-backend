package com.pdg.simca.utils;

import java.util.UUID;

import org.modelmapper.ModelMapper;

public class ModelUtils {

	public static String generateUUID() {
		return UUID.randomUUID().toString();
	}

	public static String generateTokenCode() {
		return UUID.randomUUID().toString().replace("-", "");
	}

	public static <T> T mapObjectToClass(Object obj, Class<T> mapClass, boolean ambiguityIgnored) {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setAmbiguityIgnored(ambiguityIgnored);
		return modelMapper.map(obj, mapClass);
	}

	public static <T> T mapObjectToClass(Object obj, Class<T> mapClass) {
		return new ModelMapper().map(obj, mapClass);
	}

}