package com.pdg.simca.utils;

import com.pdg.simca.utils.Constants;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.validator.routines.UrlValidator;

import com.pdg.simca.dto.AddMeasurementDTO;
import com.pdg.simca.dto.AddPlaceDTO;
import com.pdg.simca.dto.AddPollutantDTO;
import com.pdg.simca.dto.AddTransactionDTO;
import com.pdg.simca.dto.AddUserDTO;
import com.pdg.simca.dto.AddUserProfileDTO;
import com.pdg.simca.dto.CountryDTO;
import com.pdg.simca.dto.PlaceDTO;
import com.pdg.simca.dto.PollutantDTO;
import com.pdg.simca.dto.TimezoneDTO;
import com.pdg.simca.dto.UserProfileDTO;
import com.pdg.simca.exception.BadRequestException;
import com.pdg.simca.exception.ConflictException;

public class ValidationUtils {

	private static String PERMISSION_NAME_REGEX_VALIDATOR = "^[A-Z0-9_-]*$";
	private static String USER_USERNAME_REGEX_VALIDATOR = "^[a-zA-Z0-9_-]{5,15}$";
	private static String USER_PASSWORD_REGEX_VALIDATOR = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d$@$!%* #?&]{8,20}$";
	private static String USER_UUID_REGEX_VALIDATOR = "^[a-f0-9]{8}(-[a-f0-9]{4}){3}-[a-f0-9]{12}$";
	private static String EMAIL_REGEX_VALIDATOR = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
	private static String TOKEN_VALIDATOR = "^[a-f0-9]{32}$";

	public static boolean validatePermissionName(String name) {
		Pattern pattern = Pattern.compile(PERMISSION_NAME_REGEX_VALIDATOR);
		Matcher matcher = pattern.matcher(name);
		return name != null && !name.trim().isEmpty() && matcher.matches();
	}

	public static boolean validateUserId(String uuid) {
		Pattern pattern = Pattern.compile(USER_UUID_REGEX_VALIDATOR);
		Matcher matcher = pattern.matcher(uuid);
		return uuid != null && !uuid.trim().isEmpty() && matcher.matches();
	}

	public static boolean validateUsername(String username) {
		Pattern pattern = Pattern.compile(USER_USERNAME_REGEX_VALIDATOR);
		Matcher matcher = pattern.matcher(username);
		return username != null && !username.trim().isEmpty() && matcher.matches();
	}

	public static boolean validateUserPassword(String password) {
		Pattern pattern = Pattern.compile(USER_PASSWORD_REGEX_VALIDATOR);
		Matcher matcher = pattern.matcher(password);
		return password != null && !password.trim().isEmpty() && matcher.matches();
	}

	public static boolean validateEmail(String email) {
		Pattern pattern = Pattern.compile(EMAIL_REGEX_VALIDATOR, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(email);
		return email != null && !email.trim().isEmpty() && matcher.matches();
	}

	public static boolean validateToken(String token) {
		Pattern pattern = Pattern.compile(TOKEN_VALIDATOR);
		Matcher matcher = pattern.matcher(token);
		return token != null && !token.trim().isEmpty() && matcher.matches();
	}

	public static boolean validateURL(String url) {
		UrlValidator urlValidator = new UrlValidator();
		return url != null && !url.trim().isEmpty() && urlValidator.isValid(url);
	}

	public static boolean validateProfileName(String name) {
		return name != null && !name.trim().isEmpty();
	}

	public static boolean validateProfileLastName(String lastName) {
		return lastName != null && !lastName.trim().isEmpty();
	}

	public static boolean validateProfileBio(String bio) {
		return bio != null && !bio.trim().isEmpty();
	}

	public static boolean validateIntegerId(Integer id) {
		return id > 0;
	}

	public static boolean isValidDateFormat(SimpleDateFormat sdf, String value) {
		Date date = null;
		try {
			date = sdf.parse(value);
			if (!value.equals(sdf.format(date))) {
				date = null;
			}
		} catch (ParseException ex) {
			ex.printStackTrace();
		}
		return date != null;
	}

	public static boolean isValidIntegerList(String integerList) {
		return Pattern.matches("^(\\d+(,\\d+)*)$", integerList);
	}

	public static boolean isValidIntegerNumber(String number) {

		try {
			Integer.parseInt(number);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	public static void validateUserAddition(AddUserDTO addUserDTO) throws BadRequestException, ConflictException {

		String username = addUserDTO.getUsername();

		if (!ValidationUtils.validateUsername(username))
			throw new BadRequestException(
					"Invalid username. It must be from 6 to 15 characters. Allowed characters: letters, numbers and special symbols ('_','-')");

		String email = addUserDTO.getEmail();

		if (!ValidationUtils.validateEmail(email))
			throw new BadRequestException("Invalid email");

		String password = addUserDTO.getPassword();

		if (!ValidationUtils.validateUserPassword(password))
			throw new BadRequestException(
					"Password must contain from 8 to 15 characters. At least one lowercase letter, one uppercase letter and a number");

		AddUserProfileDTO addProfileDTO = addUserDTO.getProfile();

		if (addProfileDTO != null)
			validateUserProfileAddition(addProfileDTO);

	}

	public static void validateUserProfileAddition(AddUserProfileDTO addProfileDTO) throws BadRequestException {

		String name = addProfileDTO.getName();

		if (!ValidationUtils.validateProfileName(name))
			throw new BadRequestException("Invalid profile name");

		String lastName = addProfileDTO.getLastname();

		if (!ValidationUtils.validateProfileLastName(lastName))
			throw new BadRequestException("Invalid profile last name");

		String bio = addProfileDTO.getBio();

		if (!ValidationUtils.validateProfileBio(bio))
			throw new BadRequestException("Invalid profile bio");

		String pictureUrl = addProfileDTO.getProfilePicture();

		if (!ValidationUtils.validateURL(pictureUrl))
			throw new BadRequestException("Invalid profile picture URL");

		String url = addProfileDTO.getUrl();

		if (!ValidationUtils.validateURL(url))
			throw new BadRequestException("Invalid profile URL");

		Integer countryId = addProfileDTO.getCountry();

		if (!ValidationUtils.validateIntegerId(countryId))
			throw new BadRequestException("Invalid country id");

		Integer timezoneId = addProfileDTO.getTimezone();

		if (!ValidationUtils.validateIntegerId(timezoneId))
			throw new BadRequestException("Invalid timezone id");
	}

	public static void validateUserProfileDTO(UserProfileDTO userProfileDTO) throws BadRequestException {

		String name = userProfileDTO.getName();

		if (!ValidationUtils.validateProfileName(name))
			throw new BadRequestException("Invalid profile name");

		String lastName = userProfileDTO.getLastname();

		if (!ValidationUtils.validateProfileLastName(lastName))
			throw new BadRequestException("Invalid profile last name");

		String bio = userProfileDTO.getBio();

		if (!ValidationUtils.validateProfileBio(bio))
			throw new BadRequestException("Invalid profile bio");

		String pictureUrl = userProfileDTO.getProfilePicture();

		if (!ValidationUtils.validateURL(pictureUrl))
			throw new BadRequestException("Invalid profile picture URL");

		String url = userProfileDTO.getUrl();

		if (!ValidationUtils.validateURL(url))
			throw new BadRequestException("Invalid profile URL");

		CountryDTO country = userProfileDTO.getCountry();

		if (!ValidationUtils.validateIntegerId(country.getId()))
			throw new BadRequestException("Invalid country id");

		TimezoneDTO timezone = userProfileDTO.getTimezone();

		if (!ValidationUtils.validateIntegerId(timezone.getId()))
			throw new BadRequestException("Invalid timezone id");
	}

	public static void validateUserProfileEdition(UserProfileDTO userProfileDTO) throws BadRequestException {

		String id = userProfileDTO.getUserId();

		if (!validateUserId(id))
			throw new BadRequestException("Invalid user id");

		validateUserProfileDTO(userProfileDTO);
	}

	public static void validateMeasurementAddition(AddMeasurementDTO addMeasurementDTO) throws BadRequestException {

		Integer placeId = addMeasurementDTO.getPlaceId();

		if (placeId == null || placeId <= 0)
			throw new BadRequestException("Invalid place id");

		List<Integer> pollutantIds = addMeasurementDTO.getPollutantIds();

		if (pollutantIds == null || pollutantIds.isEmpty())
			throw new BadRequestException("Pollutant ids list must contain at least one valid pollutant id");

		Date date = addMeasurementDTO.getDate();

		if (date == null)
			throw new BadRequestException("Invalid date");

		Date time = addMeasurementDTO.getTime();

		if (time == null)
			throw new BadRequestException("Invalid time");
	}

	public static void validatePollutantDTO(PollutantDTO pollutantDTO) throws BadRequestException {

		Integer id = pollutantDTO.getId();

		if (!validateIntegerId(id))
			throw new BadRequestException("Invalid pollutant id");

		AddPollutantDTO addPollutantDTO = ModelUtils.mapObjectToClass(pollutantDTO, AddPollutantDTO.class);

		validatePollutantAddition(addPollutantDTO);

	}

	public static void validatePollutantAddition(AddPollutantDTO pollutantDTO) throws BadRequestException {

		String name = pollutantDTO.getName();
		if (!validateSimpleString(name))
			throw new BadRequestException("Invalid pollutant name");

		String symbol = pollutantDTO.getSymbol();
		if (!validateSimpleString(symbol))
			throw new BadRequestException("Invalid pollutant symbol");

		String description = pollutantDTO.getDescription();
		if (!validateSimpleString(description))
			throw new BadRequestException("Invalid pollutant description");

		String units = pollutantDTO.getUnits();
		if (!validateSimpleString(units))
			throw new BadRequestException("Invalid pollutant units");

	}

	public static void validatePlacetDTO(PlaceDTO placeDTO) throws BadRequestException {

		Integer id = placeDTO.getId();

		if (!validateIntegerId(id))
			throw new BadRequestException("Invalid place id");

		AddPlaceDTO addPlaceDTO = ModelUtils.mapObjectToClass(placeDTO, AddPlaceDTO.class);

		validatePlaceAddition(addPlaceDTO);

	}

	public static void validatePlaceAddition(AddPlaceDTO addPlaceDTO) throws BadRequestException {

		String name = addPlaceDTO.getName();
		if (!validateSimpleString(name))
			throw new BadRequestException("Invalid place name");

		String address = addPlaceDTO.getAddress();
		if (!validateSimpleString(address))
			throw new BadRequestException("Invalid place address");

		float latitude = addPlaceDTO.getLatitude();
		float longitude = addPlaceDTO.getLongitude();
		if (!validateLocation(latitude, longitude))
			throw new BadRequestException("Invalid place location");

	}
	
	public static void validateTransactionAddition(AddTransactionDTO addTransactionDTO) throws BadRequestException {
		
		Integer state= addTransactionDTO.getState();
		
		if(!(state.equals(Constants.STATE_APPROVED) || state.equals(Constants.STATE_DECLINED) || state.equals(Constants.STATE_EXPIRED)))
			throw new BadRequestException("Invalid transaction state");
		
		if(!validateSignature(addTransactionDTO))
			throw new BadRequestException("Invalid transaction signature");
		
		if(addTransactionDTO.getValue() < 0)
			throw new BadRequestException("Invalid transaction value");
	}

	public static boolean validateSimpleString(String string) {

		return string != null && !string.trim().isEmpty();
	}

	public static boolean validateLocation(float latitude, float longitude) {
		return (latitude >= -90) && (latitude <= 90) && (longitude >= -180) && (longitude <= 180);
	}
	
	public static boolean validateSignature(AddTransactionDTO addTransactionDTO) {
		
		try {
			
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
			
			String signature = Constants.API_KEY +"~"+
					Constants.MERCHANT_ID+"~"+
					addTransactionDTO.getReference()+"~"+
					addTransactionDTO.getValue()+"~"+
					Constants.CURRENCY+"~"+
					addTransactionDTO.getState();
			
			System.out.println("To calculate: "+signature);
			
			byte[] md5Bytes = messageDigest.digest(signature.getBytes(Charset.forName("UTF-8")));
			
			String md5Signature = new String(md5Bytes);
			System.out.println("Calculated: "+md5Signature);
			
			return md5Signature.equals(addTransactionDTO.getSignature());

			
		} catch (NoSuchAlgorithmException e) {
			
			e.printStackTrace();
		}
		
		
		return true;
	}

}
