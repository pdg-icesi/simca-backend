package com.pdg.simca.utils;

import java.text.SimpleDateFormat;

public class DateUtils {

	public final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
	public final static SimpleDateFormat HOUR_FORMAT = new SimpleDateFormat("HH:mm:ss");

}
