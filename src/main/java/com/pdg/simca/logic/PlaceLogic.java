package com.pdg.simca.logic;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.pdg.simca.dao.PlaceRepository;
import com.pdg.simca.dto.AddPlaceDTO;
import com.pdg.simca.dto.PlaceDTO;
import com.pdg.simca.exception.BadRequestException;
import com.pdg.simca.exception.NotFoundException;
import com.pdg.simca.model.Place;
import com.pdg.simca.utils.ModelUtils;
import com.pdg.simca.utils.ValidationUtils;

@Component
public class PlaceLogic {

	@Autowired
	private PlaceRepository placeRepository;

	/**
	 * 
	 * @param addPlaceDTO
	 * @throws BadRequestException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void addPlace(AddPlaceDTO addPlaceDTO) throws BadRequestException {

		ValidationUtils.validatePlaceAddition(addPlaceDTO);

		Place place = ModelUtils.mapObjectToClass(addPlaceDTO, Place.class);
		place.setCreated(new Date());
		placeRepository.save(place);
	}

	/**
	 * 
	 * @param id
	 * @throws NotFoundException
	 * @throws BadRequestException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void deletePlace(Integer id) throws NotFoundException, BadRequestException {

		Place place = placeRepository.findOne(id);

		if (place == null)
			throw new NotFoundException("No place found with specified id");

		placeRepository.delete(id);
	}

	/**
	 * 
	 * @param placeDTO
	 * @throws NotFoundException
	 * @throws BadRequestException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void editPlace(PlaceDTO placeDTO) throws NotFoundException, BadRequestException {

		ValidationUtils.validatePlacetDTO(placeDTO);

		Integer id = placeDTO.getId();

		Place placeFound = placeRepository.findOne(id);

		if (placeFound == null)
			throw new NotFoundException("No place found with specified id");

		Place place = ModelUtils.mapObjectToClass(placeDTO, Place.class);
		place.setCreated(placeFound.getCreated());
		place.setUpdated(new Date());
		placeRepository.save(place);
	}

	/**
	 * 
	 * @param id
	 * @return Place
	 * @throws NotFoundException
	 * @throws BadRequestException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public Place getPlace(Integer id) throws NotFoundException, BadRequestException {

		if (!ValidationUtils.validateIntegerId(id))
			throw new BadRequestException("Invalid place id");

		Place place = placeRepository.findOne(id);

		if (place == null)
			throw new NotFoundException("No place found with specified id");

		return place;
	}

	/**
	 * 
	 * @return List<Place>
	 * @throws NotFoundException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public List<Place> getPlaces() throws NotFoundException {
		return placeRepository.findAll();
	}

}
