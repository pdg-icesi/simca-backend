package com.pdg.simca.logic;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.pdg.simca.dao.TransactionRepository;
import com.pdg.simca.dao.UserRepository;
import com.pdg.simca.dto.AddTransactionDTO;
import com.pdg.simca.exception.BadRequestException;
import com.pdg.simca.exception.ConflictException;
import com.pdg.simca.model.Transaction;
import com.pdg.simca.utils.ModelUtils;
import com.pdg.simca.utils.ValidationUtils;

@Component
public class TransactionLogic {

	@Autowired
	private TransactionRepository transactionRepository;
	
	@Autowired
	private UserRepository UserRepository;
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void addTransaction(AddTransactionDTO addTransactionDTO) throws BadRequestException, ConflictException {
		
		ValidationUtils.validateTransactionAddition(addTransactionDTO);
		
		if(transactionRepository.findByReference(addTransactionDTO.getReference()) != null)
			throw new ConflictException("There is a transaction with the same reference code");
		
		if(UserRepository.findOne(addTransactionDTO.getUserId()) == null)
			throw new BadRequestException("The user doesn't exist");
		
		Transaction transaction = ModelUtils.mapObjectToClass(addTransactionDTO, Transaction.class);
		transaction.setCreated(new Date());
		transactionRepository.save(transaction);
	}

}
