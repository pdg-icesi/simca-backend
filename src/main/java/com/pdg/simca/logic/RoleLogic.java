package com.pdg.simca.logic;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.pdg.simca.dao.RoleRepository;
import com.pdg.simca.dto.AddRoleDTO;
import com.pdg.simca.dto.RoleDTO;
import com.pdg.simca.exception.BadRequestException;
import com.pdg.simca.exception.NotFoundException;
import com.pdg.simca.model.Permission;
import com.pdg.simca.model.Role;
import com.pdg.simca.model.RolePermission;
import com.pdg.simca.utils.ModelUtils;

@Service
@Scope("singleton")
public class RoleLogic {

	@Autowired
	private RoleRepository roleRepository;

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public List<Role> getRoles() {
		return roleRepository.findAll();
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public Role getRole(Integer id) throws BadRequestException, NotFoundException {
		return roleRepository.findOne(id);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void addRole(AddRoleDTO addRoleDTO) throws BadRequestException {
		Role role = ModelUtils.mapObjectToClass(addRoleDTO, Role.class);
		roleRepository.save(role);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void editRole(RoleDTO roleDTO) throws BadRequestException, NotFoundException {
		Role role = ModelUtils.mapObjectToClass(roleDTO, Role.class);
		roleRepository.save(role);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void deleteRole(Integer id) {
		roleRepository.delete(id);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void addPermissionsToRole(Integer roleId, List<Integer> permissionIds)
			throws BadRequestException, NotFoundException {

		Role role = roleRepository.findOne(roleId);
		List<RolePermission> rolePermissions = role.getRolePermissionList();
		for (Integer permissionId : permissionIds) {

			RolePermission rolePermission = new RolePermission();
			rolePermission.setRoleId(new Role(roleId));
			rolePermission.setPermissionId(new Permission(permissionId));
			rolePermission.setCreated(new Date());

			rolePermissions.add(rolePermission);
		}
		role.setRolePermissionList(rolePermissions);
		roleRepository.save(role);
	}

}
