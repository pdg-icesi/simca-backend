package com.pdg.simca.logic;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import com.pdg.simca.dao.UserRepository;
import com.pdg.simca.dto.AddUserDTO;
import com.pdg.simca.dto.AddUserProfileDTO;
import com.pdg.simca.dto.RequestPasswordResetDTO;
import com.pdg.simca.dto.ResetPasswordDTO;
import com.pdg.simca.exception.BadRequestException;
import com.pdg.simca.exception.ConflictException;
import com.pdg.simca.exception.NotFoundException;
import com.pdg.simca.model.Country;
import com.pdg.simca.model.EmailConfirmation;
import com.pdg.simca.model.PasswordResetRequest;
import com.pdg.simca.model.Role;
import com.pdg.simca.model.State;
import com.pdg.simca.model.Timezone;
import com.pdg.simca.model.User;
import com.pdg.simca.model.UserProfile;
import com.pdg.simca.model.UserRole;
import com.pdg.simca.service.EmailService;
import com.pdg.simca.utils.ModelUtils;
import com.pdg.simca.utils.ValidationUtils;
import com.pdg.simca.values.DateValues;
import com.pdg.simca.values.RoleValues;
import com.pdg.simca.values.StateValues;

@Service
@Scope("singleton")
public class UserLogic {
	
	@Autowired
	private EmailService emailService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private EmailConfirmationLogic emailConfirmationLogic;

	@Autowired
	private PasswordResetRequestLogic passwordResetRequestLogic;

	@Autowired
	private UserRepository userRepository;

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public List<User> getAll() {
		List<User> users = userRepository.findAll();
		return users == null ? new ArrayList<User>() : users;
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public User get(String id) throws BadRequestException, NotFoundException {

		if (!ValidationUtils.validateUserId(id))
			throw new BadRequestException("Invalid user id");

		User user = userRepository.findOne(id);

		if (user == null)
			throw new NotFoundException("No user found with specified id");

		return user;
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public User getByUsername(String userName) throws BadRequestException, NotFoundException {

		if (!ValidationUtils.validateUsername(userName))
			throw new BadRequestException("Invalid user name");

		User user = userRepository.findByUsername(userName);

		if (user == null)
			throw new NotFoundException("No user found with specified username");

		return user;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void add(AddUserDTO addUserDTO) throws BadRequestException, ConflictException {

		ValidationUtils.validateUserAddition(addUserDTO);

		if (userRepository.findByUsername(addUserDTO.getUsername()) != null)
			throw new ConflictException("Username already in use");

		if (userRepository.findByEmail(addUserDTO.getEmail()) != null)
			throw new ConflictException("Email already in use");

		// No conflicts with existing users

		UserProfile userProfile = new UserProfile();

		AddUserProfileDTO addProfileDTO = addUserDTO.getProfile();
		if (addProfileDTO != null) {
			userProfile = ModelUtils.mapObjectToClass(addProfileDTO, UserProfile.class);
			userProfile.setCountryId(new Country(addProfileDTO.getCountry()));
			userProfile.setTimezoneId(new Timezone(addProfileDTO.getTimezone()));
		}

		Date now = new Date();

		User user = ModelUtils.mapObjectToClass(addUserDTO, User.class);

		user.setId(ModelUtils.generateUUID());
		user.setPassword(passwordEncoder.encode(user.getPassword()));

		userProfile.setUserId(user.getId());
		userProfile.setUser(user);
		userProfile.setCreated(now);

		user.setUserProfile(userProfile);
		user.setCreated(now);
		user.setStateId(new State(StateValues.USER_UNVERIFIED.id()));

		List<UserRole> roleList = new ArrayList<UserRole>();

		UserRole userRole = new UserRole();
		userRole.setUserId(user);
		userRole.setRoleId(new Role(RoleValues.ROLE_STANDARD.id()));
		userRole.setCreated(now);

		roleList.add(userRole);

		user.setUserRoleList(roleList);

		EmailConfirmation emailConfirmation = new EmailConfirmation(ModelUtils.generateTokenCode(),
				new Date(now.getTime() + DateValues.DAY_IN_MILLIS*14), now);
		emailConfirmation.setStateId(new State(StateValues.EMAIL_CONFIRMATION_PENDING.id()));
		emailConfirmation.setUserId(user);

		userRepository.save(user);
		EmailConfirmation savedEmailConfirmation = emailConfirmationLogic.add(emailConfirmation);
		
		emailService.sendConfirmationEmail(savedEmailConfirmation);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void delete(String id) throws BadRequestException {

		if (!ValidationUtils.validateUserId(id))
			throw new BadRequestException("Invalid user id");

		User user = userRepository.findOne(id);

		if (user != null)
			userRepository.delete(id);

	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public State checkEmailConfirmationStatus(String token) throws BadRequestException, NotFoundException {

		EmailConfirmation emailConfirmation = emailConfirmationLogic.get(token);
		return emailConfirmation.getStateId();
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public State checkPasswordResetRequestStatus(String token) throws BadRequestException, NotFoundException {

		PasswordResetRequest passwordResetRequest = passwordResetRequestLogic.get(token);
		return passwordResetRequest.getStateId();
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void confirmEmail(String token) throws BadRequestException, NotFoundException, ConflictException {

		EmailConfirmation emailConfirmation = emailConfirmationLogic.get(token);

		if (emailConfirmation.getStateId().getId() == StateValues.EMAIL_CONFIRMATION_EXPIRED.id())
			throw new ConflictException("The email confirmation token has expired");

		emailConfirmation.setStateId(new State(StateValues.EMAIL_CONFIRMATION_CONFIRMED.id()));

		User user = emailConfirmation.getUserId();
		user.setStateId(new State(StateValues.USER_ACTIVE.id()));

		userRepository.save(user);
		emailConfirmationLogic.edit(emailConfirmation);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void resetPassword(ResetPasswordDTO resetPasswordDTO)
			throws BadRequestException, NotFoundException, ConflictException {

		String token = resetPasswordDTO.getToken();
		PasswordResetRequest passwordResetRequest = passwordResetRequestLogic.get(token);

		if (passwordResetRequest.getStateId().getId() == StateValues.PRR_EXPIRED.id())
			throw new ConflictException("The password reset request token has expired");

		if (passwordResetRequest.getStateId().getId() == StateValues.PRR_CONFIRMED.id())
			throw new ConflictException("The password reset request token has already been used");

		passwordResetRequest.setStateId(new State(StateValues.PRR_CONFIRMED.id()));

		String newPassword = resetPasswordDTO.getPassword();

		if (!ValidationUtils.validateUserPassword(newPassword))
			throw new BadRequestException(
					"Password must contain from 8 to 15 characters. At least one lowercase letter, one uppercase letter and a number");

		User user = passwordResetRequest.getUserId();
		user.setPassword(passwordEncoder.encode(newPassword));

		userRepository.save(user);
		passwordResetRequestLogic.edit(passwordResetRequest);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void requestPasswordReset(@RequestBody RequestPasswordResetDTO requestPasswordResetDTO)
			throws BadRequestException, NotFoundException, ConflictException {

		String email = requestPasswordResetDTO.getEmail();

		if (!ValidationUtils.validateEmail(email))
			throw new BadRequestException("Invalid email address");

		User user = userRepository.findByEmail(email);

		if (user == null)
			throw new NotFoundException("No user found with specified email");
		
		if(user.getStateId().getId() == StateValues.USER_UNVERIFIED.id())
			throw new ConflictException("The user has not confirmed it's email address");

		passwordResetRequestLogic.createRequest(user);
	}

}
