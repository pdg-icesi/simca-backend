package com.pdg.simca.logic;

import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mongodb.BasicDBObject;
import com.pdg.simca.dao.MeasurementPollutantRepository;
import com.pdg.simca.dao.nosql.MeasurementDataRepository;
import com.pdg.simca.exception.BadRequestException;
import com.pdg.simca.exception.NotFoundException;
import com.pdg.simca.model.MeasurementPollutant;
import com.pdg.simca.model.nosql.MeasurementData;
import com.pdg.simca.utils.Tools;

@Service
@Scope("singleton")
public class MeasurementDataLogic {

	@Autowired
	private MeasurementLogic measurementLogic;

	@Autowired
	private PollutantLogic pollutantLogic;

	@Autowired
	private MeasurementDataRepository measurementDataRepository;

	@Autowired
	private MeasurementPollutantRepository measurementPollutantRepository;

	public MeasurementData find(Integer measurementId, Integer pollutantId)
			throws BadRequestException, NotFoundException {

		if (measurementId == null || measurementId <= 0)
			throw new BadRequestException("Invalid measurement id");

		if (pollutantId == null || pollutantId <= 0)
			throw new BadRequestException("Invalid pollutant id");

		// Validate if measurement and pollutant exist

		measurementLogic.get(measurementId);
		pollutantLogic.get(pollutantId);

		MeasurementPollutant measurementPollutant = measurementPollutantRepository
				.findByMeasurementAndPollutant(measurementId, pollutantId);

		Integer id = measurementPollutant.getId();

		MeasurementData measurementData = measurementDataRepository.findOne(id);

		if (measurementData == null)
			throw new NotFoundException("There is no data for this pollutant measurement");

		return measurementData;
	}

	public void add(Integer id, HashMap<Date, Float> data) throws BadRequestException {

		if (id == null || id <= 0)
			throw new BadRequestException("Invalid measurement data id");

		MeasurementData measurementData = new MeasurementData(id);
		BasicDBObject basicDBObject = Tools.convertMapToBasicDBObject(data);
		measurementData.setData(basicDBObject);
		
		measurementDataRepository.save(measurementData);
	}

}
