package com.pdg.simca.logic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.pdg.simca.dao.EmailConfirmationRepository;
import com.pdg.simca.exception.BadRequestException;
import com.pdg.simca.exception.NotFoundException;
import com.pdg.simca.model.EmailConfirmation;
import com.pdg.simca.utils.ValidationUtils;

@Service
@Scope("singleton")
public class EmailConfirmationLogic {

	@Autowired
	private EmailConfirmationRepository emailConfirmationRepository;

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public EmailConfirmation get(String token) throws BadRequestException, NotFoundException {

		if (!ValidationUtils.validateToken(token))
			throw new BadRequestException("Invalid token");

		EmailConfirmation emailConfirmation = emailConfirmationRepository.findOne(token);

		if (emailConfirmation == null)
			throw new NotFoundException("No email confirmation found for specified token");

		return emailConfirmation;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public EmailConfirmation add(EmailConfirmation emailConfirmation) {

		return emailConfirmationRepository.save(emailConfirmation);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void edit(EmailConfirmation emailConfirmation) {

		emailConfirmationRepository.save(emailConfirmation);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void delete(String token) {

		emailConfirmationRepository.delete(token);
	}

}
