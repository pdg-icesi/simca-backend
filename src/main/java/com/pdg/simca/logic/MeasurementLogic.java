package com.pdg.simca.logic;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.pdg.simca.dao.MeasurementPollutantRepository;
import com.pdg.simca.dao.MeasurementRepository;
import com.pdg.simca.dao.UserRepository;
import com.pdg.simca.dto.AddMeasurementDTO;
import com.pdg.simca.dto.AddValuesToMeasurementDTO;
import com.pdg.simca.dto.MeasurementDTO;
import com.pdg.simca.exception.BadRequestException;
import com.pdg.simca.exception.NotFoundException;
import com.pdg.simca.model.Measurement;
import com.pdg.simca.model.MeasurementPollutant;
import com.pdg.simca.model.Pollutant;
import com.pdg.simca.model.User;
import com.pdg.simca.utils.DateUtils;
import com.pdg.simca.utils.ModelUtils;
import com.pdg.simca.utils.Tools;
import com.pdg.simca.utils.ValidationUtils;
import com.pdg.simca.values.MeasurementValues;

@Component
public class MeasurementLogic {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PollutantLogic pollutantLogic;
	
	@Autowired
	private MeasurementDataLogic measurementDataLogic;

	@Autowired
	private MeasurementRepository measurementRepository;

	@Autowired
	private MeasurementPollutantRepository measurementPollutantRepository;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void add(AddMeasurementDTO addMeasurementDTO, OAuth2Authentication auth) throws NotFoundException, BadRequestException {

		// Check if pollutant ids are valid and exist
		
		User user = userRepository.findByUsername(auth.getName());

		List<Integer> pollutantIds = addMeasurementDTO.getPollutantIds();
		for (Integer pollutantId : pollutantIds) {
			pollutantLogic.get(pollutantId);
		}

		ValidationUtils.validateMeasurementAddition(addMeasurementDTO);

		Date now = new Date();

		Measurement measurement = ModelUtils.mapObjectToClass(addMeasurementDTO, Measurement.class);
		measurement.setId(null);
		measurement.setUserId(new User(user.getId()));
		measurement.setCreated(now);
		measurement.setMeasurementPollutantList(null);

		// Create measurement
		measurement = measurementRepository.save(measurement);

		List<MeasurementPollutant> measurementPollutants = new ArrayList<MeasurementPollutant>();
		for (Integer pollutantId : pollutantIds) {

			MeasurementPollutant measurementPollutant = new MeasurementPollutant();
			measurementPollutant.setPollutantId(new Pollutant(pollutantId));
			measurementPollutant.setMeasurementId(measurement);
			measurementPollutant.setCreated(now);
			measurementPollutants.add(measurementPollutant);
		}
		measurement.setMeasurementPollutantList(measurementPollutants);

		// Update measurement with its pollutants (no setting updated needed as is not
		// an update operation)
		measurementRepository.save(measurement);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void edit(MeasurementDTO measurementDTO) throws NotFoundException, BadRequestException {

		Measurement measurement = ModelUtils.mapObjectToClass(measurementDTO, Measurement.class);

		Integer id = measurement.getId();

		if (get(id) == null)
			throw new NotFoundException("No measurement found with specified id");

		measurementRepository.save(measurement);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void delete(Integer idMeasurement) throws BadRequestException {

		if (idMeasurement == null || idMeasurement <= 0)
			throw new BadRequestException("Invalid measurement id");

		Measurement measurement = measurementRepository.findOne(idMeasurement);

		if (measurement != null)
			measurementRepository.delete(measurement);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void addValuesToMeasurement(Integer measurementId, AddValuesToMeasurementDTO addValuesToMeasurementDTO)
			throws NotFoundException, BadRequestException {

		pollutantLogic.get(addValuesToMeasurementDTO.getPollutantId());

		Float average = addValuesToMeasurementDTO.getAverage();

		if (average == null)
			throw new BadRequestException("Invalid average value");

		HashMap<Date, Float> data = addValuesToMeasurementDTO.getData();

		if (data.isEmpty())
			throw new BadRequestException("Data can not be empty");

		MeasurementPollutant measurementPollutant = measurementPollutantRepository
				.findByMeasurementAndPollutant(measurementId, addValuesToMeasurementDTO.getPollutantId());

		measurementPollutant.setAverage(average);
		measurementPollutant.setUpdated(new Date());

		measurementPollutant = measurementPollutantRepository.save(measurementPollutant);

		measurementDataLogic.add(measurementPollutant.getId(), data);
		
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public List<Measurement> findAll() {
		return measurementRepository.findAll();
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public Measurement get(Integer id) throws NotFoundException, BadRequestException {

		if (id == null)
			throw new BadRequestException("Invalid measurement id (null)");

		Measurement measurement = measurementRepository.findOne(id);

		if (measurement == null)
			throw new NotFoundException("No measurement found with specified id");

		return measurement;
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public List<Measurement> findByFilter(String dateRange, String timeRange, String placesIds, String pollutantsIds)
			throws BadRequestException, ParseException {

		Integer mode = setFilterMode(dateRange, timeRange, placesIds, pollutantsIds);
		System.out.println("MODE: " + mode);

		return findMeasurements(mode, dateRange, timeRange, placesIds, pollutantsIds);
	}

	private Integer setFilterMode(String dateRange, String timeRange, String placesIds, String pollutantsIds) {

		int mode = -1;

		if (dateRange == null && timeRange == null && placesIds == null && pollutantsIds == null)
			mode = MeasurementValues.MODE_NO_FILTER;
		else if (dateRange != null && timeRange == null && placesIds == null && pollutantsIds == null)
			mode = MeasurementValues.MODE_DATE;
		else if (dateRange == null && timeRange != null && placesIds == null && pollutantsIds == null)
			mode = MeasurementValues.MODE_TIME;
		else if (dateRange == null && timeRange == null && placesIds != null && pollutantsIds == null)
			mode = MeasurementValues.MODE_PLACE;
		else if (dateRange == null && timeRange == null && placesIds == null && pollutantsIds != null)
			mode = MeasurementValues.MODE_POLLUTANT;
		else if (dateRange != null && timeRange != null && placesIds == null && pollutantsIds == null)
			mode = MeasurementValues.MODE_DATE_TIME;
		else if (dateRange != null && timeRange == null && placesIds != null && pollutantsIds == null)
			mode = MeasurementValues.MODE_DATE_PLACE;
		else if (dateRange != null && timeRange == null && placesIds == null && pollutantsIds != null)
			mode = MeasurementValues.MODE_DATE_POLLUTANT;
		else if (dateRange == null && timeRange != null && placesIds != null && pollutantsIds == null)
			mode = MeasurementValues.MODE_TIME_PLACE;
		else if (dateRange == null && timeRange != null && placesIds == null && pollutantsIds != null)
			mode = MeasurementValues.MODE_TIME_POLLUTANT;
		else if (dateRange == null && timeRange == null && placesIds != null && pollutantsIds != null)
			mode = MeasurementValues.MODE_PLACE_POLLUTANT;
		else if (dateRange != null && timeRange != null && placesIds != null && pollutantsIds == null)
			mode = MeasurementValues.MODE_DATE_TIME_PLACE;
		else if (dateRange != null && timeRange != null && placesIds == null && pollutantsIds != null)
			mode = MeasurementValues.MODE_DATE_TIME_POLLUTANT;
		else if (dateRange != null && timeRange == null && placesIds != null && pollutantsIds != null)
			mode = MeasurementValues.MODE_DATE_PLACE_POLLUTANT;
		else if (dateRange == null && timeRange != null && placesIds != null && pollutantsIds != null)
			mode = MeasurementValues.MODE_TIME_PLACE_POLLUTANT;
		else if (dateRange != null && timeRange != null && placesIds != null && pollutantsIds != null)
			mode = MeasurementValues.MODE_DATE_TIME_PLACE_POLLUTANT;

		return mode;
	}

	private List<Measurement> findMeasurements(int mode, String dateRange, String timeRange, String placesIds,
			String pollutantsIds) throws BadRequestException, ParseException {

		switch (mode) {

		case MeasurementValues.MODE_NO_FILTER:
			return findAll();

		case MeasurementValues.MODE_DATE:
			return findByDateFilter(dateRange);

		case MeasurementValues.MODE_TIME:
			return findByTimeFilter(timeRange);

		case MeasurementValues.MODE_PLACE:
			return findByPlaceFilter(placesIds);

		case MeasurementValues.MODE_POLLUTANT:
			return findByPollutantFilter(pollutantsIds);

		case MeasurementValues.MODE_DATE_TIME:
			return findByDateTimeFilter(dateRange, timeRange);

		case MeasurementValues.MODE_DATE_PLACE:
			return findByDatePlaceFilter(dateRange, placesIds);

		case MeasurementValues.MODE_DATE_POLLUTANT:
			return findByDatePollutantFilter(dateRange, pollutantsIds);

		case MeasurementValues.MODE_TIME_PLACE:
			return findByTimePlaceFilter(timeRange, placesIds);

		case MeasurementValues.MODE_TIME_POLLUTANT:
			return findByTimePollutantFilter(timeRange, pollutantsIds);

		case MeasurementValues.MODE_PLACE_POLLUTANT:
			return findByPlacePollutantFilter(placesIds, pollutantsIds);

		case MeasurementValues.MODE_DATE_TIME_PLACE:
			return findByTimePlaceFilter(timeRange, placesIds);

		case MeasurementValues.MODE_DATE_TIME_POLLUTANT:
			return findByTimePollutantFilter(timeRange, pollutantsIds);

		case MeasurementValues.MODE_DATE_PLACE_POLLUTANT:
			return findByDatePlacePollutantFilter(dateRange, placesIds, pollutantsIds);

		case MeasurementValues.MODE_TIME_PLACE_POLLUTANT:
			return findByTimePlacePollutantFilter(timeRange, placesIds, pollutantsIds);

		case MeasurementValues.MODE_DATE_TIME_PLACE_POLLUTANT:
			return findByDateTimePlacePollutantFilter(dateRange, timeRange, placesIds, pollutantsIds);
		}

		return null;
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	private List<Measurement> findByDateFilter(String dateFilter) throws BadRequestException, ParseException {

		String[] dateRange = getDateFilterValues(dateFilter);
		String minDate = dateRange[0];
		String maxDate = dateRange[1];

		System.out.println("Min date: " + DateUtils.DATE_FORMAT.parse(minDate).toString());
		return measurementRepository.findByDateFilter(DateUtils.DATE_FORMAT.parse(minDate),
				DateUtils.DATE_FORMAT.parse(maxDate));
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	private List<Measurement> findByTimeFilter(String timeRange) throws BadRequestException, ParseException {

		String[] timeRangeVals = getTimeFilterValues(timeRange);
		String minTime = timeRangeVals[0];
		String maxTime = timeRangeVals[1];

		System.out.println("Min time: " + DateUtils.HOUR_FORMAT.parse(minTime).toString());
		return measurementRepository.findByTimeFilter(DateUtils.HOUR_FORMAT.parse(minTime),
				DateUtils.HOUR_FORMAT.parse(maxTime));
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	private List<Measurement> findByPlaceFilter(String placeIds) throws BadRequestException {

		placeIds = getPlaceFilterValues(placeIds);

		Integer[] intplaceIds = Tools.convertStringArrayToIntegerArray(placeIds.split(","));
		List<Integer> listplaceIds = new ArrayList<Integer>(Arrays.asList(intplaceIds));

		return measurementRepository.findByPlaceFilter(listplaceIds);
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	private List<Measurement> findByPollutantFilter(String pollutantIds) throws BadRequestException {

		pollutantIds = getPollutantFilterValue(pollutantIds);

		Integer[] intpollutantIds = Tools.convertStringArrayToIntegerArray(pollutantIds.split(","));
		List<Integer> listpollutantIds = new ArrayList<Integer>(Arrays.asList(intpollutantIds));

		return measurementRepository.findByPollutantFilter(listpollutantIds);
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	private List<Measurement> findByDateTimeFilter(String dateFilter, String timeRange)
			throws ParseException, BadRequestException {

		String[] dateRange = getDateFilterValues(dateFilter);
		String minDate = dateRange[0];
		String maxDate = dateRange[1];

		String[] timeRangeVals = getTimeFilterValues(timeRange);
		String minTime = timeRangeVals[0];
		String maxTime = timeRangeVals[1];

		return measurementRepository.findByDateTimeFilter(DateUtils.DATE_FORMAT.parse(minDate),
				DateUtils.DATE_FORMAT.parse(maxDate), DateUtils.HOUR_FORMAT.parse(minTime),
				DateUtils.HOUR_FORMAT.parse(maxTime));
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	private List<Measurement> findByDatePlaceFilter(String dateFilter, String placeIds)
			throws ParseException, BadRequestException {

		String[] dateRange = getDateFilterValues(dateFilter);
		String minDate = dateRange[0];
		String maxDate = dateRange[1];

		placeIds = getPlaceFilterValues(placeIds);

		Integer[] intplaceIds = Tools.convertStringArrayToIntegerArray(placeIds.split(","));
		List<Integer> listplaceIds = new ArrayList<Integer>(Arrays.asList(intplaceIds));

		return measurementRepository.findByDatePlaceFilter(DateUtils.DATE_FORMAT.parse(minDate),
				DateUtils.DATE_FORMAT.parse(maxDate), listplaceIds);
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	private List<Measurement> findByDatePollutantFilter(String dateFilter, String pollutantIds)
			throws ParseException, BadRequestException {

		String[] dateRange = getDateFilterValues(dateFilter);
		String minDate = dateRange[0];
		String maxDate = dateRange[1];

		pollutantIds = getPollutantFilterValue(pollutantIds);

		Integer[] intpollutantIds = Tools.convertStringArrayToIntegerArray(pollutantIds.split(","));
		List<Integer> listpollutantIds = new ArrayList<Integer>(Arrays.asList(intpollutantIds));

		return measurementRepository.findByDatePollutantFilter(DateUtils.DATE_FORMAT.parse(minDate),
				DateUtils.DATE_FORMAT.parse(maxDate), listpollutantIds);
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	private List<Measurement> findByTimePlaceFilter(String timeRange, String placeIds)
			throws ParseException, BadRequestException {

		String[] timeRangeVals = getTimeFilterValues(timeRange);
		String minTime = timeRangeVals[0];
		String maxTime = timeRangeVals[1];

		placeIds = getPlaceFilterValues(placeIds);

		Integer[] intplaceIds = Tools.convertStringArrayToIntegerArray(placeIds.split(","));
		List<Integer> listplaceIds = new ArrayList<Integer>(Arrays.asList(intplaceIds));

		return measurementRepository.findByTimePlaceFilter(DateUtils.HOUR_FORMAT.parse(minTime),
				DateUtils.HOUR_FORMAT.parse(maxTime), listplaceIds);
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	private List<Measurement> findByTimePollutantFilter(String timeRange, String pollutantIds)
			throws ParseException, BadRequestException {

		String[] timeRangeVals = getTimeFilterValues(timeRange);
		String minTime = timeRangeVals[0];
		String maxTime = timeRangeVals[1];

		pollutantIds = getPollutantFilterValue(pollutantIds);

		Integer[] intpollutantIds = Tools.convertStringArrayToIntegerArray(pollutantIds.split(","));
		List<Integer> listpollutantIds = new ArrayList<Integer>(Arrays.asList(intpollutantIds));

		return measurementRepository.findByTimePollutantFilter(DateUtils.HOUR_FORMAT.parse(minTime),
				DateUtils.HOUR_FORMAT.parse(maxTime), listpollutantIds);

	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	private List<Measurement> findByPlacePollutantFilter(String placeIds, String pollutantIds)
			throws BadRequestException {

		placeIds = getPlaceFilterValues(placeIds);
		pollutantIds = getPollutantFilterValue(pollutantIds);

		Integer[] intplaceIds = Tools.convertStringArrayToIntegerArray(placeIds.split(","));
		List<Integer> listplaceIds = new ArrayList<Integer>(Arrays.asList(intplaceIds));
		Integer[] intpollutantIds = Tools.convertStringArrayToIntegerArray(pollutantIds.split(","));
		List<Integer> listpollutantIds = new ArrayList<Integer>(Arrays.asList(intpollutantIds));

		return measurementRepository.findByPlacePollutantFilter(listplaceIds, listpollutantIds);

	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	private List<Measurement> findByDateTimePlaceFilter(String dateFilter, String timeRange, String placeIds)
			throws BadRequestException, ParseException {

		String[] dateRange = getDateFilterValues(dateFilter);
		String minDate = dateRange[0];
		String maxDate = dateRange[1];

		String[] timeRangeVals = getTimeFilterValues(timeRange);
		String minTime = timeRangeVals[0];
		String maxTime = timeRangeVals[1];

		placeIds = getPlaceFilterValues(placeIds);

		Integer[] intplaceIds = Tools.convertStringArrayToIntegerArray(placeIds.split(","));
		List<Integer> listplaceIds = new ArrayList<Integer>(Arrays.asList(intplaceIds));

		return measurementRepository.findByDateTimePlaceFilter(DateUtils.DATE_FORMAT.parse(minDate),
				DateUtils.DATE_FORMAT.parse(maxDate), DateUtils.HOUR_FORMAT.parse(minTime),
				DateUtils.HOUR_FORMAT.parse(maxTime), listplaceIds);
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	private List<Measurement> findByDateTimePollutantFilter(String dateFilter, String timeRange, String pollutantIds)
			throws BadRequestException, ParseException {

		String[] dateRange = getDateFilterValues(dateFilter);
		String minDate = dateRange[0];
		String maxDate = dateRange[1];

		String[] timeRangeVals = getTimeFilterValues(timeRange);
		String minTime = timeRangeVals[0];
		String maxTime = timeRangeVals[1];

		pollutantIds = getPollutantFilterValue(pollutantIds);

		Integer[] intpollutantIds = Tools.convertStringArrayToIntegerArray(pollutantIds.split(","));
		List<Integer> listpollutantIds = new ArrayList<Integer>(Arrays.asList(intpollutantIds));

		return measurementRepository.findByDateTimePollutantFilter(DateUtils.DATE_FORMAT.parse(minDate),
				DateUtils.DATE_FORMAT.parse(maxDate), DateUtils.HOUR_FORMAT.parse(minTime),
				DateUtils.HOUR_FORMAT.parse(maxTime), listpollutantIds);

	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	private List<Measurement> findByDatePlacePollutantFilter(String dateFilter, String placeIds, String pollutantIds)
			throws BadRequestException, ParseException {

		String[] dateRange = getDateFilterValues(dateFilter);
		String minDate = dateRange[0];
		String maxDate = dateRange[1];

		placeIds = getPlaceFilterValues(placeIds);
		pollutantIds = getPollutantFilterValue(pollutantIds);

		Integer[] intplaceIds = Tools.convertStringArrayToIntegerArray(placeIds.split(","));
		List<Integer> listplaceIds = new ArrayList<Integer>(Arrays.asList(intplaceIds));
		Integer[] intpollutantIds = Tools.convertStringArrayToIntegerArray(pollutantIds.split(","));
		List<Integer> listpollutantIds = new ArrayList<Integer>(Arrays.asList(intpollutantIds));

		return measurementRepository.findByDatePlacePollutantFilter(DateUtils.DATE_FORMAT.parse(minDate),
				DateUtils.DATE_FORMAT.parse(maxDate), listplaceIds, listpollutantIds);

	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	private List<Measurement> findByTimePlacePollutantFilter(String timeRange, String placeIds, String pollutantIds)
			throws BadRequestException, ParseException {

		String[] timeRangeVals = getTimeFilterValues(timeRange);
		String minTime = timeRangeVals[0];
		String maxTime = timeRangeVals[1];

		placeIds = getPlaceFilterValues(placeIds);
		pollutantIds = getPollutantFilterValue(pollutantIds);

		Integer[] intplaceIds = Tools.convertStringArrayToIntegerArray(placeIds.split(","));
		List<Integer> listplaceIds = new ArrayList<Integer>(Arrays.asList(intplaceIds));
		Integer[] intpollutantIds = Tools.convertStringArrayToIntegerArray(pollutantIds.split(","));
		List<Integer> listpollutantIds = new ArrayList<Integer>(Arrays.asList(intpollutantIds));

		return measurementRepository.findByTimePlacePollutantFilter(DateUtils.HOUR_FORMAT.parse(minTime),
				DateUtils.HOUR_FORMAT.parse(maxTime), listplaceIds, listpollutantIds);

	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	private List<Measurement> findByDateTimePlacePollutantFilter(String dateFilter, String timeRange, String placeIds,
			String pollutantIds) throws BadRequestException, ParseException {

		String[] dateRange = getDateFilterValues(dateFilter);
		String minDate = dateRange[0];
		String maxDate = dateRange[1];

		String[] timeRangeVals = getTimeFilterValues(timeRange);
		String minTime = timeRangeVals[0];
		String maxTime = timeRangeVals[1];

		placeIds = getPlaceFilterValues(placeIds);
		pollutantIds = getPollutantFilterValue(pollutantIds);

		Integer[] intPlaceIds = Tools.convertStringArrayToIntegerArray(placeIds.split(","));
		List<Integer> listPlaceIds = new ArrayList<Integer>(Arrays.asList(intPlaceIds));
		Integer[] intPollutantIds = Tools.convertStringArrayToIntegerArray(pollutantIds.split(","));
		List<Integer> listPollutantIds = new ArrayList<Integer>(Arrays.asList(intPollutantIds));

		return measurementRepository.findByDateTimePlacePollutantFilter(DateUtils.DATE_FORMAT.parse(minDate),
				DateUtils.DATE_FORMAT.parse(maxDate), DateUtils.HOUR_FORMAT.parse(minTime),
				DateUtils.HOUR_FORMAT.parse(maxTime), listPlaceIds, listPollutantIds);

	}

	private String[] getDateFilterValues(String dateFilter) throws BadRequestException {

		if (dateFilter == null || dateFilter.trim().isEmpty())
			throw new BadRequestException("Invalid date filter (null or empty)");

		String[] dateRange = dateFilter.split("x");

		String minDate = "";
		String maxDate = "";

		if (dateRange.length == 1) {

			if (!ValidationUtils.isValidDateFormat(DateUtils.DATE_FORMAT, dateFilter))
				throw new BadRequestException("Invalid date filter format");

			minDate = dateFilter;
			maxDate = dateFilter;

		} else if (dateRange.length == 2) {

			minDate = dateRange[0];
			maxDate = dateRange[1];

			if (minDate == null || minDate.trim().isEmpty()
					|| !ValidationUtils.isValidDateFormat(DateUtils.DATE_FORMAT, minDate))
				throw new BadRequestException("Invalid minimum date");

			if (maxDate == null || maxDate.trim().isEmpty()
					|| !ValidationUtils.isValidDateFormat(DateUtils.DATE_FORMAT, maxDate))
				throw new BadRequestException("Invalid maximum date");

		} else {
			throw new BadRequestException("Invalid date filter. Only one or two dates allowed");
		}

		if (minDate.compareTo(maxDate) > 0)
			throw new BadRequestException("Invalid date range. Max date should be greater than min date.");

		return new String[] { minDate, maxDate };
	}

	private String[] getTimeFilterValues(String timeRange) throws BadRequestException {

		if (timeRange == null || timeRange.trim().isEmpty())
			throw new BadRequestException("Invalid time range (null or empty)");

		String[] timeRangeVals = timeRange.split("x");

		String minTime = "";
		String maxTime = "";

		if (timeRangeVals.length == 2) {

			minTime = timeRangeVals[0];
			maxTime = timeRangeVals[1];

			if (minTime == null || minTime.trim().isEmpty()
					|| !ValidationUtils.isValidDateFormat(DateUtils.HOUR_FORMAT, minTime))
				throw new BadRequestException("Invalid minimum time");

			if (maxTime == null || maxTime.trim().isEmpty()
					|| !ValidationUtils.isValidDateFormat(DateUtils.HOUR_FORMAT, maxTime))
				throw new BadRequestException("Invalid maximum time");

			if (minTime.compareTo(maxTime) > 0)
				throw new BadRequestException("Invalid time range");

		} else {
			throw new BadRequestException("Invalid time filter. Must receive 2 times");
		}

		return new String[] { minTime, maxTime };
	}

	private String getPlaceFilterValues(String placeIds) throws BadRequestException {

		if (placeIds == null || placeIds.trim().isEmpty() || !ValidationUtils.isValidIntegerList(placeIds))
			throw new BadRequestException("Invalid place ids");

		return placeIds;
	}

	private String getPollutantFilterValue(String pollutantIds) throws BadRequestException {

		if (pollutantIds == null || pollutantIds.trim().isEmpty() || !ValidationUtils.isValidIntegerList(pollutantIds))
			throw new BadRequestException("Invalid pollutant ids");

		return pollutantIds;
	}

}
