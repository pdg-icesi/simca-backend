package com.pdg.simca.logic;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.pdg.simca.dao.UserProfileRepository;
import com.pdg.simca.dto.UserProfileDTO;
import com.pdg.simca.exception.BadRequestException;
import com.pdg.simca.exception.NotFoundException;
import com.pdg.simca.model.UserProfile;
import com.pdg.simca.utils.ModelUtils;
import com.pdg.simca.utils.ValidationUtils;

@Service
@Scope("singleton")
public class UserProfileLogic {

	@Autowired
	private UserProfileRepository userProfileRepository;

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public UserProfile getUserProfile(String id) throws BadRequestException {

		if (!ValidationUtils.validateUserId(id))
			throw new BadRequestException("Invalid user id");

		return userProfileRepository.findOne(id);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void editUserProfile(UserProfileDTO userProfileDTO) throws BadRequestException, NotFoundException {

		ValidationUtils.validateUserProfileEdition(userProfileDTO);

		UserProfile userProfileFound = userProfileRepository.findOne(userProfileDTO.getUserId());

		if (userProfileFound == null)
			throw new NotFoundException("No user found with specified id");

		UserProfile userProfile = ModelUtils.mapObjectToClass(userProfileDTO, UserProfile.class, true);
		userProfile.setCreated(userProfileFound.getCreated());
		userProfile.setUpdated(new Date());
		userProfileRepository.save(userProfile);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void delete(String id) {
		userProfileRepository.delete(id);
	}
}
