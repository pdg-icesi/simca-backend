package com.pdg.simca.logic;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.pdg.simca.dao.PollutantRepository;
import com.pdg.simca.dto.AddPollutantDTO;
import com.pdg.simca.dto.PollutantDTO;
import com.pdg.simca.exception.BadRequestException;
import com.pdg.simca.exception.ConflictException;
import com.pdg.simca.exception.NotFoundException;
import com.pdg.simca.model.Pollutant;
import com.pdg.simca.utils.ModelUtils;
import com.pdg.simca.utils.ValidationUtils;

@Component
public class PollutantLogic {

	@Autowired
	private PollutantRepository pollutantRepository;

	/**
	 * 
	 * @param addPollutantDTO
	 * @throws BadRequestException
	 * @throws ConflictException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void addPollutant(AddPollutantDTO addPollutantDTO) throws BadRequestException, ConflictException {

		ValidationUtils.validatePollutantAddition(addPollutantDTO);

		if (pollutantRepository.findByName(addPollutantDTO.getName()) != null)
			throw new ConflictException("Pollutant name already in use");

		Pollutant pollutant = ModelUtils.mapObjectToClass(addPollutantDTO, Pollutant.class);
		pollutant.setCreated(new Date());
		pollutantRepository.save(pollutant);
	}

	/**
	 * 
	 * @param id
	 * @throws NotFoundException
	 * @throws BadRequestException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void deletePollutant(Integer id) throws NotFoundException, BadRequestException {

		Pollutant pollutant = pollutantRepository.findOne(id);

		if (pollutant == null)
			throw new NotFoundException("No pollutant found with specified id");

		pollutantRepository.delete(id);
	}

	/**
	 * 
	 * @param pollutantDTO
	 * @throws NotFoundException
	 * @throws BadRequestException
	 * @throws ConflictException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void editPollutant(PollutantDTO pollutantDTO)
			throws BadRequestException, NotFoundException, ConflictException {

		ValidationUtils.validatePollutantDTO(pollutantDTO);

		Integer id = pollutantDTO.getId();

		Pollutant pollutantFound = pollutantRepository.findOne(id);

		if (pollutantFound == null)
			throw new NotFoundException("No pollutant found with specified id");

		Pollutant pollutantByName = pollutantRepository.findByName(pollutantDTO.getName());

		if (pollutantByName != null && pollutantByName.getId() != id)
			throw new ConflictException("Pollutant name already in use");

		Pollutant pollutant = ModelUtils.mapObjectToClass(pollutantDTO, Pollutant.class);
		pollutant.setCreated(pollutantFound.getCreated());
		pollutant.setUpdated(new Date());
		pollutantRepository.save(pollutant);
	}

	/**
	 * 
	 * @param id
	 * @return Pollutant
	 * @throws NotFoundException
	 * @throws BadRequestException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public Pollutant get(Integer id) throws NotFoundException, BadRequestException {

		if (!ValidationUtils.validateIntegerId(id))
			throw new BadRequestException("Invalid pollutant id");

		Pollutant pollutant = pollutantRepository.findOne(id);

		if (pollutant == null)
			throw new NotFoundException("No pollutant found with specified id");

		return pollutant;
	}

	/**
	 * 
	 * @return List<Pollutant
	 * @throws NotFoundException
	 */
	public List<Pollutant> getPollutants() {
		return pollutantRepository.findAll();
	}

}
