package com.pdg.simca.logic;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.pdg.simca.dao.PasswordResetRequestRepository;
import com.pdg.simca.exception.BadRequestException;
import com.pdg.simca.exception.NotFoundException;
import com.pdg.simca.model.PasswordResetRequest;
import com.pdg.simca.model.State;
import com.pdg.simca.model.User;
import com.pdg.simca.service.EmailService;
import com.pdg.simca.utils.ModelUtils;
import com.pdg.simca.utils.ValidationUtils;
import com.pdg.simca.values.DateValues;
import com.pdg.simca.values.StateValues;

@Service
@Scope("singleton")
public class PasswordResetRequestLogic {
	
	@Autowired
	private EmailService emailService;

	@Autowired
	private PasswordResetRequestRepository passwordResetRequestRepository;

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public PasswordResetRequest get(String token) throws BadRequestException, NotFoundException {

		if (!ValidationUtils.validateToken(token))
			throw new BadRequestException("Invalid token");

		PasswordResetRequest passwordResetRequest = passwordResetRequestRepository.findOne(token);

		if (passwordResetRequest == null)
			throw new NotFoundException("No password reset request found with specified id");

		return passwordResetRequest;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void createRequest(User user) {

		// Cancel all active requests

		String token = ModelUtils.generateTokenCode();
		Date now = new Date();
		Date nowPast24h = new Date(now.getTime() + DateValues.DAY_IN_MILLIS);

		PasswordResetRequest passwordResetRequest = new PasswordResetRequest(token, nowPast24h, now);
		passwordResetRequest.setUserId(user);
		passwordResetRequest.setStateId(new State(StateValues.PRR_REQUESTED.id()));

		PasswordResetRequest savedPasswordResetRequest = passwordResetRequestRepository.save(passwordResetRequest);

		emailService.sendPasswordResetRequestEmail(savedPasswordResetRequest);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void edit(PasswordResetRequest passwordResetRequest) {
		passwordResetRequestRepository.save(passwordResetRequest);
	}

}
