package com.pdg.simca.logic;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.pdg.simca.dao.PermissionRepository;
import com.pdg.simca.dto.AddPermissionDTO;
import com.pdg.simca.dto.PermissionDTO;
import com.pdg.simca.exception.BadRequestException;
import com.pdg.simca.exception.NotFoundException;
import com.pdg.simca.model.Permission;
import com.pdg.simca.utils.ModelUtils;
import com.pdg.simca.utils.ValidationUtils;

@Service
@Scope("singleton")
public class PermissionLogic {

	@Autowired
	private PermissionRepository permissionRepository;

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public List<Permission> getPermissions() {
		List<Permission> permissions = permissionRepository.findAll();
		return permissions == null ? new ArrayList<Permission>() : permissions;
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public Permission getPermission(Integer id) throws BadRequestException, NotFoundException {

		if (id <= 0)
			throw new BadRequestException("Id must be a valid number greater than 0");

		Permission permission = permissionRepository.findOne(id);

		if (permission == null)
			throw new NotFoundException("No permission with id " + id + " found");

		return permission;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void addPermission(AddPermissionDTO addPermissionDTO) throws BadRequestException {

		String name = addPermissionDTO.getName();

		if (!ValidationUtils.validatePermissionName(name))
			throw new BadRequestException("Name must contain [_-]][0-9][A-Z (uppercase)] and no spaces");

		Permission permission = ModelUtils.mapObjectToClass(addPermissionDTO, Permission.class);
		permission.setCreated(new Date());
		permissionRepository.save(permission);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void editPermission(PermissionDTO permissionDTO) throws BadRequestException, NotFoundException {

		Integer id = permissionDTO.getId();
		Permission permission = getPermission(id);

		String name = permissionDTO.getName();
		if (!ValidationUtils.validatePermissionName(name))
			throw new BadRequestException("Name must contain [_-]][0-9][A-Z (uppercase)] and no spaces");

		permission.setName(name);
		permission.setUpdated(new Date());
		permissionRepository.save(permission);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void deletePermission(Integer id) throws BadRequestException {

		try {
			Permission permission = getPermission(id);
			permissionRepository.delete(permission);

		} catch (NotFoundException e) {
		}
	}

}
