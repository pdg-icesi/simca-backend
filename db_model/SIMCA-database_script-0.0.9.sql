CREATE DATABASE  IF NOT EXISTS `simca` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `simca`;
-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: simca
-- ------------------------------------------------------
-- Server version	5.7.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `code` varchar(45) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=248 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,'Afghanistan','AF',current_timestamp(),NULL),(2,'Aland Islands','AX',current_timestamp(),NULL),(3,'Albania','AL',current_timestamp(),NULL),(4,'Algeria','DZ',current_timestamp(),NULL),(5,'American Samoa','AS',current_timestamp(),NULL),(6,'Andorra','AD',current_timestamp(),NULL),(7,'Angola','AO',current_timestamp(),NULL),(8,'Anguilla','AI',current_timestamp(),NULL),(9,'Antarctica','AQ',current_timestamp(),NULL),(10,'Antigua and Barbuda','AG',current_timestamp(),NULL),(11,'Argentina','AR',current_timestamp(),NULL),(12,'Armenia','AM',current_timestamp(),NULL),(13,'Aruba','AW',current_timestamp(),NULL),(14,'Australia','AU',current_timestamp(),NULL),(15,'Austria','AT',current_timestamp(),NULL),(16,'Azerbaijan','AZ',current_timestamp(),NULL),(17,'Bahamas','BS',current_timestamp(),NULL),(18,'Bahrain','BH',current_timestamp(),NULL),(19,'Bangladesh','BD',current_timestamp(),NULL),(20,'Barbados','BB',current_timestamp(),NULL),(21,'Belarus','BY',current_timestamp(),NULL),(22,'Belgium','BE',current_timestamp(),NULL),(23,'Belize','BZ',current_timestamp(),NULL),(24,'Benin','BJ',current_timestamp(),NULL),(25,'Bermuda','BM',current_timestamp(),NULL),(26,'Bhutan','BT',current_timestamp(),NULL),(27,'Bolivia','BO',current_timestamp(),NULL),(28,'Bosnia and Herzegovina','BA',current_timestamp(),NULL),(29,'Botswana','BW',current_timestamp(),NULL),(30,'Bouvet Island','BV',current_timestamp(),NULL),(31,'Brazil','BR',current_timestamp(),NULL),(32,'British Virgin Islands','VG',current_timestamp(),NULL),(33,'British Indian Ocean Territory','IO',current_timestamp(),NULL),(34,'Brunei Darussalam','BN',current_timestamp(),NULL),(35,'Bulgaria','BG',current_timestamp(),NULL),(36,'Burkina Faso','BF',current_timestamp(),NULL),(37,'Burundi','BI',current_timestamp(),NULL),(38,'Cambodia','KH',current_timestamp(),NULL),(39,'Cameroon','CM',current_timestamp(),NULL),(40,'Canada','CA',current_timestamp(),NULL),(41,'Cape Verde','CV',current_timestamp(),NULL),(42,'Cayman Islands','KY',current_timestamp(),NULL),(43,'Central African Republic','CF',current_timestamp(),NULL),(44,'Chad','TD',current_timestamp(),NULL),(45,'Chile','CL',current_timestamp(),NULL),(46,'China','CN',current_timestamp(),NULL),(47,'Hong Kong, SAR China','HK',current_timestamp(),NULL),(48,'Macao, SAR China','MO',current_timestamp(),NULL),(49,'Christmas Island','CX',current_timestamp(),NULL),(50,'Cocos (Keeling) Islands','CC',current_timestamp(),NULL),(51,'Colombia','CO',current_timestamp(),NULL),(52,'Comoros','KM',current_timestamp(),NULL),(53,'Congo (Brazzaville)','CG',current_timestamp(),NULL),(54,'Congo, (Kinshasa)','CD',current_timestamp(),NULL),(55,'Cook Islands','CK',current_timestamp(),NULL),(56,'Costa Rica','CR',current_timestamp(),NULL),(57,'Côte d\'Ivoire','CI',current_timestamp(),NULL),(58,'Croatia','HR',current_timestamp(),NULL),(59,'Cuba','CU',current_timestamp(),NULL),(60,'Cyprus','CY',current_timestamp(),NULL),(61,'Czech Republic','CZ',current_timestamp(),NULL),(62,'Denmark','DK',current_timestamp(),NULL),(63,'Djibouti','DJ',current_timestamp(),NULL),(64,'Dominica','DM',current_timestamp(),NULL),(65,'Dominican Republic','DO',current_timestamp(),NULL),(66,'Ecuador','EC',current_timestamp(),NULL),(67,'Egypt','EG',current_timestamp(),NULL),(68,'El Salvador','SV',current_timestamp(),NULL),(69,'Equatorial Guinea','GQ',current_timestamp(),NULL),(70,'Eritrea','ER',current_timestamp(),NULL),(71,'Estonia','EE',current_timestamp(),NULL),(72,'Ethiopia','ET',current_timestamp(),NULL),(73,'Falkland Islands (Malvinas)','FK',current_timestamp(),NULL),(74,'Faroe Islands','FO',current_timestamp(),NULL),(75,'Fiji','FJ',current_timestamp(),NULL),(76,'Finland','FI',current_timestamp(),NULL),(77,'France','FR',current_timestamp(),NULL),(78,'French Guiana','GF',current_timestamp(),NULL),(79,'French Polynesia','PF',current_timestamp(),NULL),(80,'French Southern Territories','TF',current_timestamp(),NULL),(81,'Gabon','GA',current_timestamp(),NULL),(82,'Gambia','GM',current_timestamp(),NULL),(83,'Georgia','GE',current_timestamp(),NULL),(84,'Germany','DE',current_timestamp(),NULL),(85,'Ghana','GH',current_timestamp(),NULL),(86,'Gibraltar','GI',current_timestamp(),NULL),(87,'Greece','GR',current_timestamp(),NULL),(88,'Greenland','GL',current_timestamp(),NULL),(89,'Grenada','GD',current_timestamp(),NULL),(90,'Guadeloupe','GP',current_timestamp(),NULL),(91,'Guam','GU',current_timestamp(),NULL),(92,'Guatemala','GT',current_timestamp(),NULL),(93,'Guernsey','GG',current_timestamp(),NULL),(94,'Guinea','GN',current_timestamp(),NULL),(95,'Guinea-Bissau','GW',current_timestamp(),NULL),(96,'Guyana','GY',current_timestamp(),NULL),(97,'Haiti','HT',current_timestamp(),NULL),(98,'Heard and Mcdonald Islands','HM',current_timestamp(),NULL),(99,'Holy See (Vatican City State)','VA',current_timestamp(),NULL),(100,'Honduras','HN',current_timestamp(),NULL),(101,'Hungary','HU',current_timestamp(),NULL),(102,'Iceland','IS',current_timestamp(),NULL),(103,'India','IN',current_timestamp(),NULL),(104,'Indonesia','ID',current_timestamp(),NULL),(105,'Iran, Islamic Republic of','IR',current_timestamp(),NULL),(106,'Iraq','IQ',current_timestamp(),NULL),(107,'Ireland','IE',current_timestamp(),NULL),(108,'Isle of Man','IM',current_timestamp(),NULL),(109,'Israel','IL',current_timestamp(),NULL),(110,'Italy','IT',current_timestamp(),NULL),(111,'Jamaica','JM',current_timestamp(),NULL),(112,'Japan','JP',current_timestamp(),NULL),(113,'Jersey','JE',current_timestamp(),NULL),(114,'Jordan','JO',current_timestamp(),NULL),(115,'Kazakhstan','KZ',current_timestamp(),NULL),(116,'Kenya','KE',current_timestamp(),NULL),(117,'Kiribati','KI',current_timestamp(),NULL),(118,'Korea (North)','KP',current_timestamp(),NULL),(119,'Korea (South)','KR',current_timestamp(),NULL),(120,'Kuwait','KW',current_timestamp(),NULL),(121,'Kyrgyzstan','KG',current_timestamp(),NULL),(122,'Lao PDR','LA',current_timestamp(),NULL),(123,'Latvia','LV',current_timestamp(),NULL),(124,'Lebanon','LB',current_timestamp(),NULL),(125,'Lesotho','LS',current_timestamp(),NULL),(126,'Liberia','LR',current_timestamp(),NULL),(127,'Libya','LY',current_timestamp(),NULL),(128,'Liechtenstein','LI',current_timestamp(),NULL),(129,'Lithuania','LT',current_timestamp(),NULL),(130,'Luxembourg','LU',current_timestamp(),NULL),(131,'Macedonia, Republic of','MK',current_timestamp(),NULL),(132,'Madagascar','MG',current_timestamp(),NULL),(133,'Malawi','MW',current_timestamp(),NULL),(134,'Malaysia','MY',current_timestamp(),NULL),(135,'Maldives','MV',current_timestamp(),NULL),(136,'Mali','ML',current_timestamp(),NULL),(137,'Malta','MT',current_timestamp(),NULL),(138,'Marshall Islands','MH',current_timestamp(),NULL),(139,'Martinique','MQ',current_timestamp(),NULL),(140,'Mauritania','MR',current_timestamp(),NULL),(141,'Mauritius','MU',current_timestamp(),NULL),(142,'Mayotte','YT',current_timestamp(),NULL),(143,'Mexico','MX',current_timestamp(),NULL),(144,'Micronesia, Federated States of','FM',current_timestamp(),NULL),(145,'Moldova','MD',current_timestamp(),NULL),(146,'Monaco','MC',current_timestamp(),NULL),(147,'Mongolia','MN',current_timestamp(),NULL),(148,'Montenegro','ME',current_timestamp(),NULL),(149,'Montserrat','MS',current_timestamp(),NULL),(150,'Morocco','MA',current_timestamp(),NULL),(151,'Mozambique','MZ',current_timestamp(),NULL),(152,'Myanmar','MM',current_timestamp(),NULL),(153,'Namibia','NA',current_timestamp(),NULL),(154,'Nauru','NR',current_timestamp(),NULL),(155,'Nepal','NP',current_timestamp(),NULL),(156,'Netherlands','NL',current_timestamp(),NULL),(157,'Netherlands Antilles','AN',current_timestamp(),NULL),(158,'New Caledonia','NC',current_timestamp(),NULL),(159,'New Zealand','NZ',current_timestamp(),NULL),(160,'Nicaragua','NI',current_timestamp(),NULL),(161,'Niger','NE',current_timestamp(),NULL),(162,'Nigeria','NG',current_timestamp(),NULL),(163,'Niue','NU',current_timestamp(),NULL),(164,'Norfolk Island','NF',current_timestamp(),NULL),(165,'Northern Mariana Islands','MP',current_timestamp(),NULL),(166,'Norway','NO',current_timestamp(),NULL),(167,'Oman','OM',current_timestamp(),NULL),(168,'Pakistan','PK',current_timestamp(),NULL),(169,'Palau','PW',current_timestamp(),NULL),(170,'Palestinian Territory','PS',current_timestamp(),NULL),(171,'Panama','PA',current_timestamp(),NULL),(172,'Papua New Guinea','PG',current_timestamp(),NULL),(173,'Paraguay','PY',current_timestamp(),NULL),(174,'Peru','PE',current_timestamp(),NULL),(175,'Philippines','PH',current_timestamp(),NULL),(176,'Pitcairn','PN',current_timestamp(),NULL),(177,'Poland','PL',current_timestamp(),NULL),(178,'Portugal','PT',current_timestamp(),NULL),(179,'Puerto Rico','PR',current_timestamp(),NULL),(180,'Qatar','QA',current_timestamp(),NULL),(181,'Réunion','RE',current_timestamp(),NULL),(182,'Romania','RO',current_timestamp(),NULL),(183,'Russian Federation','RU',current_timestamp(),NULL),(184,'Rwanda','RW',current_timestamp(),NULL),(185,'Saint-Barthélemy','BL',current_timestamp(),NULL),(186,'Saint Helena','SH',current_timestamp(),NULL),(187,'Saint Kitts and Nevis','KN',current_timestamp(),NULL),(188,'Saint Lucia','LC',current_timestamp(),NULL),(189,'Saint-Martin (French part)','MF',current_timestamp(),NULL),(190,'Saint Pierre and Miquelon','PM',current_timestamp(),NULL),(191,'Saint Vincent and Grenadines','VC',current_timestamp(),NULL),(192,'Samoa','WS',current_timestamp(),NULL),(193,'San Marino','SM',current_timestamp(),NULL),(194,'Sao Tome and Principe','ST',current_timestamp(),NULL),(195,'Saudi Arabia','SA',current_timestamp(),NULL),(196,'Senegal','SN',current_timestamp(),NULL),(197,'Serbia','RS',current_timestamp(),NULL),(198,'Seychelles','SC',current_timestamp(),NULL),(199,'Sierra Leone','SL',current_timestamp(),NULL),(200,'Singapore','SG',current_timestamp(),NULL),(201,'Slovakia','SK',current_timestamp(),NULL),(202,'Slovenia','SI',current_timestamp(),NULL),(203,'Solomon Islands','SB',current_timestamp(),NULL),(204,'Somalia','SO',current_timestamp(),NULL),(205,'South Africa','ZA',current_timestamp(),NULL),(206,'South Georgia and the South Sandwich Islands','GS',current_timestamp(),NULL),(207,'South Sudan','SS',current_timestamp(),NULL),(208,'Spain','ES',current_timestamp(),NULL),(209,'Sri Lanka','LK',current_timestamp(),NULL),(210,'Sudan','SD',current_timestamp(),NULL),(211,'Suriname','SR',current_timestamp(),NULL),(212,'Svalbard and Jan Mayen Islands','SJ',current_timestamp(),NULL),(213,'Swaziland','SZ',current_timestamp(),NULL),(214,'Sweden','SE',current_timestamp(),NULL),(215,'Switzerland','CH',current_timestamp(),NULL),(216,'Syrian Arab Republic (Syria)','SY',current_timestamp(),NULL),(217,'Taiwan, Republic of China','TW',current_timestamp(),NULL),(218,'Tajikistan','TJ',current_timestamp(),NULL),(219,'Tanzania, United Republic of','TZ',current_timestamp(),NULL),(220,'Thailand','TH',current_timestamp(),NULL),(221,'Timor-Leste','TL',current_timestamp(),NULL),(222,'Togo','TG',current_timestamp(),NULL),(223,'Tokelau','TK',current_timestamp(),NULL),(224,'Tonga','TO',current_timestamp(),NULL),(225,'Trinidad and Tobago','TT',current_timestamp(),NULL),(226,'Tunisia','TN',current_timestamp(),NULL),(227,'Turkey','TR',current_timestamp(),NULL),(228,'Turkmenistan','TM',current_timestamp(),NULL),(229,'Turks and Caicos Islands','TC',current_timestamp(),NULL),(230,'Tuvalu','TV',current_timestamp(),NULL),(231,'Uganda','UG',current_timestamp(),NULL),(232,'Ukraine','UA',current_timestamp(),NULL),(233,'United Arab Emirates','AE',current_timestamp(),NULL),(234,'United Kingdom','GB',current_timestamp(),NULL),(235,'United States of America','US',current_timestamp(),NULL),(236,'US Minor Outlying Islands','UM',current_timestamp(),NULL),(237,'Uruguay','UY',current_timestamp(),NULL),(238,'Uzbekistan','UZ',current_timestamp(),NULL),(239,'Vanuatu','VU',current_timestamp(),NULL),(240,'Venezuela (Bolivarian Republic)','VE',current_timestamp(),NULL),(241,'Viet Nam','VN',current_timestamp(),NULL),(242,'Virgin Islands, US','VI',current_timestamp(),NULL),(243,'Wallis and Futuna Islands','WF',current_timestamp(),NULL),(244,'Western Sahara','EH',current_timestamp(),NULL),(245,'Yemen','YE',current_timestamp(),NULL),(246,'Zambia','ZM',current_timestamp(),NULL),(247,'Zimbabwe','ZW',current_timestamp(),NULL);
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_confirmation`
--

DROP TABLE IF EXISTS `email_confirmation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_confirmation` (
  `token` varchar(32) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `state_id` int(11) NOT NULL,
  `expiry_date` datetime NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`token`),
  KEY `fk_password_reset_request_state1_idx` (`state_id`),
  KEY `fk_email_confirmation_user1_idx` (`user_id`),
  CONSTRAINT `fk_email_confirmation_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_password_reset_request_state10` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_confirmation`
--

LOCK TABLES `email_confirmation` WRITE;
/*!40000 ALTER TABLE `email_confirmation` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_confirmation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `http_method`
--

DROP TABLE IF EXISTS `http_method`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `http_method` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `http_method`
--

LOCK TABLES `http_method` WRITE;
/*!40000 ALTER TABLE `http_method` DISABLE KEYS */;
INSERT INTO `http_method` VALUES (1,'GET',current_timestamp(),NULL),(2,'POST',current_timestamp(),NULL),(3,'PUT',current_timestamp(),NULL),(4,'DELETE',current_timestamp(),NULL);
/*!40000 ALTER TABLE `http_method` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `measurement`
--

DROP TABLE IF EXISTS `measurement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `measurement` (
  `id` int(11) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `place_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_measurement_place1_idx` (`place_id`),
  KEY `fk_measurement_user1_idx` (`user_id`),
  CONSTRAINT `fk_measurement_place1` FOREIGN KEY (`place_id`) REFERENCES `place` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_measurement_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `measurement`
--

LOCK TABLES `measurement` WRITE;
/*!40000 ALTER TABLE `measurement` DISABLE KEYS */;
/*!40000 ALTER TABLE `measurement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `measurement_pollutant`
--

DROP TABLE IF EXISTS `measurement_pollutant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `measurement_pollutant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `measurement_id` int(11) NOT NULL,
  `pollutant_id` int(11) NOT NULL,
  `average` float NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_measurement_pollutant_measurement1_idx` (`measurement_id`),
  KEY `fk_measurement_pollutant_pollutant1_idx` (`pollutant_id`),
  CONSTRAINT `fk_measurement_pollutant_measurement1` FOREIGN KEY (`measurement_id`) REFERENCES `measurement` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_measurement_pollutant_pollutant1` FOREIGN KEY (`pollutant_id`) REFERENCES `pollutant` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `measurement_pollutant`
--

LOCK TABLES `measurement_pollutant` WRITE;
/*!40000 ALTER TABLE `measurement_pollutant` DISABLE KEYS */;
/*!40000 ALTER TABLE `measurement_pollutant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_reset_request`
--

DROP TABLE IF EXISTS `password_reset_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_reset_request` (
  `token` varchar(32) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `state_id` int(11) NOT NULL,
  `expiry_date` datetime NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`token`),
  KEY `fk_password_reset_request_state1_idx` (`state_id`),
  KEY `fk_password_reset_request_user1_idx` (`user_id`),
  CONSTRAINT `fk_password_reset_request_state1` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_password_reset_request_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_reset_request`
--

LOCK TABLES `password_reset_request` WRITE;
/*!40000 ALTER TABLE `password_reset_request` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_reset_request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission`
--

DROP TABLE IF EXISTS `permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission`
--

LOCK TABLES `permission` WRITE;
/*!40000 ALTER TABLE `permission` DISABLE KEYS */;
INSERT INTO `permission` VALUES (1,'GET_MEASUREMENT_DATA','Fetch a measurement data',current_timestamp(),NULL),(2,'CREATE_MEASUREMENT',NULL,current_timestamp(),NULL),(3,'EDIT_MEASUREMENT',NULL,current_timestamp(),NULL),(4,'DELETE_MEASUREMENT',NULL,current_timestamp(),NULL),(5,'GET_MEASUREMENT_BY_ID',NULL,current_timestamp(),NULL),(6,'LIST_MEASUREMENTS',NULL,current_timestamp(),NULL),(7,'ADD_VALUES_TO_MEASUREMENT',NULL,current_timestamp(),NULL),(8,'CREATE_PLACE',NULL,current_timestamp(),NULL),(9,'EDIT_PLACE',NULL,current_timestamp(),NULL),(10,'DELETE_PLACE',NULL,current_timestamp(),NULL),(11,'GET_PLACE',NULL,current_timestamp(),NULL),(12,'LIST_PLACES',NULL,current_timestamp(),NULL),(13,'CREATE_POLLUTANT',NULL,current_timestamp(),NULL),(14,'EDIT_POLLUTANT',NULL,current_timestamp(),NULL),(15,'DELETE_POLLUTANT',NULL,current_timestamp(),NULL),(16,'GET_POLLUTANT',NULL,current_timestamp(),NULL),(17,'LIST_POLLUTANTS',NULL,current_timestamp(),NULL),(18,'CREATE_TRANSACTION',NULL,current_timestamp(),NULL),(19,'GET_EMAIL_CONFIRMATION_STATUS',NULL,current_timestamp(),NULL),(20,'CONFIRM_EMAIL',NULL,current_timestamp(),NULL),(21,'GET_PASSWORD_RESET_STATUS',NULL,current_timestamp(),NULL),(22,'REQUEST_PASSWORD_RESET',NULL,current_timestamp(),NULL),(23,'RESET_PASSWORD',NULL,current_timestamp(),NULL),(24,'DELETE_USER',NULL,current_timestamp(),NULL),(25,'LIST_USERS',NULL,current_timestamp(),NULL),(26,'CREATE_USER',NULL,current_timestamp(),NULL),(27,'GET_USER_BY_USERNAME',NULL,current_timestamp(),NULL),(28,'GET_USER',NULL,current_timestamp(),NULL),(29,'EDIT_USER_PROFILE','',current_timestamp(),NULL),(30,'GET_USER_PROFILE',NULL,current_timestamp(),NULL);
/*!40000 ALTER TABLE `permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `place`
--

DROP TABLE IF EXISTS `place`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `place` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `place`
--

LOCK TABLES `place` WRITE;
/*!40000 ALTER TABLE `place` DISABLE KEYS */;
/*!40000 ALTER TABLE `place` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pollutant`
--

DROP TABLE IF EXISTS `pollutant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pollutant` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `symbol` varchar(45) NOT NULL,
  `description` varchar(45) NOT NULL,
  `units` varchar(20) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pollutant`
--

LOCK TABLES `pollutant` WRITE;
/*!40000 ALTER TABLE `pollutant` DISABLE KEYS */;
/*!40000 ALTER TABLE `pollutant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resource_authorization`
--

DROP TABLE IF EXISTS `resource_authorization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource_authorization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_id` int(11) DEFAULT NULL,
  `http_method_id` int(11) NOT NULL,
  `url_pattern` varchar(45) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_resource_authorization_permission1_idx` (`permission_id`),
  KEY `fk_resource_authorization_http_method1_idx` (`http_method_id`),
  CONSTRAINT `fk_resource_authorization_http_method1` FOREIGN KEY (`http_method_id`) REFERENCES `http_method` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_resource_authorization_permission1` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resource_authorization`
--

LOCK TABLES `resource_authorization` WRITE;
/*!40000 ALTER TABLE `resource_authorization` DISABLE KEYS */;
INSERT INTO `resource_authorization` VALUES (1,NULL,1,'/api/data/v*/data',current_timestamp(),NULL),(2,2,2,'/api/measurement/v*/measurement',current_timestamp(),NULL),(3,3,3,'/api/measurement/v*/measurement',current_timestamp(),NULL),(4,4,4,'/api/measurement/v*/measurement/*',current_timestamp(),NULL),(5,NULL,1,'/api/measurement/v*/measurement/*',current_timestamp(),NULL),(6,6,2,'/api/measurement/v*/measurement/*/add-values',current_timestamp(),NULL),(7,NULL,1,'/api/measurement/v*/measurements',current_timestamp(),NULL),(8,8,2,'/api/place/v*/place',current_timestamp(),NULL),(9,9,3,'/api/place/v*/place',current_timestamp(),NULL),(10,10,4,'/api/place/v*/place/*',current_timestamp(),NULL),(11,NULL,1,'/api/place/v*/place/*',current_timestamp(),NULL),(12,NULL,1,'/api/place/v*/places',current_timestamp(),NULL),(13,13,2,'/api/pollutant/v*/pollutant',current_timestamp(),NULL),(14,14,3,'/api/pollutant/v*/pollutant',current_timestamp(),NULL),(15,15,4,'/api/pollutant/v*/pollutant/*',current_timestamp(),NULL),(16,NULL,1,'/api/pollutant/v*/pollutant/*',current_timestamp(),NULL),(17,NULL,1,'/api/pollutant/v*/pollutants',current_timestamp(),NULL),(18,NULL,2,'/api/transaction/v*/donation',current_timestamp(),NULL),(19,NULL,1,'/api/user/v*/confirm-email',current_timestamp(),NULL),(20,NULL,3,'/api/user/v*/confirm-email',current_timestamp(),NULL),(21,NULL,1,'/api/user/v*/password-reset',current_timestamp(),NULL),(22,NULL,2,'/api/user/v*/password-reset',current_timestamp(),NULL),(23,NULL,3,'/api/user/v*/password-reset',current_timestamp(),NULL),(24,24,4,'/api/user/v*/user',current_timestamp(),NULL),(25,25,1,'/api/user/v*/users',current_timestamp(),NULL),(26,26,2,'/api/user/v*/user',current_timestamp(),NULL),(27,27,1,'/api/user/v*/user/by-username/*',current_timestamp(),NULL),(28,28,1,'/api/user/v*/user/*',current_timestamp(),NULL),(29,29,3,'/api/userprofile/v*/profile',current_timestamp(),NULL),(30,30,1,'/api/userprofile/v*/profile/*',current_timestamp(),NULL);
/*!40000 ALTER TABLE `resource_authorization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'ADMIN','Administrator user',current_timestamp(),NULL),(2,'STANDARD','Standard user',current_timestamp(),NULL);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_permission`
--

DROP TABLE IF EXISTS `role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_role_has_permission_permission1_idx` (`permission_id`),
  KEY `fk_role_has_permission_role1_idx` (`role_id`),
  CONSTRAINT `fk_role_has_permission_permission1` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_role_has_permission_role1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_permission`
--

LOCK TABLES `role_permission` WRITE;
/*!40000 ALTER TABLE `role_permission` DISABLE KEYS */;
INSERT INTO `role_permission` VALUES (5,2,1,current_timestamp(),NULL),(6,2,2,current_timestamp(),NULL),(7,2,3,current_timestamp(),NULL),(8,2,4,current_timestamp(),NULL),(9,2,5,current_timestamp(),NULL),(10,2,6,current_timestamp(),NULL),(11,2,7,current_timestamp(),NULL),(12,2,8,current_timestamp(),NULL),(13,2,9,current_timestamp(),NULL),(14,2,10,current_timestamp(),NULL),(15,2,11,current_timestamp(),NULL),(16,2,12,current_timestamp(),NULL),(17,2,13,current_timestamp(),NULL),(18,2,14,current_timestamp(),NULL),(19,2,15,current_timestamp(),NULL),(20,2,16,current_timestamp(),NULL),(21,2,17,current_timestamp(),NULL),(22,2,19,current_timestamp(),NULL),(23,2,28,current_timestamp(),NULL),(24,2,29,current_timestamp(),NULL),(25,2,30,current_timestamp(),NULL);
/*!40000 ALTER TABLE `role_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `state`
--

DROP TABLE IF EXISTS `state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `state` (
  `id` int(11) NOT NULL,
  `state_type_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_state_state_type1_idx` (`state_type_id`),
  CONSTRAINT `fk_state_state_type1` FOREIGN KEY (`state_type_id`) REFERENCES `state_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `state`
--

LOCK TABLES `state` WRITE;
/*!40000 ALTER TABLE `state` DISABLE KEYS */;
INSERT INTO `state` VALUES (1,1,'UNCONFIRMED','User has not confirmed it\'s email address',current_timestamp(),NULL),(2,1,'ACTIVE','User is in active state',current_timestamp(),NULL),(3,1,'INACTIVE','User his currently inactive',current_timestamp(),NULL),(4,1,'BANNED','User has been banned',current_timestamp(),NULL),(5,2,'REQUESTED','Password reset confirmation requested',current_timestamp(),NULL),(6,2,'EXPIRED','Password reset confirmation expired',current_timestamp(),NULL),(7,2,'CONFIRMED','Password reset confirmation confirmed',current_timestamp(),NULL),(8,3,'PENDING','Email confirmation pending',current_timestamp(),NULL),(9,3,'CONFIRMED','Email confirmation confirmed',current_timestamp(),NULL),(10,3,'EXPIRED','Email confirmation expired',current_timestamp(),NULL);
/*!40000 ALTER TABLE `state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `state_type`
--

DROP TABLE IF EXISTS `state_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `state_type` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `state_type`
--

LOCK TABLES `state_type` WRITE;
/*!40000 ALTER TABLE `state_type` DISABLE KEYS */;
INSERT INTO `state_type` VALUES (1,'USER_STATE','Defines a user state',current_timestamp(),NULL),(2,'PASSWORD_RESET_REQ_STATE','Defines a password reset request state',current_timestamp(),NULL),(3,'EMAIL_CONFIRMATION_STATE','Defines an email confirmation state',current_timestamp(),NULL);
/*!40000 ALTER TABLE `state_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timezone`
--

DROP TABLE IF EXISTS `timezone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timezone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `utc_offset` varchar(10) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=594 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timezone`
--

LOCK TABLES `timezone` WRITE;
/*!40000 ALTER TABLE `timezone` DISABLE KEYS */;
INSERT INTO `timezone` VALUES (1,'Africa/Abidjan','+00:00',current_timestamp(),NULL),(2,'Africa/Accra','+00:00',current_timestamp(),NULL),(3,'Africa/Addis_Ababa','+03:00',current_timestamp(),NULL),(4,'Africa/Algiers','+01:00',current_timestamp(),NULL),(5,'Africa/Asmara','+03:00',current_timestamp(),NULL),(6,'Africa/Asmera','+03:00',current_timestamp(),NULL),(7,'Africa/Bamako','+00:00',current_timestamp(),NULL),(8,'Africa/Bangui','+01:00',current_timestamp(),NULL),(9,'Africa/Banjul','+00:00',current_timestamp(),NULL),(10,'Africa/Bissau','+00:00',current_timestamp(),NULL),(11,'Africa/Blantyre','+02:00',current_timestamp(),NULL),(12,'Africa/Brazzaville','+01:00',current_timestamp(),NULL),(13,'Africa/Bujumbura','+02:00',current_timestamp(),NULL),(14,'Africa/Cairo','+02:00',current_timestamp(),NULL),(15,'Africa/Casablanca','+00:00',current_timestamp(),NULL),(16,'Africa/Ceuta','+01:00',current_timestamp(),NULL),(17,'Africa/Conakry','+00:00',current_timestamp(),NULL),(18,'Africa/Dakar','+00:00',current_timestamp(),NULL),(19,'Africa/Dar_es_Salaam','+03:00',current_timestamp(),NULL),(20,'Africa/Djibouti','+03:00',current_timestamp(),NULL),(21,'Africa/Douala','+01:00',current_timestamp(),NULL),(22,'Africa/El_Aaiun','+00:00',current_timestamp(),NULL),(23,'Africa/Freetown','+00:00',current_timestamp(),NULL),(24,'Africa/Gaborone','+02:00',current_timestamp(),NULL),(25,'Africa/Harare','+02:00',current_timestamp(),NULL),(26,'Africa/Johannesburg','+02:00',current_timestamp(),NULL),(27,'Africa/Juba','+03:00',current_timestamp(),NULL),(28,'Africa/Kampala','+03:00',current_timestamp(),NULL),(29,'Africa/Khartoum','+03:00',current_timestamp(),NULL),(30,'Africa/Kigali','+02:00',current_timestamp(),NULL),(31,'Africa/Kinshasa','+01:00',current_timestamp(),NULL),(32,'Africa/Lagos','+01:00',current_timestamp(),NULL),(33,'Africa/Libreville','+01:00',current_timestamp(),NULL),(34,'Africa/Lome','+00:00',current_timestamp(),NULL),(35,'Africa/Luanda','+01:00',current_timestamp(),NULL),(36,'Africa/Lubumbashi','+02:00',current_timestamp(),NULL),(37,'Africa/Lusaka','+02:00',current_timestamp(),NULL),(38,'Africa/Malabo','+01:00',current_timestamp(),NULL),(39,'Africa/Maputo','+02:00',current_timestamp(),NULL),(40,'Africa/Maseru','+02:00',current_timestamp(),NULL),(41,'Africa/Mbabane','+02:00',current_timestamp(),NULL),(42,'Africa/Mogadishu','+03:00',current_timestamp(),NULL),(43,'Africa/Monrovia','+00:00',current_timestamp(),NULL),(44,'Africa/Nairobi','+03:00',current_timestamp(),NULL),(45,'Africa/Ndjamena','+01:00',current_timestamp(),NULL),(46,'Africa/Niamey','+01:00',current_timestamp(),NULL),(47,'Africa/Nouakchott','+00:00',current_timestamp(),NULL),(48,'Africa/Ouagadougou','+00:00',current_timestamp(),NULL),(49,'Africa/Porto-Novo','+01:00',current_timestamp(),NULL),(50,'Africa/Sao_Tome','+00:00',current_timestamp(),NULL),(51,'Africa/Timbuktu','+00:00',current_timestamp(),NULL),(52,'Africa/Tripoli','+02:00',current_timestamp(),NULL),(53,'Africa/Tunis','+01:00',current_timestamp(),NULL),(54,'Africa/Windhoek','+01:00',current_timestamp(),NULL),(55,'America/Adak','-10:00',current_timestamp(),NULL),(56,'America/Anchorage','-09:00',current_timestamp(),NULL),(57,'America/Anguilla','-04:00',current_timestamp(),NULL),(58,'America/Antigua','-04:00',current_timestamp(),NULL),(59,'America/Araguaina','-03:00',current_timestamp(),NULL),(60,'America/Argentina/Buenos_Aires','-03:00',current_timestamp(),NULL),(61,'America/Argentina/Catamarca','-03:00',current_timestamp(),NULL),(62,'America/Argentina/ComodRivadavia','-03:00',current_timestamp(),NULL),(63,'America/Argentina/Cordoba','-03:00',current_timestamp(),NULL),(64,'America/Argentina/Jujuy','-03:00',current_timestamp(),NULL),(65,'America/Argentina/La_Rioja','-03:00',current_timestamp(),NULL),(66,'America/Argentina/Mendoza','-03:00',current_timestamp(),NULL),(67,'America/Argentina/Rio_Gallegos','-03:00',current_timestamp(),NULL),(68,'America/Argentina/Salta','-03:00',current_timestamp(),NULL),(69,'America/Argentina/San_Juan','-03:00',current_timestamp(),NULL),(70,'America/Argentina/San_Luis','-03:00',current_timestamp(),NULL),(71,'America/Argentina/Tucuman','-03:00',current_timestamp(),NULL),(72,'America/Argentina/Ushuaia','-03:00',current_timestamp(),NULL),(73,'America/Aruba','-04:00',current_timestamp(),NULL),(74,'America/Asuncion','-04:00',current_timestamp(),NULL),(75,'America/Atikokan','-05:00',current_timestamp(),NULL),(76,'America/Atka','-10:00',current_timestamp(),NULL),(77,'America/Bahia','-03:00',current_timestamp(),NULL),(78,'America/Bahia_Banderas','-06:00',current_timestamp(),NULL),(79,'America/Barbados','-04:00',current_timestamp(),NULL),(80,'America/Belem','-03:00',current_timestamp(),NULL),(81,'America/Belize','-06:00',current_timestamp(),NULL),(82,'America/Blanc-Sablon','-04:00',current_timestamp(),NULL),(83,'America/Boa_Vista','-04:00',current_timestamp(),NULL),(84,'America/Bogota','-05:00',current_timestamp(),NULL),(85,'America/Boise','-07:00',current_timestamp(),NULL),(86,'America/Buenos_Aires','-03:00',current_timestamp(),NULL),(87,'America/Cambridge_Bay','-07:00',current_timestamp(),NULL),(88,'America/Campo_Grande','-04:00',current_timestamp(),NULL),(89,'America/Cancun','-05:00',current_timestamp(),NULL),(90,'America/Caracas','-04:00',current_timestamp(),NULL),(91,'America/Catamarca','-03:00',current_timestamp(),NULL),(92,'America/Cayenne','-03:00',current_timestamp(),NULL),(93,'America/Cayman','-05:00',current_timestamp(),NULL),(94,'America/Chicago','-06:00',current_timestamp(),NULL),(95,'America/Chihuahua','-07:00',current_timestamp(),NULL),(96,'America/Coral_Harbour','-05:00',current_timestamp(),NULL),(97,'America/Cordoba','-03:00',current_timestamp(),NULL),(98,'America/Costa_Rica','-06:00',current_timestamp(),NULL),(99,'America/Creston','-07:00',current_timestamp(),NULL),(100,'America/Cuiaba','-04:00',current_timestamp(),NULL),(101,'America/Curacao','-04:00',current_timestamp(),NULL),(102,'America/Danmarkshavn','+00:00',current_timestamp(),NULL),(103,'America/Dawson','-08:00',current_timestamp(),NULL),(104,'America/Dawson_Creek','-07:00',current_timestamp(),NULL),(105,'America/Denver','-07:00',current_timestamp(),NULL),(106,'America/Detroit','-05:00',current_timestamp(),NULL),(107,'America/Dominica','-04:00',current_timestamp(),NULL),(108,'America/Edmonton','-07:00',current_timestamp(),NULL),(109,'America/Eirunepe','-05:00',current_timestamp(),NULL),(110,'America/El_Salvador','-06:00',current_timestamp(),NULL),(111,'America/Ensenada','-08:00',current_timestamp(),NULL),(112,'America/Fort_Nelson','-07:00',current_timestamp(),NULL),(113,'America/Fort_Wayne','-05:00',current_timestamp(),NULL),(114,'America/Fortaleza','-03:00',current_timestamp(),NULL),(115,'America/Glace_Bay','-04:00',current_timestamp(),NULL),(116,'America/Godthab','-03:00',current_timestamp(),NULL),(117,'America/Goose_Bay','-04:00',current_timestamp(),NULL),(118,'America/Grand_Turk','-04:00',current_timestamp(),NULL),(119,'America/Grenada','-04:00',current_timestamp(),NULL),(120,'America/Guadeloupe','-04:00',current_timestamp(),NULL),(121,'America/Guatemala','-06:00',current_timestamp(),NULL),(122,'America/Guayaquil','-05:00',current_timestamp(),NULL),(123,'America/Guyana','-04:00',current_timestamp(),NULL),(124,'America/Halifax','-04:00',current_timestamp(),NULL),(125,'America/Havana','-05:00',current_timestamp(),NULL),(126,'America/Hermosillo','-07:00',current_timestamp(),NULL),(127,'America/Indiana/Indianapolis','-05:00',current_timestamp(),NULL),(128,'America/Indiana/Knox','-06:00',current_timestamp(),NULL),(129,'America/Indiana/Marengo','-05:00',current_timestamp(),NULL),(130,'America/Indiana/Petersburg','-05:00',current_timestamp(),NULL),(131,'America/Indiana/Tell_City','-06:00',current_timestamp(),NULL),(132,'America/Indiana/Vevay','-05:00',current_timestamp(),NULL),(133,'America/Indiana/Vincennes','-05:00',current_timestamp(),NULL),(134,'America/Indiana/Winamac','-05:00',current_timestamp(),NULL),(135,'America/Indianapolis','-05:00',current_timestamp(),NULL),(136,'America/Inuvik','-07:00',current_timestamp(),NULL),(137,'America/Iqaluit','-05:00',current_timestamp(),NULL),(138,'America/Jamaica','-05:00',current_timestamp(),NULL),(139,'America/Jujuy','-03:00',current_timestamp(),NULL),(140,'America/Juneau','-09:00',current_timestamp(),NULL),(141,'America/Kentucky/Louisville','-05:00',current_timestamp(),NULL),(142,'America/Kentucky/Monticello','-05:00',current_timestamp(),NULL),(143,'America/Knox_IN','-06:00',current_timestamp(),NULL),(144,'America/Kralendijk','-04:00',current_timestamp(),NULL),(145,'America/La_Paz','-04:00',current_timestamp(),NULL),(146,'America/Lima','-05:00',current_timestamp(),NULL),(147,'America/Los_Angeles','-08:00',current_timestamp(),NULL),(148,'America/Louisville','-05:00',current_timestamp(),NULL),(149,'America/Lower_Princes','-04:00',current_timestamp(),NULL),(150,'America/Maceio','-03:00',current_timestamp(),NULL),(151,'America/Managua','-06:00',current_timestamp(),NULL),(152,'America/Manaus','-04:00',current_timestamp(),NULL),(153,'America/Marigot','-04:00',current_timestamp(),NULL),(154,'America/Martinique','-04:00',current_timestamp(),NULL),(155,'America/Matamoros','-06:00',current_timestamp(),NULL),(156,'America/Mazatlan','-07:00',current_timestamp(),NULL),(157,'America/Mendoza','-03:00',current_timestamp(),NULL),(158,'America/Menominee','-06:00',current_timestamp(),NULL),(159,'America/Merida','-06:00',current_timestamp(),NULL),(160,'America/Metlakatla','-09:00',current_timestamp(),NULL),(161,'America/Mexico_City','-06:00',current_timestamp(),NULL),(162,'America/Miquelon','-03:00',current_timestamp(),NULL),(163,'America/Moncton','-04:00',current_timestamp(),NULL),(164,'America/Monterrey','-06:00',current_timestamp(),NULL),(165,'America/Montevideo','-03:00',current_timestamp(),NULL),(166,'America/Montreal','-05:00',current_timestamp(),NULL),(167,'America/Montserrat','-04:00',current_timestamp(),NULL),(168,'America/Nassau','-05:00',current_timestamp(),NULL),(169,'America/New_York','-05:00',current_timestamp(),NULL),(170,'America/Nipigon','-05:00',current_timestamp(),NULL),(171,'America/Nome','-09:00',current_timestamp(),NULL),(172,'America/Noronha','-02:00',current_timestamp(),NULL),(173,'America/North_Dakota/Beulah','-06:00',current_timestamp(),NULL),(174,'America/North_Dakota/Center','-06:00',current_timestamp(),NULL),(175,'America/North_Dakota/New_Salem','-06:00',current_timestamp(),NULL),(176,'America/Ojinaga','-07:00',current_timestamp(),NULL),(177,'America/Panama','-05:00',current_timestamp(),NULL),(178,'America/Pangnirtung','-05:00',current_timestamp(),NULL),(179,'America/Paramaribo','-03:00',current_timestamp(),NULL),(180,'America/Phoenix','-07:00',current_timestamp(),NULL),(181,'America/Port_of_Spain','-04:00',current_timestamp(),NULL),(182,'America/Port-au-Prince','-05:00',current_timestamp(),NULL),(183,'America/Porto_Acre','-05:00',current_timestamp(),NULL),(184,'America/Porto_Velho','-04:00',current_timestamp(),NULL),(185,'America/Puerto_Rico','-04:00',current_timestamp(),NULL),(186,'America/Punta_Arenas','-03:00',current_timestamp(),NULL),(187,'America/Rainy_River','-06:00',current_timestamp(),NULL),(188,'America/Rankin_Inlet','-06:00',current_timestamp(),NULL),(189,'America/Recife','-03:00',current_timestamp(),NULL),(190,'America/Regina','-06:00',current_timestamp(),NULL),(191,'America/Resolute','-06:00',current_timestamp(),NULL),(192,'America/Rio_Branco','-05:00',current_timestamp(),NULL),(193,'America/Rosario','-03:00',current_timestamp(),NULL),(194,'America/Santa_Isabel','-08:00',current_timestamp(),NULL),(195,'America/Santarem','-03:00',current_timestamp(),NULL),(196,'America/Santiago','-04:00',current_timestamp(),NULL),(197,'America/Santo_Domingo','-04:00',current_timestamp(),NULL),(198,'America/Sao_Paulo','-03:00',current_timestamp(),NULL),(199,'America/Scoresbysund','-01:00',current_timestamp(),NULL),(200,'America/Shiprock','-07:00',current_timestamp(),NULL),(201,'America/Sitka','-09:00',current_timestamp(),NULL),(202,'America/St_Barthelemy','-04:00',current_timestamp(),NULL),(203,'America/St_Johns','   -03:30',current_timestamp(),NULL),(204,'America/St_Kitts','-04:00',current_timestamp(),NULL),(205,'America/St_Lucia','-04:00',current_timestamp(),NULL),(206,'America/St_Thomas','-04:00',current_timestamp(),NULL),(207,'America/St_Vincent','-04:00',current_timestamp(),NULL),(208,'America/Swift_Current','-06:00',current_timestamp(),NULL),(209,'America/Tegucigalpa','-06:00',current_timestamp(),NULL),(210,'America/Thule','-04:00',current_timestamp(),NULL),(211,'America/Thunder_Bay','-05:00',current_timestamp(),NULL),(212,'America/Tijuana','-08:00',current_timestamp(),NULL),(213,'America/Toronto','-05:00',current_timestamp(),NULL),(214,'America/Tortola','-04:00',current_timestamp(),NULL),(215,'America/Vancouver','-08:00',current_timestamp(),NULL),(216,'America/Virgin','-04:00',current_timestamp(),NULL),(217,'America/Whitehorse','-08:00',current_timestamp(),NULL),(218,'America/Winnipeg','-06:00',current_timestamp(),NULL),(219,'America/Yakutat','-09:00',current_timestamp(),NULL),(220,'America/Yellowknife','-07:00',current_timestamp(),NULL),(221,'Antarctica/Casey','+11:00',current_timestamp(),NULL),(222,'Antarctica/Davis','+07:00',current_timestamp(),NULL),(223,'Antarctica/DumontDUrville','+10:00',current_timestamp(),NULL),(224,'Antarctica/Macquarie','+11:00',current_timestamp(),NULL),(225,'Antarctica/Mawson','+05:00',current_timestamp(),NULL),(226,'Antarctica/McMurdo','+12:00',current_timestamp(),NULL),(227,'Antarctica/Palmer','-03:00',current_timestamp(),NULL),(228,'Antarctica/Rothera','-03:00',current_timestamp(),NULL),(229,'Antarctica/South_Pole','+12:00',current_timestamp(),NULL),(230,'Antarctica/Syowa','+03:00',current_timestamp(),NULL),(231,'Antarctica/Troll','+00:00',current_timestamp(),NULL),(232,'Antarctica/Vostok','+06:00',current_timestamp(),NULL),(233,'Arctic/Longyearbyen','+01:00',current_timestamp(),NULL),(234,'Asia/Aden','+03:00',current_timestamp(),NULL),(235,'Asia/Almaty','+06:00',current_timestamp(),NULL),(236,'Asia/Amman','+02:00',current_timestamp(),NULL),(237,'Asia/Anadyr','+12:00',current_timestamp(),NULL),(238,'Asia/Aqtau','+05:00',current_timestamp(),NULL),(239,'Asia/Aqtobe','+05:00',current_timestamp(),NULL),(240,'Asia/Ashgabat','+05:00',current_timestamp(),NULL),(241,'Asia/Ashkhabad','+05:00',current_timestamp(),NULL),(242,'Asia/Atyrau','+05:00',current_timestamp(),NULL),(243,'Asia/Baghdad','+03:00',current_timestamp(),NULL),(244,'Asia/Bahrain','+03:00',current_timestamp(),NULL),(245,'Asia/Baku','+04:00',current_timestamp(),NULL),(246,'Asia/Bangkok','+07:00',current_timestamp(),NULL),(247,'Asia/Barnaul','+07:00',current_timestamp(),NULL),(248,'Asia/Beirut','+02:00',current_timestamp(),NULL),(249,'Asia/Bishkek','+06:00',current_timestamp(),NULL),(250,'Asia/Brunei','+08:00',current_timestamp(),NULL),(251,'Asia/Calcutta','+05:30',current_timestamp(),NULL),(252,'Asia/Chita','+09:00',current_timestamp(),NULL),(253,'Asia/Choibalsan','+08:00',current_timestamp(),NULL),(254,'Asia/Chongqing','+08:00',current_timestamp(),NULL),(255,'Asia/Chungking','+08:00',current_timestamp(),NULL),(256,'Asia/Colombo','+05:30',current_timestamp(),NULL),(257,'Asia/Dacca','+06:00',current_timestamp(),NULL),(258,'Asia/Damascus','+02:00',current_timestamp(),NULL),(259,'Asia/Dhaka','+06:00',current_timestamp(),NULL),(260,'Asia/Dili','+09:00',current_timestamp(),NULL),(261,'Asia/Dubai','+04:00',current_timestamp(),NULL),(262,'Asia/Dushanbe','+05:00',current_timestamp(),NULL),(263,'Asia/Famagusta','+03:00',current_timestamp(),NULL),(264,'Asia/Gaza','+02:00',current_timestamp(),NULL),(265,'Asia/Harbin','+08:00',current_timestamp(),NULL),(266,'Asia/Hebron','+02:00',current_timestamp(),NULL),(267,'Asia/Ho_Chi_Minh','+07:00',current_timestamp(),NULL),(268,'Asia/Hong_Kong','+08:00',current_timestamp(),NULL),(269,'Asia/Hovd','+07:00',current_timestamp(),NULL),(270,'Asia/Irkutsk','+08:00',current_timestamp(),NULL),(271,'Asia/Istanbul','+03:00',current_timestamp(),NULL),(272,'Asia/Jakarta','+07:00',current_timestamp(),NULL),(273,'Asia/Jayapura','+09:00',current_timestamp(),NULL),(274,'Asia/Jerusalem','+02:00',current_timestamp(),NULL),(275,'Asia/Kabul','+04:30',current_timestamp(),NULL),(276,'Asia/Kamchatka','+12:00',current_timestamp(),NULL),(277,'Asia/Karachi','+05:00',current_timestamp(),NULL),(278,'Asia/Kashgar','+06:00',current_timestamp(),NULL),(279,'Asia/Kathmandu','+05:45',current_timestamp(),NULL),(280,'Asia/Katmandu','+05:45',current_timestamp(),NULL),(281,'Asia/Khandyga','+09:00',current_timestamp(),NULL),(282,'Asia/Kolkata','+05:30',current_timestamp(),NULL),(283,'Asia/Krasnoyarsk','+07:00',current_timestamp(),NULL),(284,'Asia/Kuala_Lumpur','+08:00',current_timestamp(),NULL),(285,'Asia/Kuching','+08:00',current_timestamp(),NULL),(286,'Asia/Kuwait','+03:00',current_timestamp(),NULL),(287,'Asia/Macao','+08:00',current_timestamp(),NULL),(288,'Asia/Macau','+08:00',current_timestamp(),NULL),(289,'Asia/Magadan','+11:00',current_timestamp(),NULL),(290,'Asia/Makassar','+08:00',current_timestamp(),NULL),(291,'Asia/Manila','+08:00',current_timestamp(),NULL),(292,'Asia/Muscat','+04:00',current_timestamp(),NULL),(293,'Asia/Nicosia','+02:00',current_timestamp(),NULL),(294,'Asia/Novokuznetsk','+07:00',current_timestamp(),NULL),(295,'Asia/Novosibirsk','+07:00',current_timestamp(),NULL),(296,'Asia/Omsk','+06:00',current_timestamp(),NULL),(297,'Asia/Oral','+05:00',current_timestamp(),NULL),(298,'Asia/Phnom_Penh','+07:00',current_timestamp(),NULL),(299,'Asia/Pontianak','+07:00',current_timestamp(),NULL),(300,'Asia/Pyongyang','+08:30',current_timestamp(),NULL),(301,'Asia/Qatar','+03:00',current_timestamp(),NULL),(302,'Asia/Qyzylorda','+06:00',current_timestamp(),NULL),(303,'Asia/Rangoon','+06:30',current_timestamp(),NULL),(304,'Asia/Riyadh','+03:00',current_timestamp(),NULL),(305,'Asia/Saigon','+07:00',current_timestamp(),NULL),(306,'Asia/Sakhalin','+11:00',current_timestamp(),NULL),(307,'Asia/Samarkand','+05:00',current_timestamp(),NULL),(308,'Asia/Seoul','+09:00',current_timestamp(),NULL),(309,'Asia/Shanghai','+08:00',current_timestamp(),NULL),(310,'Asia/Singapore','+08:00',current_timestamp(),NULL),(311,'Asia/Srednekolymsk','+11:00',current_timestamp(),NULL),(312,'Asia/Taipei','+08:00',current_timestamp(),NULL),(313,'Asia/Tashkent','+05:00',current_timestamp(),NULL),(314,'Asia/Tbilisi','+04:00',current_timestamp(),NULL),(315,'Asia/Tehran','+03:30',current_timestamp(),NULL),(316,'Asia/Tel_Aviv','+02:00',current_timestamp(),NULL),(317,'Asia/Thimbu','+06:00',current_timestamp(),NULL),(318,'Asia/Thimphu','+06:00',current_timestamp(),NULL),(319,'Asia/Tokyo','+09:00',current_timestamp(),NULL),(320,'Asia/Tomsk','+07:00',current_timestamp(),NULL),(321,'Asia/Ujung_Pandang','+08:00',current_timestamp(),NULL),(322,'Asia/Ulaanbaatar','+08:00',current_timestamp(),NULL),(323,'Asia/Ulan_Bator','+08:00',current_timestamp(),NULL),(324,'Asia/Urumqi','+06:00',current_timestamp(),NULL),(325,'Asia/Ust-Nera','+10:00',current_timestamp(),NULL),(326,'Asia/Vientiane','+07:00',current_timestamp(),NULL),(327,'Asia/Vladivostok','+10:00',current_timestamp(),NULL),(328,'Asia/Yakutsk','+09:00',current_timestamp(),NULL),(329,'Asia/Yangon','+06:30',current_timestamp(),NULL),(330,'Asia/Yekaterinburg','+05:00',current_timestamp(),NULL),(331,'Asia/Yerevan','+04:00',current_timestamp(),NULL),(332,'Atlantic/Azores','-01:00',current_timestamp(),NULL),(333,'Atlantic/Bermuda','-04:00',current_timestamp(),NULL),(334,'Atlantic/Canary','+00:00',current_timestamp(),NULL),(335,'Atlantic/Cape_Verde','-01:00',current_timestamp(),NULL),(336,'Atlantic/Faeroe','+00:00',current_timestamp(),NULL),(337,'Atlantic/Faroe','+00:00',current_timestamp(),NULL),(338,'Atlantic/Jan_Mayen','+01:00',current_timestamp(),NULL),(339,'Atlantic/Madeira','+00:00',current_timestamp(),NULL),(340,'Atlantic/Reykjavik','+00:00',current_timestamp(),NULL),(341,'Atlantic/South_Georgia','-02:00',current_timestamp(),NULL),(342,'Atlantic/St_Helena','+00:00',current_timestamp(),NULL),(343,'Atlantic/Stanley','-03:00',current_timestamp(),NULL),(344,'Australia/ACT','+10:00',current_timestamp(),NULL),(345,'Australia/Adelaide','+09:30',current_timestamp(),NULL),(346,'Australia/Brisbane','+10:00',current_timestamp(),NULL),(347,'Australia/Broken_Hill','+09:30',current_timestamp(),NULL),(348,'Australia/Canberra','+10:00',current_timestamp(),NULL),(349,'Australia/Currie','+10:00',current_timestamp(),NULL),(350,'Australia/Darwin','+09:30',current_timestamp(),NULL),(351,'Australia/Eucla','+08:45',current_timestamp(),NULL),(352,'Australia/Hobart','+10:00',current_timestamp(),NULL),(353,'Australia/LHI','+10:30',current_timestamp(),NULL),(354,'Australia/Lindeman','+10:00',current_timestamp(),NULL),(355,'Australia/Lord_Howe','+10:30',current_timestamp(),NULL),(356,'Australia/Melbourne','+10:00',current_timestamp(),NULL),(357,'Australia/North','+09:30',current_timestamp(),NULL),(358,'Australia/NSW','+10:00',current_timestamp(),NULL),(359,'Australia/Perth','+08:00',current_timestamp(),NULL),(360,'Australia/Queensland','+10:00',current_timestamp(),NULL),(361,'Australia/South','+09:30',current_timestamp(),NULL),(362,'Australia/Sydney','+10:00',current_timestamp(),NULL),(363,'Australia/Tasmania','+10:00',current_timestamp(),NULL),(364,'Australia/Victoria','+10:00',current_timestamp(),NULL),(365,'Australia/West','+08:00',current_timestamp(),NULL),(366,'Australia/Yancowinna','+09:30',current_timestamp(),NULL),(367,'Brazil/Acre','-05:00',current_timestamp(),NULL),(368,'Brazil/DeNoronha','-02:00',current_timestamp(),NULL),(369,'Brazil/East','-03:00',current_timestamp(),NULL),(370,'Brazil/West','-04:00',current_timestamp(),NULL),(371,'Canada/Atlantic','-04:00',current_timestamp(),NULL),(372,'Canada/Central','-06:00',current_timestamp(),NULL),(373,'Canada/Eastern','-05:00',current_timestamp(),NULL),(374,'Canada/East-Saskatchewan','-06:00',current_timestamp(),NULL),(375,'Canada/Mountain','-07:00',current_timestamp(),NULL),(376,'Canada/Newfoundland','-03:30',current_timestamp(),NULL),(377,'Canada/Pacific','-08:00',current_timestamp(),NULL),(378,'Canada/Saskatchewan','-06:00',current_timestamp(),NULL),(379,'Canada/Yukon','-08:00',current_timestamp(),NULL),(380,'CET','+01:00',current_timestamp(),NULL),(381,'Chile/Continental','-04:00',current_timestamp(),NULL),(382,'Chile/EasterIsland','-06:00',current_timestamp(),NULL),(383,'CST6CDT','-06:00',current_timestamp(),NULL),(384,'Cuba','-05:00',current_timestamp(),NULL),(385,'EET','+02:00',current_timestamp(),NULL),(386,'Egypt','+02:00',current_timestamp(),NULL),(387,'Eire','+00:00',current_timestamp(),NULL),(388,'EST','-05:00',current_timestamp(),NULL),(389,'EST5EDT','-05:00',current_timestamp(),NULL),(390,'Etc/GMT','+00:00',current_timestamp(),NULL),(391,'Etc/GMT+0','+00:00',current_timestamp(),NULL),(392,'Etc/GMT+1','-01:00',current_timestamp(),NULL),(393,'Etc/GMT+10','-10:00',current_timestamp(),NULL),(394,'Etc/GMT+11','-11:00',current_timestamp(),NULL),(395,'Etc/GMT+12','-12:00',current_timestamp(),NULL),(396,'Etc/GMT+2','-02:00',current_timestamp(),NULL),(397,'Etc/GMT+3','-03:00',current_timestamp(),NULL),(398,'Etc/GMT+4','-04:00',current_timestamp(),NULL),(399,'Etc/GMT+5','-05:00',current_timestamp(),NULL),(400,'Etc/GMT+6','-06:00',current_timestamp(),NULL),(401,'Etc/GMT+7','-07:00',current_timestamp(),NULL),(402,'Etc/GMT+8','-08:00',current_timestamp(),NULL),(403,'Etc/GMT+9','-09:00',current_timestamp(),NULL),(404,'Etc/GMT0','+00:00',current_timestamp(),NULL),(405,'Etc/GMT-0','+00:00',current_timestamp(),NULL),(406,'Etc/GMT-1','+01:00',current_timestamp(),NULL),(407,'Etc/GMT-10','+10:00',current_timestamp(),NULL),(408,'Etc/GMT-11','+11:00',current_timestamp(),NULL),(409,'Etc/GMT-12','+12:00',current_timestamp(),NULL),(410,'Etc/GMT-13','+13:00',current_timestamp(),NULL),(411,'Etc/GMT-14','+14:00',current_timestamp(),NULL),(412,'Etc/GMT-2','+02:00',current_timestamp(),NULL),(413,'Etc/GMT-3','+03:00',current_timestamp(),NULL),(414,'Etc/GMT-4','+04:00',current_timestamp(),NULL),(415,'Etc/GMT-5','+05:00',current_timestamp(),NULL),(416,'Etc/GMT-6','+06:00',current_timestamp(),NULL),(417,'Etc/GMT-7','+07:00',current_timestamp(),NULL),(418,'Etc/GMT-8','+08:00',current_timestamp(),NULL),(419,'Etc/GMT-9','+09:00',current_timestamp(),NULL),(420,'Etc/Greenwich','+00:00',current_timestamp(),NULL),(421,'Etc/UCT','+00:00',current_timestamp(),NULL),(422,'Etc/Universal','+00:00',current_timestamp(),NULL),(423,'Etc/UTC','+00:00',current_timestamp(),NULL),(424,'Etc/Zulu','+00:00',current_timestamp(),NULL),(425,'Europe/Amsterdam','+01:00',current_timestamp(),NULL),(426,'Europe/Andorra','+01:00',current_timestamp(),NULL),(427,'Europe/Astrakhan','+04:00',current_timestamp(),NULL),(428,'Europe/Athens','+02:00',current_timestamp(),NULL),(429,'Europe/Belfast','+00:00',current_timestamp(),NULL),(430,'Europe/Belgrade','+01:00',current_timestamp(),NULL),(431,'Europe/Berlin','+01:00',current_timestamp(),NULL),(432,'Europe/Bratislava','+01:00',current_timestamp(),NULL),(433,'Europe/Brussels','+01:00',current_timestamp(),NULL),(434,'Europe/Bucharest','+02:00',current_timestamp(),NULL),(435,'Europe/Budapest','+01:00',current_timestamp(),NULL),(436,'Europe/Busingen','+01:00',current_timestamp(),NULL),(437,'Europe/Chisinau','+02:00',current_timestamp(),NULL),(438,'Europe/Copenhagen','+01:00',current_timestamp(),NULL),(439,'Europe/Dublin','+00:00',current_timestamp(),NULL),(440,'Europe/Gibraltar','+01:00',current_timestamp(),NULL),(441,'Europe/Guernsey','+00:00',current_timestamp(),NULL),(442,'Europe/Helsinki','+02:00',current_timestamp(),NULL),(443,'Europe/Isle_of_Man','+00:00',current_timestamp(),NULL),(444,'Europe/Istanbul','+03:00',current_timestamp(),NULL),(445,'Europe/Jersey','+00:00',current_timestamp(),NULL),(446,'Europe/Kaliningrad','+02:00',current_timestamp(),NULL),(447,'Europe/Kiev','+02:00',current_timestamp(),NULL),(448,'Europe/Kirov','+03:00',current_timestamp(),NULL),(449,'Europe/Lisbon','+00:00',current_timestamp(),NULL),(450,'Europe/Ljubljana','+01:00',current_timestamp(),NULL),(451,'Europe/London','+00:00',current_timestamp(),NULL),(452,'Europe/Luxembourg','+01:00',current_timestamp(),NULL),(453,'Europe/Madrid','+01:00',current_timestamp(),NULL),(454,'Europe/Malta','+01:00',current_timestamp(),NULL),(455,'Europe/Mariehamn','+02:00',current_timestamp(),NULL),(456,'Europe/Minsk','+03:00',current_timestamp(),NULL),(457,'Europe/Monaco','+01:00',current_timestamp(),NULL),(458,'Europe/Moscow','+03:00',current_timestamp(),NULL),(459,'Europe/Nicosia','+02:00',current_timestamp(),NULL),(460,'Europe/Oslo','+01:00',current_timestamp(),NULL),(461,'Europe/Paris','+01:00',current_timestamp(),NULL),(462,'Europe/Podgorica','+01:00',current_timestamp(),NULL),(463,'Europe/Prague','+01:00',current_timestamp(),NULL),(464,'Europe/Riga','+02:00',current_timestamp(),NULL),(465,'Europe/Rome','+01:00',current_timestamp(),NULL),(466,'Europe/Samara','+04:00',current_timestamp(),NULL),(467,'Europe/San_Marino','+01:00',current_timestamp(),NULL),(468,'Europe/Sarajevo','+01:00',current_timestamp(),NULL),(469,'Europe/Saratov','+04:00',current_timestamp(),NULL),(470,'Europe/Simferopol','+03:00',current_timestamp(),NULL),(471,'Europe/Skopje','+01:00',current_timestamp(),NULL),(472,'Europe/Sofia','+02:00',current_timestamp(),NULL),(473,'Europe/Stockholm','+01:00',current_timestamp(),NULL),(474,'Europe/Tallinn','+02:00',current_timestamp(),NULL),(475,'Europe/Tirane','+01:00',current_timestamp(),NULL),(476,'Europe/Tiraspol','+02:00',current_timestamp(),NULL),(477,'Europe/Ulyanovsk','+04:00',current_timestamp(),NULL),(478,'Europe/Uzhgorod','+02:00',current_timestamp(),NULL),(479,'Europe/Vaduz','+01:00',current_timestamp(),NULL),(480,'Europe/Vatican','+01:00',current_timestamp(),NULL),(481,'Europe/Vienna','+01:00',current_timestamp(),NULL),(482,'Europe/Vilnius','+02:00',current_timestamp(),NULL),(483,'Europe/Volgograd','+03:00',current_timestamp(),NULL),(484,'Europe/Warsaw','+01:00',current_timestamp(),NULL),(485,'Europe/Zagreb','+01:00',current_timestamp(),NULL),(486,'Europe/Zaporozhye','+02:00',current_timestamp(),NULL),(487,'Europe/Zurich','+01:00',current_timestamp(),NULL),(488,'GB','+00:00',current_timestamp(),NULL),(489,'GB-Eire','+00:00',current_timestamp(),NULL),(490,'GMT','+00:00',current_timestamp(),NULL),(491,'GMT+0','+00:00',current_timestamp(),NULL),(492,'GMT0','+00:00',current_timestamp(),NULL),(493,'GMT-0','+00:00',current_timestamp(),NULL),(494,'Greenwich','+00:00',current_timestamp(),NULL),(495,'Hongkong','+08:00',current_timestamp(),NULL),(496,'HST','-10:00',current_timestamp(),NULL),(497,'Iceland','+00:00',current_timestamp(),NULL),(498,'Indian/Antananarivo','+03:00',current_timestamp(),NULL),(499,'Indian/Chagos','+06:00',current_timestamp(),NULL),(500,'Indian/Christmas','+07:00',current_timestamp(),NULL),(501,'Indian/Cocos','+06:30',current_timestamp(),NULL),(502,'Indian/Comoro','+03:00',current_timestamp(),NULL),(503,'Indian/Kerguelen','+05:00',current_timestamp(),NULL),(504,'Indian/Mahe','+04:00',current_timestamp(),NULL),(505,'Indian/Maldives','+05:00',current_timestamp(),NULL),(506,'Indian/Mauritius','+04:00',current_timestamp(),NULL),(507,'Indian/Mayotte','+03:00',current_timestamp(),NULL),(508,'Indian/Reunion','+04:00',current_timestamp(),NULL),(509,'Iran','+03:30',current_timestamp(),NULL),(510,'Israel','+02:00',current_timestamp(),NULL),(511,'Jamaica','-05:00',current_timestamp(),NULL),(512,'Japan','+09:00',current_timestamp(),NULL),(513,'Kwajalein','+12:00',current_timestamp(),NULL),(514,'Libya','+02:00',current_timestamp(),NULL),(515,'MET','+01:00',current_timestamp(),NULL),(516,'Mexico/BajaNorte','-08:00',current_timestamp(),NULL),(517,'Mexico/BajaSur','-07:00',current_timestamp(),NULL),(518,'Mexico/General','-06:00',current_timestamp(),NULL),(519,'MST','-07:00',current_timestamp(),NULL),(520,'MST7MDT','-07:00',current_timestamp(),NULL),(521,'Navajo','-07:00',current_timestamp(),NULL),(522,'NZ','+12:00',current_timestamp(),NULL),(523,'NZ-CHAT','+12:45',current_timestamp(),NULL),(524,'Pacific/Apia','+13:00',current_timestamp(),NULL),(525,'Pacific/Auckland','+12:00',current_timestamp(),NULL),(526,'Pacific/Bougainville','+11:00',current_timestamp(),NULL),(527,'Pacific/Chatham','+12:45',current_timestamp(),NULL),(528,'Pacific/Chuuk','+10:00',current_timestamp(),NULL),(529,'Pacific/Easter','-06:00',current_timestamp(),NULL),(530,'Pacific/Efate','+11:00',current_timestamp(),NULL),(531,'Pacific/Enderbury','+13:00',current_timestamp(),NULL),(532,'Pacific/Fakaofo','+13:00',current_timestamp(),NULL),(533,'Pacific/Fiji','+12:00',current_timestamp(),NULL),(534,'Pacific/Funafuti','+12:00',current_timestamp(),NULL),(535,'Pacific/Galapagos','-06:00',current_timestamp(),NULL),(536,'Pacific/Gambier','-09:00',current_timestamp(),NULL),(537,'Pacific/Guadalcanal','+11:00',current_timestamp(),NULL),(538,'Pacific/Guam','+10:00',current_timestamp(),NULL),(539,'Pacific/Honolulu','-10:00',current_timestamp(),NULL),(540,'Pacific/Johnston','-10:00',current_timestamp(),NULL),(541,'Pacific/Kiritimati','+14:00',current_timestamp(),NULL),(542,'Pacific/Kosrae','+11:00',current_timestamp(),NULL),(543,'Pacific/Kwajalein','+12:00',current_timestamp(),NULL),(544,'Pacific/Majuro','+12:00',current_timestamp(),NULL),(545,'Pacific/Marquesas','-09:30',current_timestamp(),NULL),(546,'Pacific/Midway','-11:00',current_timestamp(),NULL),(547,'Pacific/Nauru','+12:00',current_timestamp(),NULL),(548,'Pacific/Niue','-11:00',current_timestamp(),NULL),(549,'Pacific/Norfolk','+11:00',current_timestamp(),NULL),(550,'Pacific/Noumea','+11:00',current_timestamp(),NULL),(551,'Pacific/Pago_Pago','-11:00',current_timestamp(),NULL),(552,'Pacific/Palau','+09:00',current_timestamp(),NULL),(553,'Pacific/Pitcairn','-08:00',current_timestamp(),NULL),(554,'Pacific/Pohnpei','+11:00',current_timestamp(),NULL),(555,'Pacific/Ponape','+11:00',current_timestamp(),NULL),(556,'Pacific/Port_Moresby','+10:00',current_timestamp(),NULL),(557,'Pacific/Rarotonga','-10:00',current_timestamp(),NULL),(558,'Pacific/Saipan','+10:00',current_timestamp(),NULL),(559,'Pacific/Samoa','-11:00',current_timestamp(),NULL),(560,'Pacific/Tahiti','-10:00',current_timestamp(),NULL),(561,'Pacific/Tarawa','+12:00',current_timestamp(),NULL),(562,'Pacific/Tongatapu','+13:00',current_timestamp(),NULL),(563,'Pacific/Truk','+10:00',current_timestamp(),NULL),(564,'Pacific/Wake','+12:00',current_timestamp(),NULL),(565,'Pacific/Wallis','+12:00',current_timestamp(),NULL),(566,'Pacific/Yap','+10:00',current_timestamp(),NULL),(567,'Poland','+01:00',current_timestamp(),NULL),(568,'Portugal','+00:00',current_timestamp(),NULL),(569,'PRC','+08:00',current_timestamp(),NULL),(570,'PST8PDT','-08:00',current_timestamp(),NULL),(571,'ROC','+08:00',current_timestamp(),NULL),(572,'ROK','+09:00',current_timestamp(),NULL),(573,'Singapore','+08:00',current_timestamp(),NULL),(574,'Turkey','+03:00',current_timestamp(),NULL),(575,'UCT','+00:00',current_timestamp(),NULL),(576,'Universal','+00:00',current_timestamp(),NULL),(577,'US/Alaska','-09:00',current_timestamp(),NULL),(578,'US/Aleutian','-10:00',current_timestamp(),NULL),(579,'US/Arizona','-07:00',current_timestamp(),NULL),(580,'US/Central','-06:00',current_timestamp(),NULL),(581,'US/Eastern','-05:00',current_timestamp(),NULL),(582,'US/East-Indiana','-05:00',current_timestamp(),NULL),(583,'US/Hawaii','-10:00',current_timestamp(),NULL),(584,'US/Indiana-Starke','-06:00',current_timestamp(),NULL),(585,'US/Michigan','-05:00',current_timestamp(),NULL),(586,'US/Mountain','-07:00',current_timestamp(),NULL),(587,'US/Pacific','-08:00',current_timestamp(),NULL),(588,'US/Pacific-New','-08:00',current_timestamp(),NULL),(589,'US/Samoa','-11:00',current_timestamp(),NULL),(590,'UTC','+00:00',current_timestamp(),NULL),(591,'WET','+00:00',current_timestamp(),NULL),(592,'W-SU','+03:00',current_timestamp(),NULL),(593,'Zulu','+00:00',current_timestamp(),NULL);
/*!40000 ALTER TABLE `timezone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(36) DEFAULT NULL,
  `state` varchar(45) NOT NULL,
  `reference` varchar(45) DEFAULT NULL,
  `signature` varchar(45) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_transaction_user1_idx` (`user_id`),
  CONSTRAINT `fk_transaction_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction`
--

LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` varchar(36) NOT NULL,
  `username` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `state_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_user_state1_idx` (`state_id`),
  CONSTRAINT `fk_user_state1` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_profile`
--

DROP TABLE IF EXISTS `user_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_profile` (
  `user_id` varchar(36) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `bio` varchar(160) DEFAULT NULL,
  `url` text,
  `profile_picture` text,
  `country_id` int(11) DEFAULT NULL,
  `timezone_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `fk_user_profile_country1_idx` (`country_id`),
  KEY `fk_user_profile_timezone1_idx` (`timezone_id`),
  CONSTRAINT `fk_user_profile_country1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_profile_timezone1` FOREIGN KEY (`timezone_id`) REFERENCES `timezone` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_profile_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_profile`
--

LOCK TABLES `user_profile` WRITE;
/*!40000 ALTER TABLE `user_profile` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(36) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_has_role_role1_idx` (`role_id`),
  KEY `fk_user_has_role_user1_idx` (`user_id`),
  CONSTRAINT `fk_user_has_role_role1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_role_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-31 19:53:28
