# SIMCA - Backend
SIMCA Backend project

Pasos para desplegar el proyecto:
1. En una instancia de MySQL ejecutar el script que crea el esquema de la base de datos de simca. Este script está en la carpeta db_model/SIMCA-database_script-x.x.x.sql. Debe asegurarse de que MySQL esté corriendo en el puerto 3306

2. Levantar una instancia de MongoDB y crear una base de datos con el nombre "simca". Debe asegurarse de que MongoDB esté corriendo en el puerto 27017. Este paso solo es necesario si se desea probar el endopint que permite agregar datos a una medición. Si no se realiza, el proyecto puede ejecutarse correctamente y probar las demás funcionalidades.

3. Descargar la colección de Postman y el environment. Estos se encuentran en la carpeta /postman.

4. Dentro de la carpeta del proyecto, ejecutar en la consola:

```
mvn clean install
mvn spring-boot run
```

Para visualizar los enpoint acceder a la url:

http://localhost:8080/swagger-ui.html

Para consumir los endpoints (usando Postman):
1. Registrarse como usuario
2. Confirmar el correo con el token que ha llegado a tu inbox.
3. Autenticarse con su username y su password. Estas variables deben cambiarse en en environment de Postman.

Nota: al autenticarse en el endopint /oauth/token, el access token y el refresh token quedan almacenados en el environment de Postman. El access token tiene un tiempo de expiración de 1 minuto para que permita consumir los endpoints que requieran autenticación.

Una vez expirado el access token, se debe renovar el mismo haciendo uso del refresh token, o bien, autenticandose nuevamente. Para hacer uso del refresh token existe la petición 'Refresh Access Token' en Postman en la carpeta Security, a diferencia de la petición 'Authenticate' cambia el contenido del body.

El refresh token tiene un tiempo de expiración de 24 días. Si este expira, se debe autenticar de nuevo.

### Colección de Postman

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/876ad8f679c3ca4f28c4)

Link del repositorio: https://bitbucket.org/pdg-icesi/simca-backend
